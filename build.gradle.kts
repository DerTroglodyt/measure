@file:Suppress("HardCodedStringLiteral")

import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent


plugins {
    kotlin("jvm") version "2.0.0" apply true
//    id("java-library")
//    alias(libs.plugins.android.lint)
}

kotlin {
    // for strict mode
    explicitApi()
    // warning mode
//    explicitApiWarning()
}

group = "de.hdc.measure"
version = "2025-03-05"

kotlin {
    jvmToolchain(libs.versions.jvmToolchain.get().toInt())
}

dependencies {
//    lintChecks(project(":lint"))
    testImplementation(libs.bundles.testing)
//    testImplementation("junit:junit:4.13.2")  // for android lint checks

    implementation(libs.measure.bigmath)
    implementation(libs.measure.bigmathKotlin)
//    implementation(libs.test.androidToolsLintApi)
}

//tasks.withType<Test> {
//    testLogging {
//        events(TestLogEvent.FAILED)
//        exceptionFormat = TestExceptionFormat.FULL
//    }
//}
