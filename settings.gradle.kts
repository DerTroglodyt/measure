// Where you configure the plugins used in this project.
pluginManagement {
    repositories {              // Listing where you want to pull plugins from
        google()
        mavenCentral()          // The Central Maven Repository (more about maven later on)
        gradlePluginPortal()    // Default Gradle plugin portal (https://plugins.gradle.org/)
    }
}
plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.8.0"
}

// define global repositories
dependencyResolutionManagement {
    @Suppress("UnstableApiUsage")
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    @Suppress("UnstableApiUsage")
    repositories {
        mavenCentral()
        maven("https://jitpack.io")
        maven("https://maven.google.com/")
    }
}

rootProject.name = "measure"
include("lint")
include("tools")
