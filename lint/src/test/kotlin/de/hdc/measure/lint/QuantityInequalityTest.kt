package de.hdc.measure.lint

import com.android.tools.lint.detector.api.*
import com.android.tools.lint.checks.infrastructure.LintDetectorTest
import com.android.tools.lint.checks.infrastructure.TestFile
import com.android.tools.lint.checks.infrastructure.TestLintTask
import de.hdc.measure.Length
import de.hdc.measure.m
import de.hdc.measure.numdouble.*

class QuantityInequalityTest : LintDetectorTest() {
    private val warn1: UnitNumberDouble<Length> = 2.0 ˍ m

    private val src = """
            import de.hdc.measure.*
            import de.hdc.measure.numdouble.*
            
            fun test() {
                val warn1 = UnitNumDouble(2.0.toDN(), m)
                val warn2 = UnitNumberImpl(2.0.toDN(), m)
                val warn3 = 2.0 ˍ m
                println("Meh")
            }
            """
    val targetPath = "../../../../out/artifacts/latest/measure_jar/measure.main.jar"
    val myLib = bytecode(targetPath, object : TestFile.BytecodeProducer() {
        override fun produce() = ((this::class.java.classLoader ?: ClassLoader.getSystemClassLoader())
            .getResourceAsStream(targetPath) ?: error("measure.jar not found"))
            .readBytes()
    })

    override fun getDetector(): Detector = QuantityInequality()

    override fun getIssues(): List<Issue> = listOf(QuantityInequality.ISSUE)

    fun testDocumentation() {
        println(warn1)
        TestLintTask.lint().files(kotlin(src).indented(), classpath(targetPath), myLib)
            .allowMissingSdk()
            .issues(QuantityInequality.ISSUE)
            .run()
            .expect(
                """
ä
"""
            )
    }

    fun testQuickfix() {
        TestLintTask.lint().files(kotlin(src).indented(), classpath(targetPath), myLib)
            .allowMissingSdk()
            .issues(QuantityInequality.ISSUE)
            .run()
            .expectFixDiffs(
                """
ä
"""
            )
    }
}
