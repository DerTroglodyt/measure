package de.hdc.measure.lint

import com.android.tools.lint.client.api.UElementHandler
import com.android.tools.lint.detector.api.*
import com.android.tools.lint.detector.api.interprocedural.CallGraph
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiMethod
import com.intellij.psi.util.elementType
import org.jetbrains.kotlin.analysis.api.analyze
import org.jetbrains.kotlin.analysis.api.calls.singleFunctionCallOrNull
import org.jetbrains.kotlin.psi.KtCallExpression
import org.jetbrains.kotlin.psi.KtVariableDeclaration
import org.jetbrains.kotlin.resolve.calls.callUtil.getType
import org.jetbrains.uast.*
import org.w3c.dom.Attr
import org.w3c.dom.Element

public class QuantityInequality : Detector(), SourceCodeScanner {
    public companion object Issues {
        @JvmField
        public val ISSUE: Issue = Issue.create(
            id = "de.hdc.measure.lint.QuantityInequality",
            briefDescription = "The `Quantity`s of the two units don't match.",
            explanation = "Non matching `Quantity`s in units will produce a runtime error.",
            implementation = Implementation(QuantityInequality::class.java, Scope.JAVA_FILE_SCOPE),
            category = Category.CORRECTNESS,
            severity = Severity.ERROR
        )
    }

//    override fun getApplicableConstructorTypes(): List<String>? = listOf(
//        "de.hdc.measure.UnitNumberImpl",
//        "UnitNumberImpl",
//        "Date"
//    )

//    override fun getApplicableMethodNames(): List<String>? = listOf(
//        "println"
//    )

    override fun getApplicableUastTypes(): List<Class<out UElement>>? = listOf(
        UDeclaration::class.java
    )

    override fun createUastHandler(context: JavaContext): UElementHandler? {
        return object : UElementHandler() {
            override fun visitDeclaration(node: UDeclaration) {
                val source = node.sourcePsi as? KtVariableDeclaration ?: return
                analyze(source) {
//                    val r = source.resolveCall()?.singleFunctionCallOrNull()?.javaClass
//                    println(source.getKtType())
                }
//                if (node is UVariable) {
//                    println("var")
//                    source as KtVariableDeclaration
//                    println(node.asCall()?.asSourceString())
//                    println(node.asRecursiveLogString())
//                }

                context.report(
                    issue = ISSUE,
                    scope = node as UElement,
                    location = context.getLocation(node as UElement),
                    message = "Quantities in units don't match!",
                    fix().alternatives(
                        fix().replace().all().with("Some replacement code.").shortenNames().build(),
                        fix().replace().all().with("Some other replacement code.").shortenNames().build()
                    )
                )
            }
        }
    }

//    override fun visitElement(context: XmlContext, element: Element) {
//        val node = (element as? UCallExpression) ?: error("F")
//        val expr = node.asCall() ?: error("F")
//        val a = expr.typeArguments.first()
//        val b = expr.returnType
//        println("hurtz!")
//        // Create the lint report
//        context.report(
//            issue = ISSUE,
//            scope = null,  //node
//            location = context.getLocation(node),
//            message = "Quantities in units don't match!",
//            fix().alternatives(
//                fix().replace().all().with("Some replacement code.").shortenNames().build(),
//                fix().replace().all().with("Some other replacement code.").shortenNames().build()
//            )
//        )
//    }

//    override fun visitConstructor(context: JavaContext, node: UCallExpression, constructor: PsiMethod) {
//        if (!context.evaluator.isMemberInSubClassOf(method, "de.hdc.measure.UnitNumberImpl")) {
//            return
//        }
//        // Only handle if constructor has no parameters
//        if (node.valueArguments.isNotEmpty()) {
//            return
//        }
//        println("hurtz!")
//        // Create the lint report
//        context.report(
//            issue = ISSUE,
//            scope = node,
//            location = context.getLocation(node),
//            message = "Quantities in units don't match!",
//            fix().alternatives(
//                fix().replace().all().with("Some replacement code.").shortenNames().build(),
//                fix().replace().all().with("Some other replacement code.").shortenNames().build()
//            )
//        )
//    }
}
