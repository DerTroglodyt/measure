package de.hdc.measure.lint

import com.android.tools.lint.client.api.IssueRegistry
import com.android.tools.lint.client.api.Vendor
import com.android.tools.lint.detector.api.Issue

public class MyIssueRegistry : IssueRegistry() {
    override val issues: List<Issue> = listOf(QuantityInequality.ISSUE)
    override val vendor: Vendor = Vendor(
        vendorName = "DerTroglodyt",
        identifier = null,
        feedbackUrl = null,
        contact = null
    )
}
