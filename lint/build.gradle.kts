@file:Suppress("HardCodedStringLiteral")

plugins {
    kotlin("jvm") version "2.0.0" apply true
    id("java-library")
    alias(libs.plugins.android.lint)
}

kotlin {
    // for strict mode
    explicitApi()
    // warning mode
//    explicitApiWarning()
}

group = "de.hdc.measure"
version = "2024-06-30"

kotlin {
    jvmToolchain(libs.versions.jvmToolchain.get().toInt())
}

lint {
    htmlReport = true
    htmlOutput = file("lint-report.html")
    textReport = true
    absolutePaths = false
    ignoreTestSources = true
}

dependencies {
    testImplementation(libs.bundles.testing)
    testImplementation("junit:junit:4.13.2")  // for android lint checks

    implementation(rootProject)
    implementation(libs.measure.bigmath)
    implementation(libs.measure.bigmathKotlin)
    implementation(libs.test.androidToolsLintApi)
}
