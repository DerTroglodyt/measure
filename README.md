# Measure

## **Attention: This code may not be stable enough for production!**

## Aim
Using Kotlin inline functions to create an easy to use (and read) type safe unit system.
Kotlin type inference makes for a clean looking code.

Omit confusion / errors based on unclear / changed parameter units.

Omit bugs from unit prefixes in calculations. (Adding seconds to milliseconds)
___
Usage example:
```
import de.hdc.measure.*
import de.hdc.measure.numdouble.*

val distance = 12.53 k m
val x = distance + (45.67 c m)
// pretty printing by default
println(x)
// 12.5 km

// formatted printing
println(x.format(7, 0, false))
// 12.530 456 7 km

// conversion to other unit
println((x to m).format(4, 0, false))
// 12 530.456 7 m

val celsius = 32.0 ˍ `°C`
val kelvin = celsius to K
println("$celsius == $kelvin")
// 32.0 °C == 305.2 K

// Ensure type safety of parameters and return types:
fun foo(distance: UnitNumDouble<Length>, elapsed: UnitNumDouble<Time>): UnitNumDouble<Velocity> {
    return (distance / elapsed) as UnitNumDouble<Velocity>
}
```
___
### Design choices
- Keep it as simple as possible
- Ease of use and readability 
- Use international standards
- Format numbers in a way to help readability for users worldwide
- Immutability

Results:
- Based on SI units
- Conversion for imperial units included.
- Number format: Spaces as thousands separator, point as decimal separator
- toString() gives a "sane" and short version of the internal value
- Use format() for customized output
- Values in code consist of a Long, Double or String followed by a prefix followed by a unit. 
  No prefix is represented by a short underscore (underscore, then STRG + Space will trigger IDE completion).
```
import de.hdc.measure.*
import de.hdc.measure.money.*
import de.hdc.measure.numdouble.*
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.numinteger.*

val a: UnitNumberDouble<Length> = 3.41 c m
val c: UnitNumberDouble<Length> = 0.001 k m
val b: UnitNumberLong<Length> = 34 ˍ m
val hp: UnitNumberBF<Length> = "1.23456789012345678901234567890" k m
val dollar: Money<USD> = "15.99" ˍ USD
```

There are four versions available:
- UnitNumberDouble: The "normal" version with Double precision
- UnitNumberBF: An arbitrary precision version using BigDecimal (wrapped by BigFloat library)
- UnitNumberLong: An integer type. 
- Money: An integer type for currency calculations which prevents rounding errors.

### Calculations
The resulting unit of multiplication or division get checked against known units.
If a unit of that Quantity is known it is chosen as the results unit.
Else a Combined unit is created from the involved base units.
Examples:
- (1 ˍ m) * (1 ˍ m) will become the known unit m²
- (1 ˍ m) / (1 ˍ s) will become the known unit [m s⁻¹]
- (1 ˍ Cd) / (1 ˍ A) will become the Combined unit [A⁻¹ Cd]

### Dependencies
kotlin-big-math
Kotlin library for BigDecimal math functions (pow, sqrt, log, sin, ...) using arbitrary precision.

https://github.com/eobermuhlner/kotlin-big-math

### License
Provided you cite and link to this repository all code is free to use and change to your liking.
