package de.hdc.measure.money

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.comparables.*
import io.kotest.matchers.shouldBe

class MoneyTest : FreeSpec({
    "fractions cr" {
        val m1 = "10.2345" ˍ CRD
        m1.value shouldBe 10L
        m1.cur shouldBe CRD
        m1.cur.name shouldBe "Credits"
        m1.cur.isoCode shouldBe "CRD"
        val m2 = "2.2222" ˍ CRD
        m2.value shouldBe 2L

        m1 + m2 shouldBe ("12.0" ˍ CRD)
        m1 - m2 shouldBe ("8.0" ˍ CRD)

        // Should be flagged by compiler
//        shouldThrow<IllegalArgumentException> {
//            m1.timesRem(m2)
//        }
//
//        shouldThrow<IllegalArgumentException> {
//            m1.divRem(m2)
//        }

        (m1 * 2.0) shouldBe ("20" ˍ CRD)
        (m1 * 2.0) shouldBe ("20" ˍ CRD)
        (m1 / 2.0) shouldBe ("5" ˍ CRD)
        (m1 / 2.0) shouldBe ("5" ˍ CRD)
        (m1 / 4) shouldBe ("2" ˍ CRD)

        m1.timesRem(2) shouldBe Pair("20.0" ˍ CRD, 0.0)
        m1.timesRem(2.0) shouldBe Pair("20.0" ˍ CRD, 0.0)

        m1.divRem(2) shouldBe Pair("5.0" ˍ CRD, 0.0)
        m1.divRem(2.0) shouldBe Pair("5.0" ˍ CRD, 0.0)
        m1.divRem(4.0) shouldBe Pair("2.0" ˍ CRD, 0.5)
    }

    "fractions eur" {
        val m1 = "10.2345" ˍ EUR
        m1.getValue() shouldBe 1023L
        m1.toDouble() shouldBe 10.23

        val m2 = "2.2222" ˍ EUR
        m2.getValue() shouldBe 222L
        m2.toDouble() shouldBe 2.22

        m1 + m2 shouldBe ("12.45" ˍ EUR)
        m1 - m2 shouldBe ("8.01" ˍ EUR)

        // Should be flagged by compiler
//        shouldThrow<IllegalArgumentException> {
//            m1.timesRem(m2)
//        }
//
//        shouldThrow<IllegalArgumentException> {
//            m1.divRem(m2)
//        }

        m1.timesRem(2) shouldBe Pair("20.46" ˍ EUR, 0.0)
        m1.timesRem(2.0) shouldBe Pair("20.46" ˍ EUR, 0.0)
        m1.timesRem(2.2) shouldBe Pair("22.50" ˍ EUR, 0.006000000000003638)

        m1.divRem(2) shouldBe Pair("5.11" ˍ EUR, 0.005)
        m1.divRem(2.0) shouldBe Pair("5.11" ˍ EUR, 0.005)
        m1.divRem(4.0) shouldBe Pair("2.55" ˍ EUR, 0.0075)
    }

    "toString" {
        @Suppress("DEPRECATION")
        Money(5.1199, EUR).toString() shouldBe "5.11 €"
        ("5.1199" k EUR).toString() shouldBe "5 119.90 €"
    }

    "compare" {
        ("4.32" ˍ EUR) shouldBeLessThan ("5.32" ˍ EUR)
        ("6.32" ˍ EUR) shouldBeGreaterThan ("5.32" ˍ EUR)
    }
})
