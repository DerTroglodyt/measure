package de.hdc.measure.numdouble

import de.hdc.measure.*
import io.kotest.core.spec.style.*
import org.junit.jupiter.api.Assertions.*

internal class FormatTest : FreeSpec() {

    init {
        "main" {
            // test format()
            val n = Typography.nbsp
            // Default values: 4 padding, 3 digits
            assertEquals(" 230.000${n}g", UnitNumber(230.0, g).format())
            // Default values: 4 padding, 3 digits
            assertEquals("-230.000${n}g", UnitNumber((-230.0), g).format())
            // Default values: 4 padding, 3 digits, auto-stepup
            assertEquals("   2.300${n}kg", UnitNumber(2300.0, g).format())
            // Default values: 4 padding, 3 digits, auto-stepdown
            assertEquals("   2.300${n}mg", UnitNumber(0.0023, g).format())

            // No optimization
            assertEquals("  23.000${n}g", UnitNumber(23.0, g).format(3u, 4u))
            // no digits
            assertEquals("    2.3${n}t", (2300.0 k g).format(1u, 5u, optimize = true))
            assertEquals("    2${n}t", (2300.0 k g).format(0u, 5u, optimize = true))
            // optimization, no digits
            assertEquals("  230${n}t", (230000.0 k g).format(0u, 5u, optimize = true))
        }
    }
}
