@file:Suppress("DEPRECATION")

package de.hdc.measure.numdouble

import de.hdc.measure.*
import de.hdc.measure.tools.*
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.doubles.plusOrMinus
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows

internal class UnitNumberTest : FreeSpec() {

//  val reader = autoClose(StringReader("xyz"))

    init {
        "numbers of different precision" {
            3.04 k g shouldBe UnitNumber(3040.0, g)
            (0.0 ˍ m) shouldBe (-0.0 ˍ m)
            (1000.0 ˍ m) shouldBe (1.0 k m)
            (0.002 ˍ m3) shouldBe (2.0 m m3)
            (0.000000002 ˍ m3) shouldNotBe (2.0 m m3)
        }

        "baseVal" {
            (100.0 k Pa).baseValue.value shouldBe 100_000_000.0
            (100.0 k Pa).printValue.toDouble() shouldBe 100.0
            (100.0 k Pa).toDouble() shouldBe 100_000.0

            (100.0 k g).baseValue.value shouldBe 100_000.0
            (100.0 k g).toDouble() shouldBe 100_000.0
            (100.0 k g).printValue.toDouble() shouldBe 100.0

            (0.1 ˍ t).baseValue.value shouldBe 100_000.0
            (0.1 ˍ t).toDouble() shouldBe 0.1
            (0.1 ˍ t).printValue.toDouble() shouldBe 0.1
        }

        "main" {
            2.0 * (1.0 k g) shouldBe (2.0 k g)

            (1_000.0 ˍ m2) toUnit m2 shouldBe (1_000.0 ˍ m2)
            (1.0 k m2) toUnit m2 shouldBe (1_000.0 ˍ m2)
            (1_000.0 ˍ m2) toUnit m2 shouldBe (1.0 k m2)

            (1_000.0 ˍ m3) toUnit m3 shouldBe (1_000.0 ˍ m3)
            (1.0 k m3) toUnit m3 shouldBe (1_000.0 ˍ m3)
            (1_000.0 ˍ m3) toUnit m3 shouldBe (1.0 k m3)

            (1.0 ˍ m3).shouldBe(1000.0 ˍ L)
            (1000.0 ˍ L).shouldBe(1.0 ˍ m3)
            ((100.0 m L) toUnit L).shouldBe(0.1 ˍ L)
            ((0.1 ˍ L) toUnit L).shouldBe(100.0 m L)

            ((1273.15 ˍ K) toUnit `°C`).toString() shouldBe "1.0 k°C"
            assertEquals(
                (1000.0 ˍ `°C`).toDouble(),
                ((1273.15 ˍ K) toUnit `°C`).toDouble(),
                0.0001
            )
            (1.0 k `°C`) shouldBe ((1273.15 ˍ K) toUnit `°C`)

            assertEquals(1.2 ˍ min, 1.2 ˍ min)

            // test convertTo()
            assertEquals(1.0 ˍ min, (60.0 ˍ s) toUnit min)
            assertEquals(60.0 ˍ s, (1.0 ˍ min) toUnit s)

            assertEquals(60.0 ˍ min, (1.0 ˍ h) toUnit min)
            assertEquals(1.0 ˍ h, (60.0 ˍ min) toUnit h)

            assertEquals(UnitNumber(1000.0, min), UnitNumber(60000.0, s) toUnit min)

            assertEquals(
                UnitNumber(
                    273.15,
                    K
                ), UnitNumber(0.0, `°C`) toUnit K
            )
            assertEquals(UnitNumber(273.15, K) toUnit `°C`, UnitNumber(0.0, `°C`))

            (1.0 k `°C`) toUnit `°C` shouldBe (1000.0 ˍ `°C`)
            (0.001 M `°C`) toUnit `°C` shouldBe (1000.0 ˍ `°C`)
            (1.0 k `°C`) shouldBe ((1000.0 ˍ `°C`) toUnit `°C`)
            (0.001 M `°C`) shouldBe ((1000.0 ˍ `°C`) toUnit `°C`)
            (1000.0 ˍ `°C`) shouldBe (1.0 k `°C`)
            (0.001 M `°C`) shouldBe (1.0 k `°C`)

//            (1.000000000000001 ˍ `°C`) shouldBe (1.0 ˍ `°C`)
//            (1.000000000000001 k `°C`) shouldBe (1.0 k `°C`)
            (1.00000000001 k `°C`).approximates(1.0 k `°C`, DoubleNumber.maxDifference) shouldBe true
            (1.00000000001 k `°C`).approximates(1.0 k `°C`, DoubleNumber.maxDifference) shouldBe true

            1273.15 ˍ K shouldBe ((1.0 k `°C`) toUnit K)
            ((1273.15 ˍ K) toUnit `°C`)
                .toDouble() shouldBe ((1000.0 ˍ `°C`).toDouble() plusOrMinus 0.0001)

            (1.0 ˍ N) shouldBe ((((((1.0 k g) * (1.0 ˍ m)) / (1.0 ˍ s)) / (1.0 ˍ s))) toUnit N)

            // test inverse()
            // REALLY???
            assertEquals(
                UnitNumber((-0.0), m), UnitNumber(
                    0.0,
                    m
                ).inverse()
            )
            assertEquals(
                UnitNumber((-1.0), m), UnitNumber(
                    1.0,
                    m
                ).inverse()
            )
            assertEquals(
                UnitNumber(1.0, m), UnitNumber(
                    (-1.0),
                    m
                ).inverse()
            )


            // test plus()
            // test minus()
            // test scalar(Double)
            // test scalar(Measure<SI_NONE>)
            // test times()
            // test div()


            // test toString()
            assertEquals("1.23", UnitNumber(1.23, UNITLESS).format(2u, 0u))
            assertEquals("-1.23", UnitNumber(-1.23, UNITLESS).format(2u, 0u))
            assertEquals("1.23${Typography.nbsp}kg", (1.23 k g).format(2u, 0u))
            assertEquals("-1.23${Typography.nbsp}kg", (-1.23 k g).format(2u, 0u))
            assertEquals("1.23${Typography.nbsp}[m s⁻²]", UnitNumber(1.23, m_s2).format(2u, 0u))


            // test compareTo()
            assertEquals(1, (1.0 k g).compareTo(1.0 ˍ g))
            (1.0 P g) shouldNotBe (1.0 Y g)
            assertEquals(0, (1.0 k g).compareTo((1.0 k g)))
            assertEquals(-1, (1.0 k g).compareTo((2.0 k g)))


            // test toDouble()
            (1.0 k g).toDouble() shouldBe 1_000.0
            (1.0 k g).baseValue.toDouble() shouldBe 1_000.0
            UnitNumber(1.0, t).getValue() shouldBe 1_000_000.0
            UnitNumber(1.0, t).toDouble() shouldBe 1.0
            UnitNumber(1.0, g).getValue() shouldBe 1.0
            UnitNumber(1.0, g).toDouble() shouldBe 1.0
            UnitNumber(0.0, g).toDouble() shouldBe 0.0
            UnitNumber(-1.0, g).getValue() shouldBe -1.0
        }

        "format" {
            val n = Typography.nbsp
            (3.12345 k g).format(padding = 0u) shouldBe "3.123${n}kg"
            (3.12345 k g).format() shouldBe "   3.123${n}kg"

            assertEquals("   0${n}g", (-0.0 k g).format(0u))
            assertEquals("   3${n}kg", (3.123456 k g).format(0u))
            assertEquals("   3.1${n}kg", (3.123456 k g).format(1u))
            assertEquals("   3.123 5${n}kg", (3.123456 k g).format(4u))

            assertEquals("  -3.123${n}kg", (-3.123456 k g).format())
            assertEquals("  -3${n}kg", (-3.123456 k g).format(0u))
            assertEquals("  -3.1${n}kg", (-3.123456 k g).format(1u))
            assertEquals("  -3.123 5${n}kg", (-3.123456 k g).format(4u))

            assertEquals(" 123.450${n}g", (0.12345 k g).format())
            assertEquals("   1.123${n}kg", (1.12345 k g).format())
            assertEquals("  12.123${n}kg", (12.12345 k g).format())
            assertEquals(" 123.123${n}kg", (123.12345 k g).format())
            assertEquals("   1.234${n}t", (1234.12345 k g).format(optimize = true))
            assertEquals("  12.345${n}t", (12345.12345 k g).format(optimize = true))
            assertEquals(" 123.456${n}t", (123456.12345 k g).format(optimize = true))
            assertEquals("   1.235${n}kt", (1234567.12345 k g).format(optimize = true))
            assertEquals("  12.346${n}kt", (12345678.12345 k g).format(optimize = true))
            assertEquals(" 123.457${n}kt", (123456789.12345 k g).format(optimize = true))

            assertEquals("-123.450${n}g", (-0.12345 k g).format())
            assertEquals("  -1.123${n}kg", (-1.12345 k g).format())
            assertEquals(" -12.123${n}kg", (-12.12345 k g).format())
            assertEquals("-123.123${n}kg", (-123.12345 k g).format())
            assertEquals("  -1.234${n}t", (-1234.12345 k g).format(optimize = true))
            assertEquals(" -12.345${n}t", (-12345.12345 k g).format(optimize = true))
            assertEquals("-123.456${n}t", (-123456.12345 k g).format(optimize = true))
            assertEquals("  -1.235${n}kt", (-1234567.12345 k g).format(optimize = true))
            assertEquals(" -12.346${n}kt", (-12345678.12345 k g).format(optimize = true))
            assertEquals("-123.457${n}kt", (-123456789.12345 k g).format(optimize = true))

            assertEquals(" 100.0${n}g", (0.1 k g).format(1u, optimize = false))
            assertEquals(" 120.00${n}g", (0.12 k g).format(2u, optimize = false))
            assertEquals(" 123.000${n}g", (0.123 k g).format(3u, optimize = false))
            assertEquals(" 123.4${n}g", (0.1234 k g).format(1u, optimize = false))
            assertEquals(" 123.45${n}g", (0.12345 k g).format(2u, optimize = false))
            assertEquals(" 123.456${n}g", (0.123456 k g).format(3u, optimize = false))
            assertEquals(" 123.456 7${n}g", (0.1234567 k g).format(4u, optimize = false))
            assertEquals(" 123.456 78${n}g", (0.12345678 k g).format(5u, optimize = false))
            assertEquals(" 123.456 789${n}g", (0.123456789 k g).format(6u, optimize = false))

            (5.0 M g).toString() shouldBe "5.0${n}Mg"

            ((((1.0 ˍ m2) * (1.0 ˍ m))) toUnit L).toString() shouldBe "1.0${n}kL"

            ((1.0 ˍ m2) / (0.0 ˍ m)).toString() shouldBe "∞${n}m"
            ((-1.0 ˍ m2) / (0.0 ˍ m)).toString() shouldBe "∞${n}m"
            ((0.0 ˍ m2) / (0.0 ˍ m)).toString() shouldBe "NaN${n}m"
        }

        "calculations" {
            ((10.0 k g) / (5.0 ˍ g)) shouldBe (2000.0 ˍ UNITLESS)
            (((10.0 k g) / (5.0 ˍ g)) * (3.0 ˍ s)) shouldBe (6000.0 ˍ s)
//            ((10.0 k g) / (5.0 ˍ g) * (3.0 ˍ s)).unit shouldBe s
            (((10.0 k g) / (5.0 ˍ g) * (3.0 ˍ s))) toUnit min shouldBe (100.0 ˍ min)
            ((10.0 k m) * (10.0 k m)) shouldBe (1.0e8 ˍ m2)
//            ((10.0 k g) * (10.0 k g)).value.toDouble() shouldBe (100.0)
//            ((10.0 k g) * (10.0 ˍ g)).value.toDouble() shouldBe (0.1)

            ((12.0 ˍ m) / 2.0) shouldBe (6.0 ˍ m)
            ((12.0 ˍ m) / (6.0 ˍ m)) shouldBe (2.0 ˍ UNITLESS)

            ((12.0 k m) / 2.0) shouldBe (6.0 k m)
            ((12.0 k m) / (6.0 k m)) shouldBe (2.0 ˍ UNITLESS)

            ((12.0 k g) / 2.0) shouldBe (6.0 k g)
            ((12.0 k g) / (6.0 k g)) shouldBe (2.0 ˍ UNITLESS)

            // Measures as scalars
            (12.0 ˍ m) / (2.0 ˍ UNITLESS) shouldBe (6.0 ˍ m)
            (12.0 k m) / (2.0 ˍ UNITLESS) shouldBe (6.0 k m)
            (12.0 k m) / (2.0 k UNITLESS) shouldBe (6.0 ˍ m)

            (12.0 ˍ m) * (2.0 ˍ UNITLESS) shouldBe (24.0 ˍ m)
            (12.0 k m) * (2.0 ˍ UNITLESS) shouldBe (24.0 k m)
            (12.0 k m) * (2.0 k UNITLESS) shouldBe (24.0 M m)
        }

        "Dreisatz" {
            dreisatz((12.0 ˍ m), (2.0 ˍ s), (24.0 ˍ m), null) shouldBe (4.0 ˍ s)
            dreisatz((12.0 ˍ m), null, (24.0 ˍ m), 4.0 ˍ s) shouldBe (2.0 ˍ s)
            dreisatz(null, (2.0 ˍ s), (24.0 ˍ m), 4.0 ˍ s) shouldBe (12.0 ˍ m)
            dreisatz((12.0 ˍ m), (2.0 ˍ s), null, 4.0 ˍ s) shouldBe (24.0 ˍ m)
        }

        "reciprocal" {
            // must return NaN
            (0.0 ˍ min).reciprocal().baseValue.value shouldBe Double.POSITIVE_INFINITY
            (0.0 ˍ min).reciprocal().isInfinite() shouldBe true
        }
    }
}
