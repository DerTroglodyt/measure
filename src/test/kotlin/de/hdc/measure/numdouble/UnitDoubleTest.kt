package de.hdc.measure.numdouble

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.tools.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*
import io.kotest.matchers.booleans.*

class UnitDoubleTest : FreeSpec() {
    init {
        "Test equals()" - {
            "predefined units" {
                `°C`.shouldBe(`°C`)
                K.shouldNotBe(`°C`)

                g.shouldNotBe(Unit)
                g.shouldBe(g)
                g.shouldNotBe(ScaledUnit("", "", g.quantity, g.dimension, "1000".toBFN()))
            }

            "predefined & SI units" {
            }
        }

        "Test isEquivalentTo()" {
            g.isEquivalentTo(m).shouldBeFalse()

            g.isEquivalentTo(ScaledUnit("", "", g.quantity, g.dimension)).shouldBeTrue()
            t.isEquivalentTo(g).shouldBeTrue()
            m.isEquivalentTo(AU).shouldBeTrue()
        }

        "times a" {
            val q1 = Quantity.from(L = -2, M = 1)
            val u1 = ScaledUnit("U1", "U1", q1, Combined)
            val q3 = Quantity.from(T = 2)
            val u3 = ScaledUnit("U3", "U3", q3, Combined)

            val uu = (u1 * u3).first
            uu.quantity.toString() shouldBe "L⁻²·M·T²"
            uu.symbol shouldBe "[m⁻² g s²]"
            uu.toString() shouldBe "[m⁻² g s²]"

            val udu = (u1 / u3).first
            udu.quantity.toString() shouldBe "L⁻²·M·T⁻²"
            udu.symbol shouldBe "[m⁻² g s⁻²]"
            udu.toString() shouldBe "[m⁻² g s⁻²]"
        }

        "times b" {
            println("1".toSuperScript())
            val q1 = Quantity.from(L = -2, M = 1)
            val u1 = ScaledUnit("U1", "U1", q1, Combined)
            val q3 = Quantity.from(T = 2)
            val u3 = ScaledUnit("U3", "U3", q3, Combined)
            UnitTable.add(ScaledUnit("U1 U3", "U1 U3", q1 * q3, Combined))
            UnitTable.add(ScaledUnit("U1 U3⁻", "U1 U3⁻", q1 / q3, Combined))

            val uu = (u1 * u3).first
            uu.quantity.toString() shouldBe "L⁻²·M·T²"
            uu.symbol shouldBe "[m⁻² g s²]"
            uu.toString() shouldBe "[m⁻² g s²]"

            val udu = (u1 / u3).first
            udu.quantity.toString() shouldBe "L⁻²·M·T⁻²"
            udu.symbol shouldBe "[m⁻² g s⁻²]"
            udu.toString() shouldBe "[m⁻² g s⁻²]"
        }

        "base units" {
            N.symbol shouldBe "N"
        }
    }
}
