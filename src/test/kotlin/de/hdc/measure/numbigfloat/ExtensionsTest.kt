@file:Suppress("DEPRECATION")

package de.hdc.measure.numbigfloat

import ch.obermuhlner.math.big.*
import de.hdc.measure.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*
import org.junit.jupiter.api.Assertions.*

class ExtensionsTest : FreeSpec() {

    init {

        "approximate double" {
            1.000000000000001 shouldNotBe 1.0

            should { 1.0 approximates 1.0 }
            should { 0.999999999999991 approximates 1.0 }
            should { 1.000000000000009 approximates 1.0 }

            should { !(0.999999999999990 approximates 1.0) }
            should { !(1.000000000000010 approximates 1.0) }

            should { 999.9999999999991 approximates 1000.0 }
            should { 1000.0000000000009 approximates 1000.0 }

            should { !(999.99999999999990 approximates 1000.0) }
            should { !(1000.00000000000010 approximates 1000.0) }

            should { 1.0.approximates(1.0, 0.000000000000001) }
            should { 0.999999999999991.approximates(1.0, 0.000000000000001) }
            should { 1.000000000000009.approximates(1.0, 0.000000000000001) }
        }

        "approximate bigfloat" {
            fun String.toBF() = BigFloat.context(20).valueOf(this)
            val md = "0.000000000000001".toBF()

            "1.000000000000001".toBF() shouldNotBe "1.0".toBF()

            should { "1.0".toBF().approximates("1.0".toBF(), md) }
            should { "0.999999999999991".toBF().approximates("1.0".toBF(), md) }
            should { "1.000000000000009".toBF().approximates("1.0".toBF(), md) }

            should { !("0.999999999999990".toBF().approximates("1.0".toBF(), md)) }
            should { !("1.000000000000010".toBF().approximates("1.0".toBF(), md)) }

            should { "999.999999999999991".toBF().approximates("1000.0".toBF(), md) }
            should { "1000.000000000000009".toBF().approximates("1000.0".toBF(), md) }

            should { !("999.999999999999990".toBF().approximates("1000.0".toBF(), md)) }
            should { !("1000.000000000000010".toBF().approximates("1000.0".toBF(), md)) }

            should { "1.0".toBF().approximates("1.0".toBF(), md) }
            should { "0.999999999999991".toBF().approximates("1.0".toBF(), md) }
            should { "1.000000000000009".toBF().approximates("1.0".toBF(), md) }
        }

        "format" {
            assertEquals("NaN", Double.NaN.format())

            assertEquals("3.123", 3.12345.format())

            assertEquals("-0", (-0.0).format(0u))
            assertEquals("3", 3.12345.format(0u))
            assertEquals("3.1", 3.12345.format(1u))
            assertEquals("3.123 5", 3.12345.format(4u))

            assertEquals("-3.123", (-3.12345).format())
            assertEquals("-3", (-3.12345).format(0u))
            assertEquals("-3.1", (-3.12345).format(1u))
            assertEquals("-3.123 5", (-3.12345).format(4u))

            assertEquals("0.123", 0.12345.format())
            assertEquals("1.123", 1.12345.format())
            assertEquals("12.123", 12.12345.format())
            assertEquals("123.123", 123.12345.format())
            assertEquals("1 234.123", 1234.12345.format())
            assertEquals("12 345.123", 12345.12345.format())
            assertEquals("123 456.123", 123456.12345.format())
            assertEquals("1 234 567.123", 1234567.12345.format())
            assertEquals("12 345 678.123", 12345678.12345.format())
            assertEquals("123 456 789.123", 123456789.12345.format())

            assertEquals("-0.123", (-0.12345).format())
            assertEquals("-1.123", (-1.12345).format())
            assertEquals("-12.123", (-12.12345).format())
            assertEquals("-123.123", (-123.12345).format())
            assertEquals("-1 234.123", (-1234.12345).format())
            assertEquals("-12 345.123", (-12345.12345).format())
            assertEquals("-123 456.123", (-123456.12345).format())
            assertEquals("-1 234 567.123", (-1234567.12345).format())
            assertEquals("-12 345 678.123", (-12345678.12345).format())
            assertEquals("-123 456 789.123", (-123456789.12345).format())

            assertEquals("0.1", 0.1.format(1u))
            assertEquals("0.12", 0.12.format(2u))
            assertEquals("0.123", 0.123.format(3u))
            assertEquals("0.123 4", 0.1234.format(4u))
            assertEquals("0.123 45", 0.12345.format(5u))
            assertEquals("0.123 456", 0.123456.format(6u))
            assertEquals("0.123 456 7", 0.1234567.format(7u))
            assertEquals("0.123 456 78", 0.12345678.format(8u))
            assertEquals("0.123 456 789", 0.123456789.format(9u))
        }

        "testInvalid" {
            (BigFloat.NaN.toBFN() ˍ m).isInvalid() shouldBe true
            (BigFloat.NEGATIVE_INFINITY.toBFN() ˍ m).isInvalid() shouldBe true
            (BigFloat.POSITIVE_INFINITY.toBFN() ˍ m).isInvalid() shouldBe true
        }

//        "div" {
//        }

        "times" {
            (("12.0" ˍ UNITLESS) * ("6.0" k g)) shouldBe ("72.0" k g)
            (("12.0" ˍ UNITLESS) * ("6.0" k m)) shouldBe ("72.0" k m)

            (12 * ("6.0" ˍ g)) shouldBe ("72.0" ˍ g)
            (12.0 * ("6.0" M g)) shouldBe ("72.0" M g)

            ("6.0" ˍ t).baseValue.value.toDouble() shouldBe 6.0E6
            (12.0 * ("6.0" ˍ t)).baseValue.value.toDouble() shouldBe 7.2E7
            ("72.0" ˍ t).baseValue.value.toDouble() shouldBe 7.2E7
            (12.0 * ("6.0" ˍ t)) shouldBe ("72.0" ˍ t)
            (12.0 * ("6.0" k t)) shouldBe ("72.0" k t)

            (12.0 * ("6.0" k g)) shouldBe ("72.0" k g)
            (12.0 * ("6.0" k m)) shouldBe ("72.0" k m)

            (12.0f * ("6.0" k g)) shouldBe ("72.0" k g)
            (12.0f * ("6.0" k m)) shouldBe ("72.0" k m)

            (12 * ("6.0" k g)) shouldBe ("72.0" k g)
            (12 * ("6.0" k m)) shouldBe ("72.0" k m)

            (12L * ("6.0" k g)) shouldBe ("72.0" k g)
            (12L * ("6.0" k m)) shouldBe ("72.0" k m)


            (("6.0" k g) * 12.0) shouldBe ("72.0" k g)
            (("6.0" k m) * 12.0) shouldBe ("72.0" k m)

            (("6.0" k g) * 12.0f) shouldBe ("72.0" k g)
            (("6.0" k m) * 12.0f) shouldBe ("72.0" k m)

            (("6.0" k g) * 12) shouldBe ("72.0" k g)
            (("6.0" k m) * 12) shouldBe ("72.0" k m)

            (("6.0" k g) * 12L) shouldBe ("72.0" k g)
            (("6.0" k m) * 12L) shouldBe ("72.0" k m)
        }
    }
}
