@file:Suppress("DEPRECATION")

package de.hdc.measure.numbigfloat

import ch.obermuhlner.math.big.BigFloat.context
import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.core.DecimalPrefix.*
import de.hdc.measure.h
import de.hdc.measure.m
import de.hdc.measure.numbigfloat.BigFloatNumber.Companion.defaultContext
import io.kotest.assertions.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*
import io.kotest.matchers.doubles.*
import org.junit.jupiter.api.Assertions.*

internal class UnitNumberBFTest : FreeSpec() {

    init {
        "toDouble" {
            (("3.0" ˍ min) toUnit s).format(0u, 0u, false) shouldBe "180 s"
            (("3.0" ˍ min) toUnit s).printValue.toDouble() shouldBe 180.0
            (("3.0" ˍ min) toUnit s).printValue.toInt() shouldBe 180
        }

        "numbers of different precision" {
            "3.04" k g shouldBe UnitNumber(
                BigFloatNumber(context(15).valueOf("3040")), g
            )
            ("0.0" ˍ m) shouldBe ("-0.0" ˍ m)
            ("1000" ˍ m) shouldBe ("1" k m)
            ("0.002" ˍ m3) shouldBe ("2" m m3)
            ("0.000000002" ˍ m3) shouldNotBe ("2" m m3)
            ("1.0" ˍ m) * ("1.0" ˍ m) shouldBe ("1.0" ˍ m2)
        }

        "main" {
            ("1_000" ˍ m2) toUnit m2 shouldBe ("1_000" ˍ m2)
            ("1" k m2) toUnit m2 shouldBe ("1_000" ˍ m2)
            ("1_000" ˍ m2) toUnit m2 shouldBe ("1" k m2)

            ("1_000" ˍ m3) toUnit m3 shouldBe ("1_000" ˍ m3)
            ("1" k m3) toUnit m3 shouldBe ("1_000" ˍ m3)
            ("1_000" ˍ m3) toUnit m3 shouldBe ("1" k m3)

            ("1" ˍ m3).shouldBe("1000" ˍ L)
            ("1000" ˍ L).shouldBe("1" ˍ m3)
            (("100" m L) toUnit L).shouldBe("0.1" ˍ L)
            (("0.1" ˍ L) toUnit L).shouldBe("100" m L)

            assertEquals(
                ("1000.0" ˍ `°C`).toDouble(),
                (("1273.15" ˍ K) toUnit `°C`).toDouble(),
                0.0001
            )
            assertEquals("1.2" ˍ min, "1.2" ˍ min)

            // test convertTo()
            assertEquals("1.0" ˍ min, ("60.0" ˍ s) toUnit min)
            assertEquals("60.0" ˍ s, ("1.0" ˍ min) toUnit s)

            assertEquals("60.0" ˍ min, ("1.0" ˍ h) toUnit min)
            assertEquals("1.0" ˍ h, ("60.0" ˍ min) toUnit h)

            assertEquals(("1000.0" ˍ min), ("60.0" k s) toUnit min)

            assertEquals(("273.15" ˍ K), ("0.0" ˍ `°C`) toUnit K)
            assertEquals(("273.15" ˍ K) toUnit `°C`, ("0.0" ˍ `°C`))

            ("1000" ˍ `°C`) shouldBe (("1.0" k `°C`) toUnit `°C`)
            ("0.001" M `°C`) shouldBe (("1.0" k `°C`) toUnit `°C`)
            ("1000" ˍ `°C`) shouldBe ("1.0" k `°C`)
            ("0.001" M `°C`) shouldBe ("1.0" k `°C`)

            ("1000.0000000000001" ˍ `°C`) shouldNotBe ("1.0" k `°C`)
            ("1000.0000000000001" ˍ `°C`).approximates(
                ("1.0" k `°C`), 1e-10.toBFN()
            ) shouldBe true

            "1273.15" ˍ K shouldBe (("1.0" k `°C`) toUnit K)
            (("1273.15" ˍ K) toUnit `°C`)
                .toDouble() shouldBe (("1000.0" ˍ `°C`).toDouble() plusOrMinus 0.0001)

            // Should be flagged by linter when it's ready
//            (("1.0" k g) * ("1.0" ˍ m)) to (g)
//            ("1.0" ˍ N) shouldBe (((("1.0" k g) * ("1.0" ˍ m)) / ("1.0" ˍ s)) / ("1.0" ˍ s)) to (N)
//            ("1.0" ˍ N) shouldBe
//                    ((((("1.0" k g) * ("1.0" ˍ m)) / ("1.0" ˍ s)) / ("1.0" ˍ s)) to N)
//            ("1.0" ˍ N) shouldBe
//                    ((((("1.0" k g) * ("1.0" ˍ m)) / ("1.0" ˍ s)) / ("1.0" ˍ s)) to N)

            // test rec()
            assertSoftly {
                ("0.0" ˍ min).reciprocal().baseValue.toDouble() shouldBe Double.POSITIVE_INFINITY
                ("0.0" ˍ min).reciprocal().isInfinite() shouldBe true
            }

            // test inverse()
            ("-0.0" ˍ m) shouldBe ("0.0" ˍ m).inverse()
            ("-1.0" ˍ m) shouldBe ("1.0" ˍ m).inverse()
            ("1.0" ˍ m) shouldBe ("-1.0" ˍ m).inverse()

            // test plus()
            // test minus()
            // test scalar(Double)
            // test scalar(Measure<SI_NONE>)
            // test times()
            // test div()

            // test toString()
            assertEquals("1.23", ("1.23" ˍ UNITLESS).format(2u, 0u))
            assertEquals("-1.23", ("-1.23" ˍ UNITLESS).format(2u, 0u))
            assertEquals("1.23${Typography.nbsp}kg", ("1.23" k g).format(2u, 0u))
            assertEquals("-1.23${Typography.nbsp}kg", ("-1.23" k g).format(2u, 0u))
            assertEquals("1.23${Typography.nbsp}[m s⁻²]", ("1.23" ˍ m_s2).format(2u, 0u))

            // test compareTo()
            assertSoftly {
                ("1.0" k g).compareTo("1.0" ˍ g) shouldBe 1
                ("1.0" P g).compareTo("1.0" Y g) shouldBe -1
                ("1.0" k g).compareTo("1.0" k g) shouldBe 0
                ("1.0" k g).compareTo("2.0" k g) shouldBe -1
            }

            // test toDouble()
            ("1.0" k g).baseValue.toDouble() shouldBe 1_000.0
            ("1.0" ˍ t).baseValue.toDouble() shouldBe 1_000_000.0
            ("1.0" ˍ g).baseValue.toDouble() shouldBe 1.0
            ("0.0" ˍ g).baseValue.toDouble() shouldBe 0.0
            ("-1.0" ˍ g).baseValue.toDouble() shouldBe -1.00
            ("1.0" G g).baseValue.toDouble() shouldBe 1.0e9

            val kg = ("1.0" k g)
//            println("kg: ${kg.unit.converter}\nk..g: ${(k..g).converter}")
            kg.unit shouldBe (k..g)
            kg.printValue.toDouble() shouldBe 1.0
            kg.toDouble() shouldBe 1_000.0
            ("1.0" ˍ t).toDouble() shouldBe 1.0
            ("1.0" ˍ g).toDouble() shouldBe 1.0
            ("0.0" ˍ g).toDouble() shouldBe 0.0
            ("-1.0" ˍ g).toDouble() shouldBe -1.00
            ("1.0" G g).toDouble() shouldBe 1_000_000_000.0

        }

        "format" {
            val n = Typography.nbsp
            assertEquals("3.123${n}kg", ("3.12345" k g).format(padding = 0u))
            assertEquals("   3.123${n}kg", ("3.12345" k g).format())

            assertEquals("   0${n}g", ("-0.0" k g).format(0u))
            assertEquals("   3${n}kg", ("3.123456" k g).format(0u))
            assertEquals("   3.1${n}kg", ("3.123456" k g).format(1u))
            assertEquals("   3.123 5${n}kg", ("3.123456" k g).format(4u))

            assertEquals("  -3.123${n}kg", ("-3.123456" k g).format())
            assertEquals("  -3${n}kg", ("-3.123456" k g).format(0u))
            assertEquals("  -3.1${n}kg", ("-3.123456" k g).format(1u))
            assertEquals("  -3.123 5${n}kg", ("-3.123456" k g).format(4u))

            assertEquals(" 123.450${n}g", ("0.12345" k g).format())
            assertEquals("   1.123${n}kg", ("1.12345" k g).format())
            assertEquals("  12.123${n}kg", ("12.12345" k g).format())
            assertEquals(" 123.123${n}kg", ("123.12345" k g).format())
            assertEquals("   1.234${n}Mg", ("1234.12345" k g).format())
            assertEquals("   1.234${n}t", ("1234.12345" k g).format(optimize = true))
            assertEquals("  12.345${n}t", ("12345.12345" k g).format(optimize = true))
            assertEquals(" 123.456${n}t", ("123456.12345" k g).format(optimize = true))
            assertEquals("   1.235${n}kt", ("1234567.12345" k g).format(optimize = true))
            assertEquals("  12.346${n}kt", ("12345678.12345" k g).format(optimize = true))
            assertEquals(" 123.457${n}kt", ("123456789.12345" k g).format(optimize = true))

            assertEquals("-123.450${n}g", ("-0.12345" k g).format())
            assertEquals("  -1.123${n}kg", ("-1.12345" k g).format())
            assertEquals(" -12.123${n}kg", ("-12.12345" k g).format())
            assertEquals("-123.123${n}kg", ("-123.12345" k g).format())
            assertEquals("  -1.234${n}Mg", ("-1234.12345" k g).format())
            assertEquals("  -1.234${n}t", ("-1234.12345" k g).format(optimize = true))
            assertEquals(" -12.345${n}t", ("-12345.12345" k g).format(optimize = true))
            assertEquals("-123.456${n}t", ("-123456.12345" k g).format(optimize = true))
            assertEquals("  -1.235${n}kt", ("-1234567.12345" k g).format(optimize = true))
            assertEquals(" -12.346${n}kt", ("-12345678.12345" k g).format(optimize = true))
            assertEquals("-123.457${n}kt", ("-123456789.12345" k g).format(optimize = true))

            assertEquals(" 100.0${n}g", ("0.1" k g).format(1u))
            assertEquals(" 120.00${n}g", ("0.12" k g).format(2u))
            assertEquals(" 123.000${n}g", ("0.123" k g).format(3u))
            assertEquals(" 123.4${n}g", ("0.1234" k g).format(1u))
            assertEquals(" 123.45${n}g", ("0.12345" k g).format(2u))
            assertEquals(" 123.456${n}g", ("0.123456" k g).format(3u))
            assertEquals(" 123.456 7${n}g", ("0.1234567" k g).format(4u))
            assertEquals(" 123.456 78${n}g", ("0.12345678" k g).format(5u))
            assertEquals(" 123.456 789${n}g", ("0.123456789" k g).format(6u))

            ("5" M g).format(padding = 0u, decimals = 1u, optimize = true) shouldBe "5.0${n}t"

            (("1" ˍ m2) / ("0" ˍ m)).unit.quantity shouldBe Quantity.from(L = 1)
//            (("1" ˍ m2) / ("0" ˍ m)).unit.name shouldBe "Meter"
            (("1" ˍ m2) / ("0" ˍ m)).unit.symbol shouldBe "m"
            (("1" ˍ m2) / ("0" ˍ m)).toString() shouldBe "∞${n}m"
            (("-1" ˍ m2) / ("0" ˍ m)).toString() shouldBe "∞${n}m"
            (("0" ˍ m2) / ("0" ˍ m)).toString() shouldBe "NaN${n}m"
        }

        "calculations" {
            (("10.0" k g) / ("5.0" ˍ g)) shouldBe ("2000.0" ˍ UNITLESS)
            ((("10.0" k g) / ("5.0" ˍ g)) * ("3" ˍ s)) shouldBe ("6000.0" ˍ s)

            (("10.0" k g) / ("5.0" ˍ g) * ("3.6" ˍ s)).format(padding = 0u, decimals = 1u, optimize = true) shouldBe
                    "2.0 h"
            (("10.0" k g) / ("5.0" ˍ g) * ("3" ˍ s)) toUnit min shouldBe ("100.0" ˍ min)
            (("10.0" k m) * ("10.0" k m)) shouldBe ("1.0e8" ˍ m2)

            (("12" ˍ m) / 2.0.toBFN()) shouldBe ("6" ˍ m)
            (("12" ˍ m) / ("6.0" ˍ m)) shouldBe
                    ("2" ˍ ScaledUnit("", "", Quantity.from(), Dimensionless))

            (("12" k m) / 2.0.toBFN()) shouldBe ("6" k m)
            (("12" k m) / ("6.0" k m)) shouldBe
                    ("2" ˍ ScaledUnit("", "", Quantity.from(), Dimensionless))

            (("12" k g) / 2.0.toBFN()) shouldBe ("6" k g)
            (("12" k g) / ("6.0" k g)) shouldBe
                    ("2" ˍ ScaledUnit("", "", Quantity.from(), Dimensionless))

            // Measures as scalars
            ("12" ˍ m) / ("2" ˍ UNITLESS) shouldBe ("6" ˍ m)
            ("12" k m) / ("2" ˍ UNITLESS) shouldBe ("6" k m)
            ("12" k m) / ("2" k UNITLESS) shouldBe ("6" ˍ m)

            ("12" ˍ m) * ("2" ˍ UNITLESS) shouldBe ("24" ˍ m)
            ("12" k m) * ("2" ˍ UNITLESS) shouldBe ("24" k m)
            ("12" k m) * ("2" k UNITLESS) shouldBe ("24" M m)

            ("72" ˍ km_h).toString() shouldBe "72.0 [km h⁻¹]"
            (("20" ˍ m_s) toUnit km_h).toString() shouldBe "72.0 [km h⁻¹]"
        }

        "atan2" {
            BigFloatNumber.atan2(defaultContext.valueOf(0), defaultContext.valueOf(0)).toDouble() shouldBe
                    0.0
            BigFloatNumber.atan2(defaultContext.valueOf(90), defaultContext.valueOf(15)).toDouble() shouldBe
                    1.4056476493802699.plusOrMinus(0.00000000000001)
            BigFloatNumber.atan2(defaultContext.valueOf(-90), defaultContext.valueOf(15)).toDouble() shouldBe
                    (-1.4056476493802699).plusOrMinus(0.00000000000001)
            BigFloatNumber.atan2(defaultContext.valueOf(-90), defaultContext.valueOf(-15)).toDouble() shouldBe
                    (-1.73594500421).plusOrMinus(0.000000000001)
            BigFloatNumber.atan2(defaultContext.valueOf(90), defaultContext.valueOf(-15)).toDouble() shouldBe
                    1.73594500421.plusOrMinus(0.000000000001)

            BigFloatNumber.atan2(defaultContext.valueOf(15), defaultContext.valueOf(90)).toDouble() shouldBe
                    0.16514867741462683.plusOrMinus(0.00000000000001)
            BigFloatNumber.atan2(defaultContext.valueOf(-15), defaultContext.valueOf(90)).toDouble() shouldBe
                    (-0.16514867741462683).plusOrMinus(0.00000000000001)
            BigFloatNumber.atan2(defaultContext.valueOf(-15), defaultContext.valueOf(-90)).toDouble() shouldBe
                    (-2.976443976175).plusOrMinus(0.000000000001)
            BigFloatNumber.atan2(defaultContext.valueOf(15), defaultContext.valueOf(-90)).toDouble() shouldBe
                    (2.976443976175).plusOrMinus(0.000000000001)

            BigFloatNumber.atan2(defaultContext.valueOf(5), defaultContext.valueOf(5)).toDouble() shouldBe
                    0.785398163397.plusOrMinus(0.00000000001)
            BigFloatNumber.atan2(defaultContext.valueOf(0), defaultContext.valueOf(10)).toDouble() shouldBe
                    0.0.plusOrMinus(0.0001)
        }

        "precision test" {
            UnitNumber(defaultContext.pi().toBFN(), UNITLESS).getValue() shouldBe defaultContext.pi()
            UnitNumber("1.0".toBFN() / "3.0".toBFN(), UNITLESS).getValue().toString().length shouldBe
                    defaultContext.precision + 2
        }
    }
}
