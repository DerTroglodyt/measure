package de.hdc.measure.numbigfloat

import de.hdc.measure.core.BinaryPrefix
import de.hdc.measure.core.DecimalPrefix
import de.hdc.measure.core.NumberName
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class PrefixBigFloatTest : FreeSpec() {
    init {
        "const" {
            DecimalPrefix.Y.multiplier.toString() shouldBe "1E+24"
            DecimalPrefix.y.multiplier.toString() shouldBe "1E-24"

            BinaryPrefix.YOBI.multiplier.toString() shouldBe "1.024E+11"

            DecimalPrefix.Y.multiplier.value shouldBe NumberName.OKTILLION.multiplier
        }
    }
}
