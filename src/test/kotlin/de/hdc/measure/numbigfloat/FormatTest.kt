package de.hdc.measure.numbigfloat

import io.kotest.core.spec.style.*
import de.hdc.measure.g
import de.hdc.measure.numdouble.*
import de.hdc.measure.numdouble.ˍ
import io.kotest.matchers.shouldBe

internal class FormatTest : FreeSpec() {

    init {
        "main" {
            val n = Typography.nbsp

            (230.0 ˍ g).format() shouldBe " 230.000${n}g"
            (-230.0 ˍ g).format() shouldBe "-230.000${n}g"
            (2300.0 ˍ g).format() shouldBe "   2.300${n}kg"
            (0.0023 ˍ g).format() shouldBe "   2.300${n}mg"
            (2300.0 k g).format() shouldBe "   2.300${n}Mg"
            (2300.0 k g).format(optimize = true) shouldBe "   2.300${n}t"
            (2300.0 M g).format(optimize = true) shouldBe "   2.300${n}kt"
            (2300.0 X g).format(optimize = true) shouldBe "   2.300${n}Pt"
            (2300.0 Z g).format(optimize = true) shouldBe "   2.300${n}Xt"
            (2300000.0 Z g).format(optimize = true) shouldBe "   2.300${n}Zt"
            (2300000.0 Y g).format(optimize = true) shouldBe "   2.300${n}Yt"
            (2300000000.0 Y g).format(optimize = true) shouldBe "2 300.000${n}Yt"
            (2300.0 Y g).format(optimize = true) shouldBe "   2.300${n}Zt"
            (23000000.0 Y g).format(optimize = true) shouldBe "  23.000${n}Yt"
            (0.23 y g).format() shouldBe "   0.230${n}yg"
            (0.00023 y g).format() shouldBe "   0.000${n}yg"

            // no digits
            (230000.0 k g).format(0u, 5u, optimize = true) shouldBe "  230${n}t"
        }
    }
}
