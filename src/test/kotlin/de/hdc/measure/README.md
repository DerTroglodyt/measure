import de.hdc.measure.*
import de.hdc.measure.numdouble.*

val distance = 12.53 k m
val x = distance + (45.67 c m)
// pretty printing by default
println(x)
// 12.5 km

// formatted printing
println(x.format(7, 0, false))
// 12.530 456 7 km

// conversion to other unit
println((x toUnit m).format(4, 0, false))
// 12 530.456 7 m

val celsius = 32.0 ˍ `°C`
val kelvin = celsius toUnit K
println("$celsius == $kelvin")
// 32.0 °C == 305.2 K

// Ensure type safety of parameters and return types:
fun foo(distance: UnitNumberDouble<Length>, elapsed: UnitNumberDouble<Time>): UnitNumberDouble<Velocity> {
    return (distance / elapsed) as UnitNumberDouble<Velocity>
}

//import de.hdc.measure.*
//import de.hdc.measure.money.*
//import de.hdc.measure.numdouble.*
//import de.hdc.measure.numbigfloat.*
//import de.hdc.measure.numinteger.*

//val a: UnitNumberDouble<Length> = 3.41 c m
//val c: UnitNumberDouble<Length> = 0.001 k m
//val b: UnitNumberLong<Length> = 34 ˍ m
//val hp: UnitNumberBF<Length> = "1.23456789012345678901234567890" k m
//val dollar: Money<USD> = "15.99" ˍ USD
