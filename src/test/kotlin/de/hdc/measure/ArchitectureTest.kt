package de.hdc.measure

import com.lemonappdev.konsist.api.Konsist
import com.lemonappdev.konsist.api.architecture.KoArchitectureCreator.assertArchitecture
import com.lemonappdev.konsist.api.architecture.Layer
import com.lemonappdev.konsist.api.ext.list.*
import com.lemonappdev.konsist.api.verify.*
import io.kotest.core.spec.style.FreeSpec

// todo does not work
//class ArchitectureTest : FreeSpec({
//
//    "test package dependencies" {
//        production.assertArchitecture {
//            val root = Layer("Root", "de.hdc.measure..")
//            val core = Layer("Core", "de.hdc.measure.core..")
//            val money = Layer("Money", "de.hdc.measure.money..")
//            val bigfloatNum = Layer("BigFloat", "de.hdc.measure.numbigfloat..")
//            val doubleNum = Layer("Double", "de.hdc.measure.numdouble..")
//            val integerNum = Layer("Integer", "de.hdc.measure.numinteger..")
//            val other = Layer("Other", "de.hdc.measure.other..")
//
//            root.dependsOn(other)
//            core.dependsOn(root, bigfloatNum)
//            money.dependsOn(root)
//            bigfloatNum.dependsOn(root, core, other)
//            doubleNum.dependsOn(root, core, other)
//            integerNum.dependsOn(root, core, other)
//            other.dependsOn(root, core, bigfloatNum)
//        }
//    }
//
//    "no class should use Java util logging" {
//        Konsist
//            .scopeFromProject()
//            .files
//            .assertFalse { it.hasImport { import -> import.name == "java.util.logging.." } }
//    }
//
//    "package name must match file path" {
//        Konsist
//            .scopeFromProject()
//            .packages
//            .assertTrue { it.hasMatchingPath }
//    }
//
//    "only wildcard imports allowed" {
//        Konsist
//            .scopeFromProject()
//            .imports
//            .assertTrue { it.isWildcard }
//    }
//
//    "print all public classes" {
//        @Suppress("ReplacePrintlnWithLogging")
//        println("Public classes:")
//        production
//            .classes()
//            .filter {
//                it.hasPublicModifier
//            }
//            .print()
//    }
//}) {
//    companion object {
//        private val production = Konsist.scopeFromProduction() -
//                Konsist.scopeFromModule("lint") -
//                Konsist.scopeFromModule("tools")
//    }
//}
