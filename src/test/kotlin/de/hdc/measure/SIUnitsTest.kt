package de.hdc.measure

import de.hdc.measure.core.*
import de.hdc.measure.core.ScaledUnit.Companion.from
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class SIUnitsTest : FreeSpec({

    "UnitTable.findUnit" {
        UnitTable[from(listOf(pcs), listOf(N)).quantity, Force] shouldBe null
        UnitTable[from(listOf(m), listOf(s, s)).quantity, Acceleration] shouldBe m_s2
        UnitTable[Quantity.from(L = 1, M = 1, T = -2), Force] shouldBe N
    }
})
