@file:Suppress("DEPRECATION")

package de.hdc.measure

import de.hdc.measure.numbigfloat.*
import de.hdc.measure.numdouble.k
import de.hdc.measure.numdouble.m
import de.hdc.measure.numdouble.ˍ
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class ImperialUnitsTest : FreeSpec({
    "temperature" {
        (-459.67 ˍ `°F`).baseValue.value shouldBe 0.0
        (-459.67 ˍ `°F`) toUnit K shouldBe (0.0 ˍ K)
        (0.0 ˍ K) toUnit `°F` shouldBe (-459.67 ˍ `°F`)

        (-273.15 ˍ `°C`) toUnit K shouldBe (0.0 ˍ K)
        (0.0 ˍ K) toUnit `°C` shouldBe (-273.15 ˍ `°C`)

        (-40.0 ˍ `°F`) toUnit `°C` shouldBe (-40.0 ˍ `°C`)
        (-40.0 ˍ `°C`) toUnit `°F` shouldBe (-40.0 ˍ `°F`)

        (212.0 ˍ `°F`) toUnit `°C` shouldBe (100.0 ˍ `°C`)
        (100.0 ˍ `°C`) toUnit `°F` shouldBe (212.0 ˍ `°F`)

        (-40.0 ˍ `°F`).baseValue.value shouldBe 233.15
        (-40.0 ˍ `°F`).printValue.getValue() shouldBe -40.0
    }

    "mass" {
        (1.0 ˍ gr) shouldBe (64.79891 m g)
        (64.79891 m g) shouldBe (1.0 ˍ gr)

        (1.0 ˍ oz) shouldBe (28.349523125 ˍ g)
        (28.349523125 ˍ g) shouldBe (1.0 ˍ oz)

        (1.0 ˍ lb) shouldBe (0.45359237 k g)
        (0.45359237 k g) shouldBe (1.0 ˍ lb)

        (1.0 ˍ st) shouldBe (1.0 ˍ st)
        ("1.0" ˍ st) shouldBe ("6350.29318" ˍ g)
        ("1.0" ˍ st) shouldBe ("6.35029318" m t)
        ("1.0" ˍ st) shouldBe ("6.35029318" k g)
        ("6.35029318" k g) shouldBe ("1.0" ˍ st)
    }

    "length" {
        ("1.0" ˍ `in`) shouldBe ("2.54" c m)
        ("1.0" ˍ `in`) shouldBe ("0.0254" ˍ m)
        ("1.0" ˍ th) shouldBe ("0.0254" m m)
        ("1.0" ˍ ft) shouldBe ("30.48" c m)
        ("1.0" ˍ ft) shouldBe ("0.3048" ˍ m)
        ("1.0" ˍ yd) shouldBe ("0.9144" ˍ m)
        ("1.0" ˍ mi) shouldBe ("1609.344" ˍ m)
        ("1.0" ˍ nmi) shouldBe ("1852" ˍ m)
    }

    "area" {
        ("1.0" ˍ sq_mi) shouldBe ("2589988.110336" ˍ m2)
    }

    "volume" {
        ("1.0" ˍ fl_oz) shouldBe ("28.4130625" m L)
        ("1.0" ˍ pt) shouldBe ("568.26125" m L)
        ("1.0" ˍ gal) shouldBe ("4.54609" ˍ L)
    }
})
