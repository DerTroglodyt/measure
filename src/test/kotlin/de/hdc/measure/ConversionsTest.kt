@file:Suppress("DEPRECATION")

package de.hdc.measure

import de.hdc.measure.numdouble.k
import de.hdc.measure.numdouble.ˍ
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class ConversionsTest : FreeSpec() {
    init {
//        val n = Typography.nbsp

        "multiplication" {
            (2.0 ˍ m) * (3.0 ˍ m) shouldBe (6.0 ˍ m2)
            (2.0 ˍ m) * (3.0 ˍ m) shouldBe (6.0 ˍ m2)

            (2.0 ˍ m) * (6.0 ˍ m2) shouldBe (12.0 ˍ m3)
            (2.0 ˍ m2) * (6.0 ˍ m) shouldBe (12.0 ˍ m3)

            (2.0 k g).baseValue.value shouldBe 2000.0
            (2.0 k g) * (9.81 ˍ m_s2) shouldBe (19.62 ˍ N)
            (2.0 ˍ N) * (3.0 ˍ m) shouldBe (6.0 ˍ J)
            (2.0 ˍ J) * (3.0 ˍ s) shouldBe (6.0 ˍ Js)
            (2.0 ˍ A) * (3.0 ˍ s) shouldBe (6.0 ˍ C)
        }

        "divisions" {
            (4.0 ˍ m) / (2.0 ˍ s) shouldBe (2.0 ˍ m_s)
            (4.0 ˍ m_s) / (2.0 ˍ s) shouldBe (2.0 ˍ m_s2)
            (4.0 ˍ N) / (2.0 ˍ m2) shouldBe (2.0 ˍ Pa)
            (4.0 ˍ J) / (2.0 ˍ s) shouldBe (2.0 ˍ W)
            (18.0 ˍ rad) / (3.0 ˍ s) shouldBe (6.0 ˍ ω)
            (18.0 ˍ ω) / (3.0 ˍ s) shouldBe (6.0 ˍ α)
            (18.0 k g) / (3.0 ˍ m3) shouldBe (6.0 ˍ kg_m3)
            (18.0 k g) / (3.0 ˍ mol) shouldBe (6.0 ˍ kg_mol)
            (18.0 ˍ mol) / (3.0 ˍ m3) shouldBe (6.0 ˍ mol_m3)
        }
    }
}
