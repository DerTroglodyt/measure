package de.hdc.measure.numinteger

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class UnitNumberLongTest : FreeSpec({
    "calculations" {
        (12 ˍ _root_ide_package_.de.hdc.measure.m) * 2 shouldBe (24 ˍ _root_ide_package_.de.hdc.measure.m)
        2 * (12 ˍ _root_ide_package_.de.hdc.measure.m) shouldBe (24 ˍ _root_ide_package_.de.hdc.measure.m)

        (12 ˍ _root_ide_package_.de.hdc.measure.m) / 2 shouldBe (6 ˍ _root_ide_package_.de.hdc.measure.m)
        (12 ˍ _root_ide_package_.de.hdc.measure.m) / 5 shouldBe (2 ˍ _root_ide_package_.de.hdc.measure.m)
    }
})
