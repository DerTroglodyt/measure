package de.hdc.measure

import de.hdc.measure.core.*
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.*
import io.kotest.matchers.collections.*

class ScaledUnitsTest : FreeSpec({

    "get" {
        val list = ScaledUnitTable[(Quantity.from(L = 1))]
        list shouldNotBe null
        list!! shouldContain AU
        list!! shouldContain ly

        ScaledUnitTable[(Quantity.from(L = 5))] shouldBe null
    }

    "add" {
        ScaledUnitTable[(Quantity.from(L = 4))] shouldBe null

        val su1 = ScaledUnit("Test1", "T1", Quantity.from(L = 4), Length)
        val su2 = ScaledUnit("Test2", "T2", Quantity.from(L = 4), Length)
        ScaledUnitTable.add(su1)
        ScaledUnitTable.add(su2)

        ScaledUnitTable[(Quantity.from(L = 4))] shouldBe listOf(su1, su2)
    }
})
