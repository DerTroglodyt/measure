package de.hdc.measure.tools

import de.hdc.measure.*
import de.hdc.measure.numbigfloat.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class HmsBFTest : FreeSpec() {
    init {
        "HmsBF" {
            ("0" ˍ `°`).toHMS() shouldBe HmsBF(0, 0, "0.0")
            ("90" ˍ `°`).toHMS() shouldBe HmsBF(6, 0, "0.0")
            ("180" ˍ `°`).toHMS() shouldBe HmsBF(12, 0, "0.0")
            ("270" ˍ `°`).toHMS() shouldBe HmsBF(18, 0, "0.0")
            ("360" ˍ `°`).toHMS() shouldBe HmsBF(0, 0, "0.0")
            ("540" ˍ `°`).toHMS() shouldBe HmsBF(12, 0, "0.0")

            ("5" ˍ `°`).toHMS() shouldBe HmsBF(0, 20, "0.0")
            ("0.15" ˍ `°`).toHMS() shouldBe HmsBF(0, 0, "36.0")
            ("5.15" ˍ `°`).toHMS() shouldBe HmsBF(0, 20, "36.0")
            ("185.15" ˍ `°`).toHMS() shouldBe HmsBF(12, 20, "36.0")
        }

        "toDegrees" {
            HmsBF(0, 0, "36.0").toDegrees().format(9u, 0u) shouldBe
                    ("0.15" ˍ `°`).format(9u, 0u)
            HmsBF(0, 20, "36.0").toDegrees().format(9u, 0u) shouldBe
                    ("5.15" ˍ `°`).format(9u, 0u)
            HmsBF(12, 20, "36.0").toDegrees().format(9u, 0u) shouldBe
                    ("185.15" ˍ `°`).format(9u, 0u)
            HmsBF(12, 21, "12.0").toDegrees().format(9u, 0u) shouldBe
                    ("185.3" ˍ `°`).format(9u, 0u)
        }

        "toSeconds" {
            HmsBF(1, 2, "3").toSeconds() shouldBe ("3723.0" ˍ s)
        }
    }
}
