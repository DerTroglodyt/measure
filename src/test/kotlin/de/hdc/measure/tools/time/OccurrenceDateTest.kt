package de.hdc.measure.tools.time

import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class OccurrenceDateTest : FreeSpec({
    "now" {
        OccurrenceDate.secondsToMJD(0) shouldBe (2_440_587.5 - 2_400_000.5).toLong()
    }
})
