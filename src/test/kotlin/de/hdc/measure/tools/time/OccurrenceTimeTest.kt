package de.hdc.measure.tools.time

import de.hdc.measure.*
import de.hdc.measure.numbigfloat.*
import io.kotest.assertions.throwables.*
import io.kotest.core.spec.style.*

class OccurrenceTimeTest : FreeSpec({
    "invalid parameter throws" {
        shouldThrow<IllegalArgumentException> {
            OccurrenceTime("-1" n s)
        }

        shouldThrow<IllegalArgumentException> {
            OccurrenceTime(24.toString() ˍ h)
        }

        shouldThrow<IllegalArgumentException> {
            OccurrenceTime(32.toString() ˍ h)
        }
    }

    "valid parameter" {
        shouldNotThrow<IllegalArgumentException> {
            OccurrenceTime((24.toString() ˍ h) - (1.toString() n s))
        }
    }
})
