package de.hdc.measure.tools

import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class SubSuperScriptTest : FreeSpec() {
    init {
        "no alteration" {
            SubSuperScript.toSuperScript("") shouldBe ""
            SubSuperScript.toSuperScript("Hans") shouldBe "Hans"

            SubSuperScript.toSubScript("") shouldBe ""
            SubSuperScript.toSubScript("Hans") shouldBe "Hans"

            "".toSuperScript() shouldBe ""
            "Hans".toSuperScript() shouldBe "Hans"

            "".toSubScript() shouldBe ""
            "Hans".toSubScript() shouldBe "Hans"
        }

        "with alteration" {
            SubSuperScript.toSuperScript("Ha0123456789+-ns") shouldBe "Ha⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻ns"

            SubSuperScript.toSubScript("Ha0123456789+-ns") shouldBe "Ha₀₁₂₃₄₅₆₇₈₉₊₋ns"

            "Ha0123456789+-ns".toSuperScript() shouldBe "Ha⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻ns"

            "Ha0123456789+-ns".toSubScript() shouldBe "Ha₀₁₂₃₄₅₆₇₈₉₊₋ns"
        }
    }
}
