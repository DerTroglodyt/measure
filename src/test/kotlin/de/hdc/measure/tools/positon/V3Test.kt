@file:Suppress("DEPRECATION")

package de.hdc.measure.tools.positon

import de.hdc.measure.*
import de.hdc.measure.tools.position.*
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.tools.*
import io.kotest.assertions.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*
import io.kotest.matchers.doubles.*
import kotlin.math.*

class V3Test : FreeSpec() {
    init {
        "constructor" {
            val v1 = Cartesian("12.0", "5.0", "8.0")
            v1.length.toDouble() shouldBe sqrt((12.0 * 12.0) + (5.0 * 5.0) + (8.0 * 8.0))
            v1.coordinateSystem shouldBe Cartesian.Companion.CoordinateSystem.CARTESIAN_RIGHT_HANDED
            v1.toString() shouldBe "(12.0 m, 5.0 m, 8.0 m)"

            val v2 = Cartesian("12.0", "5000.0", "0.008")
            v2.length.toDouble() shouldBe sqrt(12.0 * 12.0 + 5000.0 * 5000.0 + 0.008 * 0.008)
            v2.coordinateSystem shouldBe Cartesian.Companion.CoordinateSystem.CARTESIAN_RIGHT_HANDED

            val v3 = SphericalPos("10.0" ˍ m, "90.0" ˍ `°`, "45.0" ˍ `°`)
            v3.r shouldBe ("10.0" ˍ m)
            v3.azimuth shouldBe ("90.0" ˍ `°`)
            v3.inclination shouldBe ("45.0" ˍ `°`)

            val v4 = GeoPos("1.0" ˍ m, "90.0" ˍ `°`, "45.0" ˍ `°`)
            v4.length shouldBe ("1.0" ˍ m)
            v4.longitude shouldBe ("90.0" ˍ `°`)
            v4.latitude shouldBe ("45.0" ˍ `°`)

            val v5 = AstroPos("1.0" ˍ m, "6.0" ˍ h, "45.0" ˍ `°`)
            v5.length shouldBe ("1.0" ˍ m)
            v5.rightAscension shouldBe ("6.0" ˍ h)
            v5.declination shouldBe ("45.0" ˍ `°`)

            val up = Cartesian.Companion.StandardDirections.UP.dir
//            println(up)
//            println(up.toSpherical())
            up.toSpherical() shouldBe SphericalPos("1.0" ˍ m, "0.0" ˍ `°`, "0.0" ˍ `°`)

            SphericalPos(
                "1.0" ˍ m,
                (Const.PI / 2).baseValue ˍ rad,
                (Const.PI / 2).baseValue ˍ rad
            ).compareTo(
                SphericalPos("1.0" ˍ m, "90.0" ˍ `°`, "90.0" ˍ `°`)
            ) shouldBe 0

            val east = Cartesian("0.0", "1.0", "0.0")
            east.toSpherical() shouldBe SphericalPos("1.0" ˍ m, "90.0" ˍ `°`, "90.0" ˍ `°`)
        }

        "cartesian cross product" {
            val v1 = Cartesian("12.0", "5.0", "8.0")
            val r1 = v1 + v1
            r1.length shouldBe (v1.length * 2.0)
            r1.x.toDouble() shouldBe 24.0
            r1.y.toDouble() shouldBe 10.0
            r1.z.toDouble() shouldBe 16.0

            v1 - v1 shouldBe Cartesian.ZERO

            Cartesian("1.0", "0.0", "0.0").cross(Cartesian("0.0", "1.0", "0.0")) shouldBe
                    Cartesian("0.0", "0.0", "1.0")
            Cartesian("0.0", "1.0", "0.0").cross(Cartesian("1.0", "0.0", "0.0")) shouldBe
                    Cartesian("0.0", "0.0", "-1.0")
            Cartesian("2.0", "0.0", "0.0").cross(Cartesian("0.0", "2.0", "0.0")) shouldBe
                    Cartesian("0.0", "0.0", "4.0")
        }

        "cartesian to astro" {
            val ast0 = AstroPos("1.0" ˍ m, "0.0" ˍ h, "0.0" ˍ `°`)
            ast0.distance shouldBe ("1.0" ˍ m)
            ast0.rightAscension shouldBe ("0.0" ˍ h)
            ast0.declination shouldBe ("0.0" ˍ `°`)
            val cart0 = ast0.toCartesian()
            cart0.x shouldBe ("1.0" ˍ m)
            cart0.y shouldBe ("0.0" ˍ m)
            cart0.z.baseValue.approximates("0.0".toBFN()) shouldBe true

            val ast1 = AstroPos("2.0" ˍ m, "3" ˍ h, "45" ˍ `°`)
            val cart1 = ast1.toCartesian()
            val d = "2".toBFN().sqrt()
            cart1.x.baseValue.approximates(d) shouldBe true
            cart1.y.baseValue.approximates(d) shouldBe true
            cart1.z.baseValue.approximates(d) shouldBe true
            val ast2 = cart1.toAstro()
            ast2 shouldBe ast1
            (ast2.declination toUnit `°`).baseValue.approximates(ast1.declination.baseValue) shouldBe true
            ast2.rightAscension.baseValue.approximates(ast1.rightAscension.baseValue) shouldBe true

            val ast3 = AstroPos("1.29" ˍ pc, "14.4966" ˍ h, "-62.681" ˍ `°`)
            val ast5 = ast3.toCartesian().toAstro()
            ast5 shouldBe ast3
            (ast5.declination toUnit `°`).baseValue.approximates(ast3.declination.baseValue) shouldBe true
            ast5.rightAscension.baseValue.approximates(ast3.rightAscension.baseValue) shouldBe true

            //                 plx    Mv
            //recno   Name     (mas)  (mag) HD     DM           _RA (    _DE (
            //   3479 Gl 849.1  59.0  3.77 210302 CD-33 15941   "h:m:s") "d:m:s")
            //                                                  22 10 08 -32 32.9
            val de = -(32 + 32.9 / 60.0)
            val parsec = (1000 / 59.0).toString() ˍ pc
            val a3 = AstroPos(parsec, 22, 10, 8.0, de.toString() ˍ `°`)
            val ra = HmsBF(22, 10, "8").toSeconds().toDouble()  // 332.53333°
            a3.rightAscension.toDouble() shouldBe ra.plusOrMinus(0.00001)
            val dec = -1.0 * (32.0 + 32.9 / 60)  // -32.5°
            (a3.declination toUnit `°`).printValue.toDouble() shouldBe dec.plusOrMinus(0.00001)
//            println(a3)
            val cart2 = a3.toCartesian()
            val a4 = cart2.toAstro()
            (a4.declination toUnit `°`).baseValue.approximates(a3.declination.baseValue) shouldBe true
            a4.rightAscension.baseValue.approximates(a3.rightAscension.baseValue) shouldBe true
        }

        "astro to cartesian" {
            //The closest star to the Sun is Proxima Centauri. Its coordinates are:
            //\alpha = 14.4966 hours
            //\delta = -62.681 degrees
            //    d = 1.29 parsecs
            //
            //which results in:
            //        x = -0.472 parsecs
            //        y = -0.361 parsecs
            //        z= -1.151 parsecs
            val a1 = AstroPos("1.29" ˍ pc, "14.4966" ˍ h, "-62.681" ˍ `°`)
            val cart1 = a1.toCartesian()
            assertSoftly {
                (cart1.x toUnit pc).printValue.toDouble() shouldBe
                        (-0.4700159664).plusOrMinus(0.0000000001)
                (cart1.y toUnit pc).printValue.toDouble() shouldBe
                        (-0.35999168752).plusOrMinus(0.0000000001)
                (cart1.z toUnit pc).printValue.toDouble() shouldBe
                        (-1.1461199659).plusOrMinus(0.0000000001)
            }
        }
    }
}
