package de.hdc.measure.tools

import de.hdc.measure.*
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.numdouble.*
import de.hdc.measure.numinteger.*
import io.kotest.assertions.throwables.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

internal class DreisatzTest : FreeSpec() {
    init {
        "Dreisatz" {
            shouldThrow<IllegalArgumentException> {
                dreisatz("3.0" k g, "1.0" ˍ h, "12.0" k g, "30.0" ˍ min)
            }
            shouldThrow<IllegalArgumentException> {
                dreisatz(null, null, "12.0" k g, "30.0" ˍ min)
            }
            shouldThrow<IllegalArgumentException> {
                dreisatz("12.0" k g, "30.0" ˍ min, null, null)
            }

            dreisatz(null, "1.0" ˍ h, "12.0" k g, "30.0" ˍ min).toString() shouldBe "24.0 kg"
            dreisatz(null, "30.0" ˍ min, "24.0" k g, "1.0" ˍ h).toString() shouldBe "12.0 kg"
            dreisatz(null, "30.0" ˍ min, "24.0" k g, "1.0" ˍ h) shouldBe ("12.0" k g)
            dreisatz("12.0" k g, null, "24.0" k g, "1.0" ˍ h) shouldBe ("0.5" ˍ h)
            dreisatz("12.0" k g, "30.0" ˍ min, null, "1.0" ˍ h) shouldBe ("24.0" k g)
            dreisatz("12.0" k g, "30.0" ˍ min, "24.0" k g, null) shouldBe ("1.0" ˍ h)

            dreisatz(null, 1.0 ˍ h, 12.0 k g, 30.0 ˍ min).toString() shouldBe "24.0 kg"
            dreisatz(null, 1.0 ˍ h, 12.0 k g, 30.0 ˍ min)
                .shouldBe(24.0 k g)
            dreisatz(24.0 k g, null, 12.0 k g, 30.0 ˍ min)
                .shouldBe(1.0 ˍ h)
            dreisatz(24.0 k g, 1.0 ˍ h, null, 30.0 ˍ min)
                .shouldBe(12.0 k g)
            dreisatz(24.0 k g, 1.0 ˍ h, 12.0 k g, null)
                .shouldBe(30.0 ˍ min)

            dreisatz((2 ˍ m), (12 ˍ s), (4 ˍ m), null) shouldBe (24 ˍ s)
            dreisatz((2 ˍ m), null, (4 ˍ m), 24 ˍ s) shouldBe (12 ˍ s)

            dreisatz(null, (2 ˍ s), (24 ˍ m), 4 ˍ s) shouldBe (12 ˍ m)
            dreisatz((12 ˍ m), (2 ˍ s), null, 4 ˍ s) shouldBe (24 ˍ m)
        }
    }
}
