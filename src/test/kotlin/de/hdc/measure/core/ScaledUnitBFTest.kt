@file:Suppress("DEPRECATION")

package de.hdc.measure.core

import de.hdc.measure.*
import de.hdc.measure.numbigfloat.*
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class ScaledUnitBFTest : FreeSpec() {
    private val n = Typography.nbsp

    init {
        "find optimal prefix" {
            assertSoftly {
                ("0.01" ˍ s).toString() shouldBe "10.0${n}ms"
                ("10" ˍ s).toString() shouldBe "10.0${n}s"
                ("60" ˍ s).format(optimize = true) shouldBe "   1.000${n}min"
                ("3600" ˍ s).format(optimize = true) shouldBe "   1.000${n}h"
                ("86400" ˍ s).format(optimize = true) shouldBe "   1.000${n}d"
            }
        }

        "mul" {
            (("5" ˍ s) * ("2" ˍ s)).toString() shouldBe "10.0${n}s²"
            (("5" ˍ min) * ("2" ˍ min)).toString() shouldBe "36.0${n}ks²"
            (("5" ˍ min) * ("2" ˍ s)).toString() shouldBe "600.0${n}s²"
            (("5" ˍ min) * ("2" ˍ h)).toString() shouldBe "2.2${n}Ms²"

            ("5" k s).toDouble() shouldBe 5000.0
            (("5" k s) * ("2" ˍ s)).toString() shouldBe "10.0${n}ks²"
            (("5" k min) * ("2" ˍ min)).toString() shouldBe "36.0${n}Ms²"
            (("5" k min) * ("2" k s)).toString() shouldBe "600.0${n}Ms²"
            (("5" ˍ min) * ("2" k h)).toString() shouldBe "2.2${n}Gs²"
        }

        "scalar mul" {
            (5 * ("2" ˍ s)).toString() shouldBe "10.0${n}s"
            (("2" ˍ s) * 5).toString() shouldBe "10.0${n}s"
            (5 * ("2" ˍ min)).toString() shouldBe "10.0${n}min"
            (("2" ˍ min) * 5).toString() shouldBe "10.0${n}min"

            ("3.6" k s).baseValue.toDouble() shouldBe 3600.0
            (2.0 * ("3.6" k s)).format(padding = 0u, decimals = 1u, optimize = true) shouldBe "2.0${n}h"
            (("3.6" k s) * 2.0).format(padding = 0u, decimals = 1u, optimize = true) shouldBe "2.0${n}h"
            ("3.6" M s).format(padding = 0u, decimals = 1u, optimize = true) shouldBe "41.7${n}d"
            (24.0 * ("3.6" M s)).format(padding = 0u, decimals = 1u, optimize = true) shouldBe "1.0${n}kd"
            (("3.6" M s) * 24.0).format(padding = 0u, decimals = 1u, optimize = true) shouldBe "1.0${n}kd"
        }

        "div" {
            (("10" ˍ s) / ("2" ˍ s)).toString() shouldBe "5.0"
            (("10" ˍ min) / ("2" ˍ min)).toString() shouldBe "5.0"
            (("10" ˍ min) / ("2" ˍ s)).toString() shouldBe "300.0"
            (("2" ˍ h) / ("10" ˍ min)).toString() shouldBe "12.0"

            (("10" k s) / ("2" ˍ s)).toString() shouldBe "5.0${n}k"
            (("10" k min) / ("2" ˍ min)).toString() shouldBe "5.0${n}k"
            (("10" k min) / (("2" k s))).toString() shouldBe "300.0"
            (("2.0" ˍ h) / ("10.0" ˍ min)).toString() shouldBe "12.0"
        }

        "scalar div" {
            (("2.0" ˍ s) / 10.0).toString() shouldBe "200.0${n}ms"
            (("20" ˍ min) / 10).toString() shouldBe "2.0${n}min"

            ("1.2" k s).baseValue shouldBe "1200.0".toBFN()
            (("1.2" k s) / 10).format(optimize = true) shouldBe "   2.000${n}min"
            (("20000.0" m min) / 10).format(optimize = true) shouldBe "   2.000${n}min"
        }
    }
}
