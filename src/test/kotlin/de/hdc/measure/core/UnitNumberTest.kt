@file:Suppress("DEPRECATION", "RedundantValueArgument")

package de.hdc.measure.core

import ch.obermuhlner.math.big.BigFloat
import de.hdc.measure.*
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.numdouble.*
import de.hdc.measure.numinteger.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*
import io.kotest.matchers.booleans.*
import io.kotest.matchers.doubles.*
import kotlin.collections.fold
import kotlin.math.PI
import kotlin.time.*

//@RunWith(KTestJUnitRunner::class)
@ExperimentalTime
class UnitNumberTest : FreeSpec() {
    init {
//        "Random values".config(timeout = 10.seconds) {
//            checkAll(100000) { v: Double ->
//                val n = UnitNumberImpl(v.toDN(), t)
//                when {
//                    v == Double.NEGATIVE_INFINITY -> n.isInfinite()
//                    v == Double.POSITIVE_INFINITY -> n.isInfinite()
//                    v == Double.MIN_VALUE -> n.isInfinite()
//                    v == Double.MAX_VALUE -> n.isInfinite()
//                    v.isNaN() -> n.isNaN()
//                    else -> if (
//                        (1_000.0 * v < Double.POSITIVE_INFINITY)
//                        && (1_000.0 * v > Double.NEGATIVE_INFINITY)
//                    ) {
//                        n.toDouble() shouldBe ((1_000.0 * v) plusOrMinus 1e-12)
//                    }
//                }
//            }
//        }

        "toString / format" {
            val v1 = ("3" ˍ m) / ("1" ˍ s)
            v1.format(decimals = 3u, optimize = false, padding = 0u) shouldBe "3.000 [m s⁻¹]"

            val a1 = v1 / ("1" ˍ s)
            a1.format(decimals = 3u, optimize = false, padding = 0u) shouldBe "3.000 [m s⁻²]"

            val v2 = ("3.6" k m) / ("1" ˍ h)
            v2.format(decimals = 3u, optimize = false, padding = 0u) shouldBe "1.000 [m s⁻¹]"
            // km/h is the biggest ScaledUnit suitable
            v2.format(decimals = 3u, optimize = true, padding = 0u) shouldBe "3.600 [km h⁻¹]"
            v2.toString() shouldBe "1.0 [m s⁻¹]"

            val a2 = v2 / ("1" ˍ s)
            a2.format(decimals = 3u, optimize = false, padding = 0u) shouldBe "1.000 [m s⁻²]"

            (a2 * 1_000_000).format(decimals = 3u, optimize = false, padding = 0u) shouldBe "1.000 M[m s⁻²]"
            (a2 * 1_000_000).format(decimals = 3u, optimize = true, padding = 0u) shouldBe "1.000 M[m s⁻²]"

            ("1" M km_h).format(decimals = 3u, optimize = false, padding = 0u) shouldBe "1.000 M[km h⁻¹]"
            ("1" M km_h).format(decimals = 3u, optimize = true, padding = 0u) shouldBe "1.000 M[km h⁻¹]"

            // Single base units in Combined don't have surrounding braces
            (("1" ˍ UNITLESS) / ("1" ˍ m)).format(decimals = 3u, optimize = true, padding = 0u) shouldBe "1.000 m⁻¹"
        }

        "Combining units" {
            val v1 = ("3" ˍ m) / ("1" ˍ s)
            v1.unit.isEquivalentTo(m_s) shouldBe true
            (v1 / ("1" ˍ s)).unit.isEquivalentTo(m_s2) shouldBe true

            val v2 = (("3.6" k m) / ("1" ˍ h))
            v2.unit.isEquivalentTo(m_s) shouldBe true
            (v2 / ("1" ˍ s)).unit.isEquivalentTo(m_s2) shouldBe true

            val u1 = (("2" ˍ Cd) * ("1" ˍ A)).unit
            u1.name shouldBe "A Cd"
            u1.symbol shouldBe "[A Cd]"
            u1.quantity shouldBe Quantity.from(J = 1, I = 1)
            u1.dimension shouldBe Combined
            u1.converter shouldBe Converter.IDENTITY

            val u2 = (("2" ˍ Cd) / ("1" ˍ A)).unit
            u2.name shouldBe "A⁻¹ Cd"
            u2.symbol shouldBe "[A⁻¹ Cd]"
            u2.quantity shouldBe Quantity.from(J = 1, I = -1)
            u2.dimension shouldBe Combined
            u2.converter shouldBe Converter.IDENTITY
        }

        "Iterable.sumOf" {
            val list0: List<UnitNumber<BigFloat, BigFloatNumber, Mass>> = listOf(("3" ˍ g), ("12" ˍ g), ("0.5" k g))
            list0.sumOf("0" ˍ g) shouldBe ("515.0" ˍ g)

            val list1: List<UnitNumber<Double, DoubleNumber, Mass>> = listOf((3.0 ˍ g), (12.0 ˍ g), (0.5 k g))
            list1.sumOf(0.0 ˍ g) shouldBe (515.0 ˍ g)

            val list2: List<UnitNumber<Long, LongNumber, Mass>> = listOf((3 ˍ g), (1 ˍ g), (1 k g))
            list2.sumOf(0 ˍ g) shouldBe (1004 ˍ g)

            val list3: List<UnitNumber<BigFloat, BigFloatNumber, Mass>> = listOf(("3" ˍ g), ("12" ˍ g), ("0.5" k g))
            list3.fold("0" ˍ g) { acc, elm ->
                acc + elm
            } shouldBe ("515.0".toBFN() ˍ g)

            val list4: List<UnitNumber<Double, DoubleNumber, Mass>> = listOf((3.0 ˍ g), (12.0 ˍ g), (0.5 k g))
            list4.fold(0.0 ˍ g) { acc, elm ->
                acc + elm
            } shouldBe (515.0 ˍ g)

            val list5: List<UnitNumber<Long, LongNumber, Mass>> = listOf((3 ˍ g), (1 ˍ g), (1 k g))
            list5.fold(0 ˍ g) { acc, elm ->
                acc + elm
            } shouldBe (1004 ˍ g)
        }

        "Conversions" {
            ((Math.PI ˍ rad) toUnit `°`).printValue.toDouble() shouldBeExactly 180.0
            ((180.0 ˍ `°`) toUnit rad).toDouble() shouldBe (Math.PI plusOrMinus 1e-15)
            ((360.0 ˍ `°`) toUnit rad).toDouble() shouldBe ((2.0 * Math.PI) plusOrMinus 1e-15)
            ((180.0 ˍ `°`) toUnit rad)
                .toDouble() shouldBe ((Math.PI ˍ rad).toDouble() plusOrMinus 1e-15)
            ((90.0 ˍ `°`) toUnit rad)
                .toDouble() shouldBe (((0.5 * Math.PI) ˍ rad).toDouble() plusOrMinus 1e-15)

            (0.0 ˍ `°C`).baseValue.value shouldBe 273.15
            (-273.15 ˍ `°C`).baseValue.value shouldBe 0.0
            (0.0 ˍ K).baseValue.value shouldBe 0.0
            (10.0 k `°C`) toUnit K shouldBe (10273.15 ˍ K)
            (9726.85 ˍ `°C`) toUnit K shouldBe (10000.0 ˍ K)
            (1.0 ˍ `°C`) shouldNotBe (1.0 ˍ K)

            ((0.0 k K) toUnit `°C`).baseValue.value shouldBe 0.0
            (0.0 k K) toUnit `°C` shouldBe (-273.15 ˍ `°C`)
            (10.0 k K) toUnit `°C` shouldBe (9726.85 ˍ `°C`)
            (10273.15 ˍ K) toUnit `°C` shouldBe (10000.0 ˍ `°C`)
            (1.0 ˍ K) shouldNotBe (1.0 ˍ `°C`)

            (1.0 ˍ d).baseValue.value shouldBe 86400.0
            (1.0 ˍ d).toString() shouldBe "1.0 d"
            (86400.0 ˍ s).format(padding = 0u, decimals = 1u, optimize = true) shouldBe "1.0 d"
            (86400.0 ˍ s) shouldBe (1.0 ˍ d)
            (1.0 ˍ d) toUnit s shouldBe (86400.0 ˍ s)
            (1.0 ˍ d) toUnit s shouldNotBe (86401.0 ˍ s)
            (1.0 ˍ L) toUnit m3 shouldBe (0.001 ˍ m3)
            (1000.0 ˍ L) toUnit m3 shouldBe (1.0 ˍ m3)
            (1.0 ˍ L).toDouble() shouldBe 1.0
            (1.0 ˍ L).baseValue.toDouble() shouldBe 0.001
            (1.0 ˍ L).printValue.toDouble() shouldBe 1.0
            (1.0 ˍ m3).toDouble() shouldBe 1.0

            val p = ((1.0 ˍ mol) * (293.15 ˍ K) * Const.R.toDouble()) / (22.4 ˍ L)
            p.toDouble() shouldBe 108_811.81770141171
        }

        "Test equals()" - {
            "predefined units" {
                `°C`.shouldBe(`°C`)
                K.shouldNotBe(`°C`)

                g.shouldNotBe(Unit)
                g.shouldBe(g)
                g.shouldNotBe(ScaledUnit("", "", g.quantity, g.dimension, "1000".toBFN()))
            }
        }

        "Test isEquivalentTo()" {
            g.isEquivalentTo(m).shouldBeFalse()

            g.isEquivalentTo(ScaledUnit("", "", g.quantity, g.dimension)).shouldBeTrue()
            m.isEquivalentTo(AU).shouldBeTrue()

            ScaledUnit("", "", Quantity.from(L = -2, M = 1), Combined)
                .isEquivalentTo(
                    ScaledUnit("", "", Quantity.from(L = -2, M = 1), Combined)
                )
                .shouldBeTrue()
        }

        "Extensions for scalars" {
            2 * (5.3 k g) shouldBe (10.6 k g)
            2.0 * (5.3 k g) shouldBe (10.6 k g)
            2.0f * (5.3 k g) shouldBe (10.6 k g)
            2L * (5.3 k g) shouldBe (10.6 k g)
        }

        "printValue" {
            (5.3 k g).toDouble() shouldBe 5300.0
            (5.3 k g).printValue.toDouble() shouldBe 5.3

            (50.0 ˍ K).toDouble() shouldBe 50.0
            (50.0 ˍ K).printValue.toDouble() shouldBe 50.0
            (0.0 ˍ `°C`).baseValue.toDouble() shouldBe 273.15
            (0.0 ˍ `°C`).printValue.toDouble() shouldBe 0.0
            (0.0 ˍ `°C`).toDouble() shouldBe 0.0

            (0.0 ˍ `°`).toDouble() shouldBe 0.0
            (0.0 ˍ `°`).printValue.toDouble() shouldBe 0.0
            (90.0 ˍ `°`).baseValue.toDouble() shouldBe (PI / 2.0)
            (90.0 ˍ `°`).printValue.toDouble() shouldBe 90.0
            (90.0 ˍ `°`).toDouble() shouldBe 90.0
        }
    }
}
