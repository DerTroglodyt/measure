package de.hdc.measure.core

import io.kotest.core.spec.style.*
import io.kotest.matchers.*
import org.junit.jupiter.api.Assertions.*

class QuantityTest : FreeSpec() {

    init {
        "main" {
            // test toString()
            // all values == 0 returns an empty string
            assertEquals("1", Quantity.from().toString())

            // short form for all values == 1 or -1
            assertEquals("N·L·I·J·M·Θ·T", Quantity.from(1, 1, 1, 1, 1, 1, 1).toString())
//            println(Quantity.from(-1, -1, -1, -1, -1, -1, -1).toString())
            assertEquals("N⁻¹·L⁻¹·I⁻¹·J⁻¹·M⁻¹·Θ⁻¹·T⁻¹", Quantity.from(-1, -1, -1, -1, -1, -1, -1).toString())

            // long form for values > 1 or < -1
            assertEquals(
                "N²·L²·I²·J²·M²·Θ²·T²",
                Quantity.from(2, 2, 2, 2, 2, 2, 2).toString()
            )
            assertEquals(
                "N⁻²·L⁻²·I⁻²·J⁻²·M⁻²·Θ⁻²·T⁻²",
                Quantity.from(-2, -2, -2, -2, -2, -2, -2).toString()
            )

            // mixed values
            assertEquals(
                "N²·L³·I⁻²·J⁴·M⁻⁴·Θ²·T⁻³",
                Quantity.from(2, 3, -2, 4, -4, 2, -3).toString()
            )
            assertEquals("N²·L·I⁻¹·Θ²·T⁻³", Quantity.from(N = 2, L = 1, I = -1, Θ = 2, T = -3).toString())


            // Test equals()
            assertEquals(
                Quantity.from(L = 1, I = 2, J = 3, M = -1, Θ = -2, T = -3),
                Quantity.from(L = 1, I = 2, J = 3, M = -1, Θ = -2, T = -3)
            )
            assertEquals(Quantity.from(), Quantity.from())
            assertFalse(Quantity.from(L = 1, I = 2, J = 3, M = -1, Θ = -2, T = -3) == Quantity.from())
            assertFalse(Quantity.from(L = 1, I = 2, J = 3, M = -1, Θ = -2, T = -3).equals(Quantity.from()))
            assertFalse(Quantity.from(L = 1, I = 2, J = 3, M = -1, Θ = -2, T = -3).equals(Unit))

            // Test times()
            (Quantity.from(2, 2, 2, 2, 2, 2, 2, 2) * Quantity.from()) shouldBe
                    Quantity.from(2, 2, 2, 2, 2, 2, 2, 2)
            (Quantity.from(2, 2, 2, 2, 2, 2, 2) * Quantity.from(L = 1, I = 2, J = 3, M = -1, Θ = -2, T = -3)) shouldBe
                    Quantity.from(2, 3, 4, 5, 1, T = -1)

            // Test div()
            (Quantity.from(2, 2, 2, 2, 2, 2, 2) / Quantity.from()) shouldBe
                    Quantity.from(2, 2, 2, 2, 2, 2, 2)

            (Quantity.from(2, 2, 2, 2, 2, 2, 2, 2) /
                    Quantity.from(L = 1, I = 2, J = 3, M = -1, Θ = -2, T = -3, pcs = -4)) shouldBe
                    Quantity.from(2, 1, J = -1, M = 3, Θ = 4, T = 5, pcs = 6)
        }

        "reciprocal" {
            val q1 = Quantity.from(1, 1, 1, 1, 1, 1, 1)
            q1.toString() shouldBe "N·L·I·J·M·Θ·T"
            q1.reciprocal().toString() shouldBe "N⁻¹·I⁻¹·L⁻¹·J⁻¹·M⁻¹·Θ⁻¹·T⁻¹"
            q1.reciprocal() shouldBe Quantity.from(-1, -1, -1, -1, -1, -1, -1)
            q1.reciprocal().reciprocal() shouldBe q1
        }
    }
}
