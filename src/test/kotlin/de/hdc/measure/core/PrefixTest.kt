package de.hdc.measure.core

import de.hdc.measure.*
import de.hdc.measure.core.DecimalPrefix.*
import de.hdc.measure.core.DecimalPrefix.m
import de.hdc.measure.numbigfloat.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class PrefixTest : FreeSpec({
    "ScaledUnit from range operator" {
        val kg0 = ScaledUnit("kg", "kg", g, Converter("1".toBFN()).combine(t))
        kg0.quantity shouldBe Quantity.from(M = 1)
        kg0.converter.toBaseUnit(BigFloatNumber.ONE) shouldBe "1_000_000.0".toBFN()
        kg0.converter.multiplier.toDouble() shouldBe 1_000_000.0

        val kg1 = (NONE..t)
        kg1.quantity shouldBe Quantity.from(M = 1)
        kg1.converter.toBaseUnit(BigFloatNumber.ONE) shouldBe "1_000_000.0".toBFN()
        kg1.converter.multiplier.toDouble() shouldBe 1_000_000.0

        val kg2 = ("2" ˍ t)
        t.converter.toBaseUnit(BigFloatNumber.ONE) shouldBe "1_000_000".toBFN()
        kg2.unit.converter.toBaseUnit(BigFloatNumber.ONE) shouldBe "1_000_000".toBFN()
        kg2.baseValue.toDouble() shouldBe 2_000_000.0
        kg2.printValue.toDouble() shouldBe 2.0

        val kg3 = ("2" ˍ t).toUnit(k..g)
        kg3.baseValue.toDouble() shouldBe 2_000_000.0
        kg3.printValue.toDouble() shouldBe 2_000.0

        ("3.5" ˍ s).toUnit(m..s).printValue.toDouble() shouldBe 3_500.0
        ("3.5" ˍ s).toUnit(m..s).baseValue.toDouble() shouldBe 3.5
    }
})
