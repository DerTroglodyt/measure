@file:Suppress("DEPRECATION")

package de.hdc.measure.core

import de.hdc.measure.*
import de.hdc.measure.core.DecimalPrefix.*
import de.hdc.measure.core.ScaledUnit.Companion.from
import de.hdc.measure.h
import de.hdc.measure.m
import de.hdc.measure.numbigfloat.BigFloatNumber.Companion.ONE
import de.hdc.measure.numdouble.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class ScaledUnitTest : FreeSpec() {
    init {
        "equality" {
            (k * DecimalPrefix.m) shouldBe NONE
            (k * NONE) shouldBe k
            (k / NONE) shouldBe k
            (1.0 k g) * (1.0 ˍ m) shouldBe (1.0 ˍ k..from(listOf(g, m), emptyList()))
            (1.0 k g) * (1.0 ˍ m) / (1.0 ˍ s) shouldBe (1.0 ˍ k..from(listOf(g, m), listOf(s)))
//            val x = (1.0 k g) * (1.0 ˍ m) / (1.0 ˍ s)
//            println(x)
            val a = (1.0 k g) * (1.0 ˍ m) / (1.0 ˍ s) / (1.0 ˍ s)
            val b = (1.0 ˍ N)
            a shouldBe b
            (1.0 ˍ g) * (1.0 ˍ m) / (1.0 ˍ s) / (1.0 ˍ s) shouldNotBe (1.0 ˍ N)
            from(listOf((k..g), m), listOf(s, s)).toString() shouldBe "[m g s⁻²]"
            val u1 = (1.0 ˍ from(listOf((k..g), m), listOf(s, s))).simplify()
            u1.toString() shouldBe "1.0 N"
            val u2 = (1.0 ˍ from(listOf(g, m), listOf(s, s))).simplify()
            u2.toString() shouldBe "1.0 mN"

            `in` shouldNotBe m

            (1.0 k g) shouldBe (1.0 k g)
            (1000.0 ˍ g) shouldBe (1.0 k g)
            (1.0 ˍ from(listOf((k..g), m), listOf(s, s))) shouldBe (1.0 ˍ N)
            (1000.0 ˍ from(listOf(g, m), listOf(s, s))) shouldBe (1.0 ˍ N)
        }

        "baseValue / printValue" {
            val m = (5.0 ˍ min)
            m.baseValue.value shouldBe 300.0
            m.printValue.toDouble() shouldBe 5.0
        }

        "mul" {
            ((5.0 ˍ s) * (2.0 ˍ s)).toString() shouldBe "10.0 s²"
            ((5.0 ˍ min) * (2.0 ˍ min)).toString() shouldBe "36.0 ks²"
            ((5.0 ˍ min) * (2.0 ˍ s)).toString() shouldBe "600.0 s²"
            ((5.0 ˍ min) * (2.0 ˍ h)).toString() shouldBe "2.2 Ms²"

            (5.0 k s).baseValue.toDouble() shouldBe 5000.0
            (5.0 k s).toDouble() shouldBe 5000.0
            ((5.0 k s) * (2.0 ˍ s)).toString() shouldBe "10.0 ks²"
            ((5.0 k min) * (2.0 ˍ min)).toString() shouldBe "36.0 Ms²"
            ((5.0 k min) * (2.0 k s)).toString() shouldBe "600.0 Ms²"
            ((5.0 ˍ min) * (2.0 k h)).toString() shouldBe "2.2 Gs²"
        }

        "scalar mul" {
            (5.0 * (2.0 ˍ s)).toString() shouldBe "10.0 s"
            ((2.0 ˍ s) * 5.0).toString() shouldBe "10.0 s"
            (5.0 * (2.0 ˍ min)).toString() shouldBe "10.0 min"
            ((2.0 ˍ min) * 5.0).toString() shouldBe "10.0 min"

            (5.0 * (2.0 k s)).toString() shouldBe "10.0 ks"
            ((2.0 k s) * 5.0).toString() shouldBe "10.0 ks"
            (5.0 * (2.0 M min)).toString() shouldBe "10.0 Mmin"
            ((2.0 M min) * 5.0).toString() shouldBe "10.0 Mmin"

            (5.0 * (2.0 k s)).simplify().toString() shouldBe "10.0 ks"
            ((2.0 k s) * 5.0).simplify().toString() shouldBe "10.0 ks"
            (5.0 * (2.0 M min)).simplify().toString() shouldBe "600.0 Ms"
            ((2.0 M min) * 5.0).simplify().toString() shouldBe "600.0 Ms"

            (5.0 * (2.0 k s)).format(optimize = true) shouldBe "   2.778 h"
            ((2.0 k s) * 5.0).format(optimize = true) shouldBe "   2.778 h"
            (5.0 * (2.0 M min)).format(optimize = true) shouldBe "   6.944 kd"
            ((2.0 M min) * 5.0).format(optimize = true) shouldBe "   6.944 kd"
        }

        "div" {
            (k..g).prefix shouldBe k
            (k..g).converter.fromBaseUnit(ONE).toDouble() shouldBe 1.0
            val u = (k..g) / s
            u.second.fromBaseUnit(ONE).toDouble() shouldBe 1.0
            u.first.prefix shouldBe k
            u.first.converter.fromBaseUnit(ONE).toDouble() shouldBe 1.0

            val n = 1.0 ˍ N
            n.unit.prefix shouldBe NONE
            n.unit.converter.fromBaseUnit(ONE).toDouble() shouldBe 0.001
            val x1 = (1.0 ˍ (k..g)) * (1.0 ˍ m) / (1.0 ˍ s) / (1.0 ˍ s)
            x1.unit.prefix shouldBe k
            x1.unit.converter.toBaseUnit(ONE).toDouble() shouldBe 1.0
            x1.baseValue.value shouldBe 1000.0

            val u2 = x1.unit / s
            u2.second.fromBaseUnit(ONE).toDouble() shouldBe 1.0
            u2.first.prefix shouldBe k
            u2.first.converter.fromBaseUnit(ONE).toDouble() shouldBe 1.0


            ((10.0 ˍ s) / (2.0 ˍ s)).toString() shouldBe "5.0"
            ((10.0 ˍ min) / (2.0 ˍ min)).toString() shouldBe "5.0"
            ((10.0 ˍ min) / (2.0 ˍ s)).toString() shouldBe "300.0"
            ((2.0 ˍ h) / (10.0 ˍ min)).toString() shouldBe "12.0"

            ((10.0 k s) / (2.0 ˍ s)).toString() shouldBe "5.0 k"
            ((10.0 k min) / (2.0 ˍ min)).toString() shouldBe "5.0 k"
            ((10.0 k min) / (2.0 k s)).toString() shouldBe "300.0"
            ((2.0 ˍ h) / (10.0 k min)).toString() shouldBe "12.0 m"
        }

        "scalar div" {
            ((2.0 ˍ s) / 10.0).toString() shouldBe "200.0 ms"
            ((20.0 ˍ min) / 10.0).toString() shouldBe "2.0 min"

            ((2.0 k s) / 10.0).toString() shouldBe "200.0 s"
            ((20.0 m min) / 10.0).toString() shouldBe "2.0 mmin"

            ((2.0 k s) / 10.0).toString() shouldBe "200.0 s"
            ((2.0 k s) / 10.0).simplify().toString() shouldBe "200.0 s"
            ((20.0 m min) / 10.0).simplify().toString() shouldBe "120.0 ms"

            ((2.0 k s) / 10.0).format(optimize = true) shouldBe "   3.333 min"
            ((20.0 m min) / 10.0).format(optimize = true) shouldBe "   2.000 mmin"
        }
    }
}
