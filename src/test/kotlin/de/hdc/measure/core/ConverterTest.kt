package de.hdc.measure.core

import de.hdc.measure.*
import de.hdc.measure.numbigfloat.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*

class ConverterTest : FreeSpec({
    "creation" {
        val c1 = Converter("60".toBFN())
        c1.multiplier shouldBe "60".toBFN()
        c1.toBaseUnit(BigFloatNumber.ONE_THOUSAND) shouldBe "60_000".toBFN()
        c1.fromBaseUnit("60_000".toBFN()) shouldBe BigFloatNumber.ONE_THOUSAND

        val c2 = Converter({ it * "60".toBFN() }, { it / "60".toBFN() }, s)
        c2.multiplier shouldBe "60".toBFN()
        c2.toBaseUnit(BigFloatNumber.ONE_THOUSAND) shouldBe "60_000".toBFN()
        c2.fromBaseUnit("60_000".toBFN()) shouldBe BigFloatNumber.ONE_THOUSAND

        val c3 = Converter("60".toBFN(), s)
        c3.multiplier shouldBe "60".toBFN()
        c3.toBaseUnit(BigFloatNumber.ONE_THOUSAND) shouldBe "60_000".toBFN()
        c3.fromBaseUnit("60_000".toBFN()) shouldBe BigFloatNumber.ONE_THOUSAND

        val c4 = Converter({ it * "60".toBFN() }, { it / "60".toBFN() }, "60".toBFN())
        c4.multiplier shouldBe "60".toBFN()
        c4.toBaseUnit(BigFloatNumber.ONE_THOUSAND) shouldBe "60_000".toBFN()
        c4.fromBaseUnit("60_000".toBFN()) shouldBe BigFloatNumber.ONE_THOUSAND
    }

    "combine" {
        val c1 = Converter.IDENTITY.combine(s)
        c1.multiplier shouldBe BigFloatNumber.ONE
        c1.toBaseUnit(BigFloatNumber.ONE) shouldBe BigFloatNumber.ONE
        c1.fromBaseUnit(BigFloatNumber.ONE) shouldBe BigFloatNumber.ONE

        val c2 = Converter.IDENTITY.combine(min)
        c2.multiplier shouldBe "60".toBFN()
        c2.toBaseUnit(BigFloatNumber.ONE) shouldBe "60".toBFN()
        c2.fromBaseUnit("60".toBFN()) shouldBe BigFloatNumber.ONE

        val c3 = Converter("10".toBFN()).combine(s)
        c3.multiplier shouldBe "10".toBFN()
        c3.toBaseUnit(BigFloatNumber.ONE) shouldBe "10".toBFN()
        c3.fromBaseUnit("10".toBFN()) shouldBe BigFloatNumber.ONE

        val c4 = Converter("10".toBFN()).combine(min)
        c4.multiplier shouldBe "600".toBFN()
        c4.toBaseUnit(BigFloatNumber.ONE) shouldBe "600".toBFN()
        c4.fromBaseUnit("600".toBFN()) shouldBe BigFloatNumber.ONE
    }
})
