@file:Suppress("NonAsciiCharacters")

package de.hdc.measure.core

import de.hdc.measure.tools.SubSuperScript
import kotlin.math.abs

/**
 * Associates a dimension (integer value) with a QuantityDimension.
 * This way Units can be expressed as being from a specific set of dimensions.
 * Examples:
 * Length has a Quantity of L=1 \[m]
 * Area has a Quantity of L=2 [m²]
 * Force (Newton) has a Quantity of {M=1, L=1, T=-2} [kg·m/s·s]
 */
public class Quantity private constructor(private val values: Map<QuantityDimension, Int>) {

    /**
     * [International System of Quantities](https://en.wikipedia.org/wiki/International_System_of_Quantities)
     * @param unitStr The unit Symbol of the corresponding base unit.
     */
    public enum class QuantityDimension(public val unitStr: String) {
        /**
         * Amount of substance \[mol].
         */
        N("mol"),

        /**
         * Electric current \[A].
         */
        I("A"),

        /**
         * Length \[m].
         */
        L("m"),

        /**
         * Luminous intensity \[Cd].
         */
        J("Cd"),

        /**
         * Mass \[kg].
         */
        M("g"),

        /**
         * Thermodynamic temperature \[K].
         */
        @Suppress("EnumEntryName")
        Θ("K"),

        /**
         * Time \[s].
         */
        T("s"),

        /**
         * Number of pieces / items \[PCS].
         */
        PCS("PCS")
    }

    public companion object {
        /**
         * Creates a Quantity object from the given dimension values for their dimensions.
         */
        @Suppress("LocalVariableName")
        public fun from(
            N: Int = 0,
            L: Int = 0,
            I: Int = 0,
            J: Int = 0,
            M: Int = 0,
            Θ: Int = 0,
            T: Int = 0,
            pcs: Int = 0
        ): Quantity = Quantity(
            mapOf(
                QuantityDimension.N to N,
                QuantityDimension.L to L,
                QuantityDimension.I to I,
                QuantityDimension.J to J,
                QuantityDimension.M to M,
                QuantityDimension.Θ to Θ,
                QuantityDimension.T to T,
                QuantityDimension.PCS to pcs
            )
        )
    }

    /**
     * Returns the dimension for a QuantityDimension.
     */
    public operator fun get(dimension: QuantityDimension): Int {
        return values[dimension]
            ?: throw IllegalArgumentException("Unknown QuantityDimension '$dimension'!")
    }

    override fun toString(): String {
        val st = values.mapNotNull { (dimension, value) ->
            if (value == 1) "$dimension"
            else if (value != 0) SubSuperScript.toSuperScript("$dimension$value")
            else null
        }.joinToString("·")

        return st.ifEmpty { "1" }
    }

    /**
     * Returns the base unit symbols corresponding to the Quantity dimensions.
     */
    public fun toUnitString(): String {
        val st = values.mapNotNull { (dimension, value) ->
            if (value == 1) dimension.unitStr
            else if (value != 0) SubSuperScript.toSuperScript("${dimension.unitStr}$value")
            else null
        }.joinToString(" ")
        return st
    }

    /**
     * Returns the base unit symbols corresponding to the Quantity dimensions.
     */
    public fun toUnitSymbolString(): String {
        val st = values.mapNotNull { (dimension, value) ->
            if (value == 1) dimension.unitStr
            else if (value != 0) SubSuperScript.toSuperScript("${dimension.unitStr}$value")
            else null
        }.joinToString(" ")
        return if (st.contains(" ")) "[$st]"
        else st
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Quantity) {
            return false
        }
        values.forEach { (dimension, value) ->
            if (other[dimension] != value) return false
        }
        return true
    }

    override fun hashCode(): Int {
        return values.hashCode()
    }

    /**
     * True if all Quantity dimensions are zero.
     */
    public fun isDimensionless(): Boolean = values.entries.sumOf { abs(it.value) } == 0

    /**
     * Combines the quantities when multiplying numbers with units.
     */
    public operator fun times(q: Quantity): Quantity {
        return Quantity(QuantityDimension.entries.associateWith { dimension ->
            this[dimension] + q[dimension]
        })
    }

    /**
     * Combines the quantities when dividing numbers with units.
     */
    public operator fun div(q: Quantity): Quantity {
        return Quantity(QuantityDimension.entries.associateWith { dimension ->
            this[dimension] - q[dimension]
        })
    }

    /**
     * Returns the reciprocal of this quantity.
     */
    public fun reciprocal(): Quantity {
        return Quantity(QuantityDimension.entries.associateWith { dimension ->
            this[dimension] * -1
        })
    }
}
