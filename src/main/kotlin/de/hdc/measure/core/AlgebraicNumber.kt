package de.hdc.measure.core

/**
 * The base class for all numbers that can be used in calculations.
 */
public interface AlgebraicNumber<N> : Comparable<AlgebraicNumber<N>> {
    public override fun compareTo(other: AlgebraicNumber<N>): Int
    override fun toString(): String
    override fun equals(other: Any?): Boolean

    /** Returns the value of this number. */
    public fun getValue(): N

    /** Add two numbers */
    public operator fun plus(other: AlgebraicNumber<N>): AlgebraicNumber<N>

    /** Subtract two numbers */
    public operator fun minus(other: AlgebraicNumber<N>): AlgebraicNumber<N>

    /** Multiply two numbers */
    public operator fun times(other: AlgebraicNumber<N>): AlgebraicNumber<N>

    /** Divide two numbers */
    public operator fun div(other: AlgebraicNumber<N>): AlgebraicNumber<N>

    /** Multiply two numbers */
    public operator fun times(other: Double): AlgebraicNumber<N>

    /** Multiply two numbers */
    public operator fun times(other: Float): AlgebraicNumber<N> = times(other.toDouble())

    /** Multiply two numbers */
    public operator fun times(other: Int): AlgebraicNumber<N> = times(other.toDouble())

    /** Multiply two numbers */
    public operator fun times(other: Long): AlgebraicNumber<N> = times(other.toDouble())

    /** Divide two numbers */
    public operator fun div(other: Double): AlgebraicNumber<N>

    /** Divide two numbers */
    public operator fun div(other: Float): AlgebraicNumber<N>

    /** Divide two numbers */
    public operator fun div(other: Int): AlgebraicNumber<N>

    /** Divide two numbers */
    public operator fun div(other: Long): AlgebraicNumber<N>

    /** Returns the reciprocal value: 1/this */
    public fun reciprocal(): AlgebraicNumber<N>

    /** Returns the negated value: -1*this */
    public fun inverse(): AlgebraicNumber<N>

    /**
     * Test if value is zero.
     */
    public fun isZero(): Boolean

    /**
     * Test if value is negative.
     */
    public fun isNegative(): Boolean

    /**
     * Test if value is positive.
     */
    public fun isPositive(): Boolean

    /**
     * Test if value is 'Not A Number'.
     */
    public fun isNaN(): Boolean

    /**
     * Test if value is at infinity.
     */
    public fun isInfinite(): Boolean

    /**
     * Test if both values are in +/- maxDiff of each other.
     */
    public fun approximates(other: AlgebraicNumber<N>, maxDiff: AlgebraicNumber<N>): Boolean

    // Number methods ####################
    /** Converts the value to a Double */
    public fun toDouble(): Double

    /** Converts the value to a Byte */
    public fun toByte(): Byte = toDouble().toInt().toByte()

    @Deprecated(
        "Direct conversion to Char is deprecated. Use toInt().toChar() or Char constructor instead.\n" +
                "If you override toChar() function in your Number inheritor, it's recommended to gradually" +
                " deprecate the overriding function and then remove it.\n" +
                "See https://youtrack.jetbrains.com/issue/KT-46465 for details about the migration",
        replaceWith = ReplaceWith("this.toInt().toChar()")
    )
    /** Converts the value to a Byte */
    public fun toChar(): Char = toDouble().toInt().toChar()

    /** Converts the value to a Float */
    public fun toFloat(): Float = toDouble().toFloat()

    /** Converts the value to a Int */
    public fun toInt(): Int = toDouble().toInt()

    /** Converts the value to a Long */
    public fun toLong(): Long = toDouble().toLong()

    /** Converts the value to a Short */
    public fun toShort(): Short = toDouble().toInt().toShort()

    /** Scales the internal value in base units by the print units multiplier */
    public fun fromSI(prefix: Prefix, converter: Converter): AlgebraicNumber<N>

    /** Scales the internal value in base units by the print units multiplier */
    public fun toSI(prefix: Prefix, converter: Converter): AlgebraicNumber<N>
}

//** Convenience operator. */
//public operator fun <N : Any> Double.times(other: AlgebraicNumber<N>): AlgebraicNumber<N> = other * this
//
///** Convenience operator. */
//public operator fun <N : Any> Float.times(other: AlgebraicNumber<N>): AlgebraicNumber<N> = other * this.toDouble()
//
///** Convenience operator. */
//public operator fun <N : Any> Int.times(other: AlgebraicNumber<N>): AlgebraicNumber<N> = other * this.toLong()
//
///** Convenience operator. */
//public operator fun <N : Any> Long.times(other: AlgebraicNumber<N>): AlgebraicNumber<N> = other * this.toLong()

/** Find the smallest of the given numbers */
public fun <T : Number> min(vararg number: AlgebraicNumber<T>): AlgebraicNumber<T> = number.minBy { it }

/** Find the biggest of the given numbers */
public fun <T : Number> max(vararg number: AlgebraicNumber<T>): AlgebraicNumber<T> = number.maxBy { it }
