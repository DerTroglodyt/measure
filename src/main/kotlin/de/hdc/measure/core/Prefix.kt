@file:Suppress(
    "unused", "EnumEntryName", "NonAsciiCharacters", "SpellCheckingInspection"
)

package de.hdc.measure.core

import de.hdc.measure.numbigfloat.*
import ch.obermuhlner.math.big.*
import ch.obermuhlner.math.big.BigFloat.context
import de.hdc.measure.*

/**
 * The factor to multiply the base unit by.
 * Example: k m   means 1000*m
 */
public interface Prefix {
    /** Long name */
    public val longName: String

    /** The symbol used to denote this prefix */
    public val symbol: String

    /** What factor to scale the base unit by */
    public val multiplier: BigFloatNumber

    /** Double value of the (exact) multiplier */
    public val multiplierDouble: Double

    override fun toString(): String

    public fun from(multiplier: BigFloatNumber): Prefix

    /** If this prefix is the biggest or smallest in of all */
    public fun isLast(): Boolean

    /** Next bigger prefis */
    public fun up(): Prefix

    /** Next smaller prefix */
    public fun down(): Prefix

    /** Result of multiplying two prefix multipliers */
    public operator fun times(other: Prefix): Prefix

    /** Result of dividing two prefix multipliers */
    public operator fun div(other: Prefix): Prefix

    /** 1/prefix */
    public fun reciprocal(): Prefix
}

@Suppress("MemberVisibilityCanBePrivate", "KDocMissingDocumentation")
/**
 * Opinionated international names for factors.
 */
public enum class NumberName(public val longName: String, public val multiplier: BigFloat) {
    TRIACONTILLIONTH("triacontillionth", con.valueOf("1e-90")),
    ICOSIENNILLIONTH("icosiennillionth", con.valueOf("1e-87")),
    ICOSIOKTILLIONTH("icosioktillionth", con.valueOf("1e-84")),
    ICOSIHEPTILLIONTH("icosiheptillionth", con.valueOf("1e-81")),
    ICOSIHEXILLIONTH("icosihexillionth", con.valueOf("1e-78")),
    ICOSIPENTILLIONTH("icosipentillionth", con.valueOf("1e-75")),
    ICOSITETRILLIONTH("icositetrillionth", con.valueOf("1e-72")),
    ICOSITRILLIONTH("icositrillionth", con.valueOf("1e-69")),
    ICOSIDILLIONTH("icosidillionth", con.valueOf("1e-66")),
    ICOSIHENILLIONTH("icosihenillionth", con.valueOf("1e-63")),
    ICOSILLIONTH("icosillionth", con.valueOf("1e-60")),
    ENNEADEKILLIONTH("enneadekillionth", con.valueOf("1e-57")),
    OKTADEKILLIONTH("oktadekillionth", con.valueOf("1e-54")),
    HEPTADEKILLIONTH("heptadekillionth", con.valueOf("1e-51")),
    HEXADEKILLIONTH("hexadekillionth", con.valueOf("1e-48")),
    PENTADEKILLIONTH("pentadekillionth", con.valueOf("1e-45")),
    TETRADEKILLIONTH("tetradekillionth", con.valueOf("1e-42")),
    TRISDEKILLIONTH("trisdekillionth", con.valueOf("1e-39")),
    DODEKILLIONTH("dodekillionth", con.valueOf("1e-36")),
    HENDEKILLIONTH("hendekillionth", con.valueOf("1e-33")),
    DEKILLIONTH("dekillionth", con.valueOf("1e-30")),
    ENNILLIONTH("ennillionth", con.valueOf("1e-27")),
    OKTILLIONTH("oktillionth", con.valueOf("1e-24")),
    TETRILLIONTH("tetrillionth", con.valueOf("1e-12")),
    HEPTILLIONTH("heptillionth", con.valueOf("1e-21")),
    HEXILLIONTH("hexillionth", con.valueOf("1e-18")),
    PENTILLIONTH("pentillionth", con.valueOf("1e-15")),
    GILLIONTH("gillionth", con.valueOf("1e-9")),
    MILLIONTH("millionth", con.valueOf("1e-6")),
    THOUSANDTH("thousandth", con.valueOf("1e-3")),
    NONE("", con.valueOf("1.0")),
    THOUSAND("thousand", con.valueOf("1e3")),
    MILLION("million", con.valueOf("1e6")),
    GILLION("gillion", con.valueOf("1e9")),
    TETRILLION("tetrillion", con.valueOf("1e12")),
    PENTILLION("pentillion", con.valueOf("1e15")),
    HEXILLION("hexillion", con.valueOf("1e18")),
    HEPTILLION("heptillion", con.valueOf("1e21")),
    OKTILLION("oktillion", con.valueOf("1e24")),
    ENNILLION("ennillion", con.valueOf("1e27")),
    DEKILLION("dekillion", con.valueOf("1e30")),
    HENDEKILLION("hendekillion", con.valueOf("1e33")),
    DODEKILLION("dodekillion", con.valueOf("1e36")),
    TRISDEKILLION("trisdekillion", con.valueOf("1e39")),
    TETRADEKILLION("tetradekillion", con.valueOf("1e42")),
    PENTADEKILLION("pentadekillion", con.valueOf("1e45")),
    HEXADEKILLION("hexadekillion", con.valueOf("1e48")),
    HEPTADEKILLION("heptadekillion", con.valueOf("1e51")),
    OKTADEKILLION("oktadekillion", con.valueOf("1e54")),
    ENNEADEKILLION("enneadekillion", con.valueOf("1e57")),
    ICOSILLION("icosillion", con.valueOf("1e60")),
    ICOSIHENILLION("icosihenillion", con.valueOf("1e63")),
    ICOSIDILLION("icosidillion", con.valueOf("1e66")),
    ICOSITRILLION("icositrillion", con.valueOf("1e69")),
    ICOSITETRILLION("icositetrillion", con.valueOf("1e72")),
    ICOSIPENTILLION("icosipentillion", con.valueOf("1e75")),
    ICOSIHEXILLION("icosihexillion", con.valueOf("1e78")),
    ICOSIHEPTILLION("icosiheptillion", con.valueOf("1e81")),
    ICOSIOKTILLION("icosioktillion", con.valueOf("1e84")),
    ICOSIENNILLION("icosiennillion", con.valueOf("1e87")),
    TRIACONTILLION("triacontillion", con.valueOf("1e90"));

    override fun toString(): String {
        return longName
    }

    internal fun isLast(): Boolean {
        return ((this == THOUSAND) || (this == TRIACONTILLION))
    }

    internal fun up(): NumberName {
        return when {
            isLast() -> this
            else -> entries[ordinal + 1]
        }
    }

    internal fun down(): NumberName {
        return when {
            isLast() -> this
            else -> entries[ordinal - 1]
        }
    }
}

@Suppress("KDocMissingDocumentation")
/**
 * Prefix names for prefixes that are powers of two.
 */
public enum class BinaryPrefix(
    override val longName: String,
    override val symbol: String,
    override val multiplier: BigFloatNumber,
    override val multiplierDouble: Double = multiplier.toDouble()
) : Prefix {
    NONE("", "", con.valueOf(1)),
    KIBI("kibi", "Ki", context(4).valueOf("1024e1")),
    MEBI("mebi", "Mi", context(8).valueOf("1024e2")),
    GIBI("gibi", "Gi", context(12).valueOf("1024e3")),
    TEBI("tebi", "Ti", context(16).valueOf("1024e4")),
    PEBI("pebi", "Pi", context(20).valueOf("1024e5")),
    EXBI("exbi", "Xi", context(24).valueOf("1024e6")),
    ZEBI("zebi", "Zi", context(28).valueOf("1024e7")),
    YOBI("yobi", "Yi", context(32).valueOf("1024e8")),
    `KIBI⁻¹`("kibi⁻¹", "Ki⁻¹", context(4).valueOf("1024e-1")),
    `MEBI⁻¹`("mebi⁻¹", "Mi⁻¹", context(8).valueOf("1024e-2")),
    `GIBI⁻¹`("gibi⁻¹", "Gi⁻¹", context(12).valueOf("1024e-3")),
    `TEBI⁻¹`("tebi⁻¹", "Ti⁻¹", context(16).valueOf("1024e-4")),
    `PEBI⁻¹`("pebi⁻¹", "Pi⁻¹", context(20).valueOf("1024e-5")),
    `EXBI⁻¹`("exbi⁻¹", "Xi⁻¹", context(24).valueOf("1024e-6")),
    `ZEBI⁻¹`("zebi⁻¹", "Zi⁻¹", context(28).valueOf("1024e-7")),
    `YOBI⁻¹`("yobi⁻¹", "Yi⁻¹", context(32).valueOf("1024e-8"));

    constructor(longName: String, symbol: String, multiplier: BigFloat) : this(longName, symbol, multiplier.toBFN())

    override fun toString(): String {
        return symbol
    }

    override fun isLast(): Boolean {
        return ((this == NONE) || (this == YOBI))
    }

    override fun reciprocal(): Prefix {
        return when (this) {
            NONE -> NONE
            KIBI -> `KIBI⁻¹`
            MEBI -> `MEBI⁻¹`
            GIBI -> `GIBI⁻¹`
            TEBI -> `TEBI⁻¹`
            PEBI -> `PEBI⁻¹`
            EXBI -> `EXBI⁻¹`
            ZEBI -> `ZEBI⁻¹`
            YOBI -> `YOBI⁻¹`
            `KIBI⁻¹` -> KIBI
            `MEBI⁻¹` -> MEBI
            `GIBI⁻¹` -> GIBI
            `TEBI⁻¹` -> TEBI
            `PEBI⁻¹` -> PEBI
            `EXBI⁻¹` -> EXBI
            `ZEBI⁻¹` -> ZEBI
            `YOBI⁻¹` -> YOBI
        }
    }

    override fun up(): BinaryPrefix {
        return when {
            isLast() -> this
            else -> entries[ordinal + 1]
        }
    }

    override fun down(): BinaryPrefix {
        return when {
            isLast() -> this
            else -> entries[ordinal - 1]
        }
    }

    public override fun from(multiplier: BigFloatNumber): DecimalPrefix =
        DecimalPrefix.entries.firstOrNull { it.multiplier == multiplier }
            ?: error("No BinaryPrefix for multiplier '$multiplier'!")

    override fun times(other: Prefix): Prefix =
        from(multiplier * other.multiplier) as Prefix

    override fun div(other: Prefix): Prefix =
        from(multiplier / other.multiplier) as Prefix
}

private val con: BigFloat.Context = context(1)

@Suppress("KDocMissingDocumentation")
/**
 * Prefix names for prefixes that are powers of ten.
 */
public enum class DecimalPrefix(
    override val longName: String,
    override val symbol: String,
    override val multiplier: BigFloatNumber,
    override val multiplierDouble: Double
) : Prefix {
    y("yocto", "y", con.valueOf("0.000000000000000000000001")),
    z("zepto", "z", con.valueOf("0.000000000000000000001")),
    a("atto", "a", con.valueOf("0.000000000000000001")),
    f("femto", "f", con.valueOf("0.000000000000001")),
    p("pico", "p", con.valueOf("0.000000000001")),
    n("nano", "n", con.valueOf("0.000000001")),
    µ("micro", "µ", con.valueOf("0.000001")),
    m("milli", "m", con.valueOf("0.001")),
    c("centi", "c", con.valueOf("0.01")),
    d("deci", "d", con.valueOf("0.1")),
    NONE("", "", con.valueOf("1")),
    da("deca", "da", con.valueOf("10")),
    h("hecto", "h", con.valueOf("100")),
    k("kilo", "k", con.valueOf("1000")),
    M("mega", "M", con.valueOf("1000000")),
    G("giga", "G", con.valueOf("1000000000")),
    T("tera", "T", con.valueOf("1000000000000")),
    P("peta", "P", con.valueOf("1000000000000000")),
    X("exa", "X", con.valueOf("1000000000000000000")),
    Z("zeta", "Z", con.valueOf("1000000000000000000000")),
    Y("yota", "Y", con.valueOf("1000000000000000000000000"));

    constructor(longName: String, symbol: String, multiplier: BigFloat) :
            this(longName, symbol, multiplier.toBFN(), multiplier.toDouble())

    override fun toString(): String = symbol

    override fun isLast(): Boolean = ((this == Y) || (this == y))

    override fun reciprocal(): Prefix {
        return when (this) {
            NONE -> NONE
            d -> da
            c -> h
            m -> k
            µ -> M
            n -> G
            p -> T
            f -> P
            a -> X
            z -> Z
            y -> Y
            da -> d
            h -> c
            k -> m
            M -> µ
            G -> n
            T -> p
            P -> f
            X -> a
            Z -> z
            Y -> y
        }
    }

    override fun up(): DecimalPrefix {
        return when {
            isLast() -> this
            this == h -> k
            this == da -> k
            this == NONE -> k
            this == d -> NONE
            this == c -> NONE
            this == m -> NONE
            else -> entries[ordinal + 1]
        }
    }

    override fun down(): DecimalPrefix {
        return when {
            isLast() -> this
            this == k -> NONE
            this == h -> NONE
            this == da -> NONE
            this == NONE -> m
            this == d -> m
            this == c -> m
            else -> entries[ordinal - 1]
        }
    }

    override fun times(other: Prefix): Prefix =
        from(multiplier * other.multiplier) as Prefix

    override fun div(other: Prefix): Prefix =
        from(multiplier / other.multiplier) as Prefix

    public override fun from(multiplier: BigFloatNumber): DecimalPrefix =
        entries.firstOrNull { it.multiplier == multiplier }
            ?: error("No DecimalPrefix for multiplier '$multiplier'!")

    public operator fun <D : PhysicalDimension> rangeTo(unit: ScaledUnit<D>): ScaledUnit<D> {
        val p = this * unit.prefix
        return ScaledUnit.prefixedUnit(p, unit)
    }
}
