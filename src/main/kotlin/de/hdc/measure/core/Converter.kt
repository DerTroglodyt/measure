package de.hdc.measure.core

import de.hdc.measure.numbigfloat.*

/**
 * Handles converting between display and base units.
 */
public class Converter(
    /**
     * Converts from display to base unit value.
     */
    public val toBaseUnit: (BigFloatNumber) -> BigFloatNumber,
    /**
     * Converts from base unit to display unit.
     */
    public val fromBaseUnit: (BigFloatNumber) -> BigFloatNumber,
    /**
     * Used for sorting when searching for best fit unit.
     */
    public val multiplier: BigFloatNumber
) {
    public constructor(multiplier: BigFloatNumber) : this({ it * multiplier }, { it / multiplier }, multiplier)

    public constructor(
        toBaseUnit: (BigFloatNumber) -> BigFloatNumber,
        fromBaseUnit: (BigFloatNumber) -> BigFloatNumber,
        unit: ScaledUnit<*>? = null
    ) : this(
        {
            if (unit is ScaledUnit<*>) {
                unit.converter.toBaseUnit(toBaseUnit(it))
            } else {
                toBaseUnit(it)
            }
        },
        {
            if (unit is ScaledUnit<*>) {
                unit.converter.fromBaseUnit(fromBaseUnit(it))
            } else {
                fromBaseUnit(it)
            }
        },
        when {
            unit == null -> BigFloatNumber.ONE
            unit.isBaseUnit -> toBaseUnit(BigFloatNumber.ONE)
            else -> unit.converter.toBaseUnit(toBaseUnit(BigFloatNumber.ONE))
        }
    )

    public constructor(
        toBaseUnit: BigFloatNumber,
        unit: ScaledUnit<*>? = null
    ) : this(
        {
            if (unit is ScaledUnit<*>) {
                unit.converter.toBaseUnit(it * toBaseUnit)
            } else {
                it * toBaseUnit
            }
        },
        {
            if (unit is ScaledUnit<*>) {
                unit.converter.fromBaseUnit(it / toBaseUnit)
            } else {
                it / toBaseUnit
            }
        },
        when {
            unit == null -> toBaseUnit
            unit.isBaseUnit -> toBaseUnit
            else -> unit.converter.toBaseUnit(toBaseUnit)
        }
    )

    /**
     * Combines the converter with a multiplier from a ScaledUnit.
     */
    public fun combine(unit: ScaledUnit<*>): Converter = Converter(this.toBaseUnit, this.fromBaseUnit, unit)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Converter

        if (
            (toBaseUnit == other.toBaseUnit) &&
            (fromBaseUnit == other.fromBaseUnit) &&
            (multiplier == other.multiplier)
        ) return true

        if (toBaseUnit(BigFloatNumber.ONE) != other.toBaseUnit(BigFloatNumber.ONE)) return false
        if (fromBaseUnit(BigFloatNumber.ONE) != other.fromBaseUnit(BigFloatNumber.ONE)) return false
        if (multiplier != other.multiplier) return false

        return true
    }

    override fun hashCode(): Int {
        var result = toBaseUnit(BigFloatNumber.ONE).hashCode()
        result = 31 * result + fromBaseUnit(BigFloatNumber.ONE).hashCode()
        result = 31 * result + multiplier.hashCode()
        return result
    }

    override fun toString(): String {
        return "Converter(toBaseUnit=${toBaseUnit(BigFloatNumber.ONE)}, fromBaseUnit=${fromBaseUnit(BigFloatNumber.ONE)}, multiplier=$multiplier)"
    }

    public companion object {
        /**
         * Does no conversion at all.
         */
        public val IDENTITY: Converter = Converter(BigFloatNumber.ONE)
    }
}
