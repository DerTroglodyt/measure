package de.hdc.measure.core

import de.hdc.measure.*
import de.hdc.measure.core.Converter.Companion.IDENTITY
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.numbigfloat.BigFloatNumber.Companion.ONE

/**
 * A derived unit scaled from BaseUnit(s).
 * @param name Name of this unit.
 * @param symbol International symbol.
 * @param quantity Combination of physical dimensions.
 * @param dimension The physical domain of this unit.
 * @param converter Convert from / to BaseUnit.
 */
public open class ScaledUnit<D : PhysicalDimension> private constructor(
    public val name: String,
    public val symbol: String,
    public val quantity: Quantity,
    public val dimension: D,
    public val converter: Converter,
    /**
     * The decimal prefix (like kilo, mega, giga, milli, etc)
     */
    public val prefix: Prefix,
    public val isBaseUnit: Boolean = false
) {
    public val baseUnit: ScaledUnit<D>
        get() = baseUnit(name, symbol, quantity, dimension)

    /**
     * Used in SScaledUnit definition.
     */
    public constructor(
        name: String,
        symbol: String,
        quantity: Quantity,
        dimension: D,
        toBaseUnitMultiplier: BigFloatNumber = ONE,
        isBaseUnit: Boolean = false,
        prefix: Prefix = DecimalPrefix.NONE
    ) : this(
        name,
        symbol,
        quantity,
        dimension,
        if (toBaseUnitMultiplier == ONE) IDENTITY
        else Converter({ it * toBaseUnitMultiplier }, { it / toBaseUnitMultiplier }),
        prefix,
        isBaseUnit
    )

    /**
     * Used in SScaledUnit and ImperialUnits definition.
     */
    public constructor(
        name: String,
        symbol: String,
        unit: ScaledUnit<D>,
        converter: Converter = IDENTITY
    ) : this(
        name,
        symbol,
        unit.quantity,
        unit.dimension,
        converter,
        DecimalPrefix.NONE
    )

    /**
     * Used in derived units definition.
     */
    public constructor(
        name: String,
        symbol: String,
        unit: ScaledUnit<D>,
        toBaseUnitMultiplier: BigFloatNumber = ONE
    ) : this(
        name,
        symbol,
        unit.quantity,
        unit.dimension,
        if (toBaseUnitMultiplier == ONE) IDENTITY
        else Converter(toBaseUnitMultiplier, unit),
        DecimalPrefix.NONE
    )

    override fun toString(): String = symbol

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ScaledUnit<*>) return false
        return (this.quantity == other.quantity)
                && (this.dimension == other.dimension)
                && (this.converter == other.converter)
                && (prefix == other.prefix)
                && (isBaseUnit == other.isBaseUnit)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + quantity.hashCode()
        result = 31 * result + dimension.hashCode()
        result = 31 * result + converter.hashCode()
        result = 31 * result + prefix.hashCode()
        result = 31 * result + isBaseUnit.hashCode()
        return result
    }

//    /**
//     * Combines two units.
//     *
//     * Returns a new unit (possibly simplified)
//     * and the resulting converter necessary to convert any associated value to this new unit.
//     */
//    public operator fun times(other: ScaledUnit<*>): Pair<ScaledUnit<*>, Converter> {
//        val nq = quantity * other.quantity
//        val p = prefix * other.prefix
//        return when {
//            nq.isDimensionless() -> prefixedUnit(p, UNITLESS) to IDENTITY
//            else -> {
//                val u = UnitTable[nq]
//                if (u != null) {
//                    prefixedUnit(p * u.prefix, u) to u.converter
//                } else {
//                    prefixedUnit(p, combinedUnit(nq.toUnitString(), nq.toUnitSymbolString(), nq)) to IDENTITY
//                }
//            }
//        }
//    }

    /**
     * Combines two units.
     *
     * Returns a new unit
     * and the resulting converter necessary to convert any associated value to this new unit.
     */
    public operator fun times(other: ScaledUnit<*>): Pair<ScaledUnit<*>, Converter> {
        val nq = quantity * other.quantity
        val p = prefix * other.prefix
        return if (nq.isDimensionless()) prefixedUnit(p, UNITLESS) to IDENTITY
        else prefixedUnit(p, combinedUnit(nq.toUnitString(), nq.toUnitSymbolString(), nq)) to IDENTITY
    }

//    /**
//     * Combines two units.
//     *
//     * Returns a new unit (possibly simplified)
//     * and the resulting converter necessary to convert any associated value to this new unit.
//     */
//    public operator fun div(other: ScaledUnit<*>): Pair<ScaledUnit<*>, Converter> {
//        val nq = quantity / other.quantity
//        val p = prefix / other.prefix
//        return when {
//            nq.isDimensionless() -> prefixedUnit(p, UNITLESS) to IDENTITY
//            else -> {
//                val u = UnitTable[nq]
//                if (u != null) {
//                    prefixedUnit(p / u.prefix, u) to u.converter.combine(this).combine(other)
//                } else {
//                    prefixedUnit(p, combinedUnit(nq.toUnitString(), nq.toUnitSymbolString(), nq)) to IDENTITY
//                }
//            }
//        }
//    }

    /**
     * Combines two units.
     *
     * Returns a new unit (possibly simplified)
     * and the resulting converter necessary to convert any associated value to this new unit.
     */
    public operator fun div(other: ScaledUnit<*>): Pair<ScaledUnit<*>, Converter> {
        val nq = quantity / other.quantity
        val p = prefix / other.prefix
        return if (nq.isDimensionless()) prefixedUnit(p, UNITLESS) to IDENTITY
        else prefixedUnit(p, combinedUnit(nq.toUnitString(), nq.toUnitSymbolString(), nq)) to IDENTITY
    }

    public fun simplify(): Pair<ScaledUnit<*>, Converter>? {
        return when {
            quantity.isDimensionless() -> prefixedUnit(prefix, UNITLESS) to IDENTITY
            else -> {
                val u = UnitTable[quantity, dimension]
                if ((u == null) || (u === this)) null
                else {
                    prefixedUnit(
//                        prefix.from(u.converter.fromBaseUnit(prefix.multiplier) / u.prefix.multiplier),
                        u.prefix,
                        u
                    ) to u.converter
                }
            }
        }
    }

    /**
     * Tests if the Quantities of both units are equal.
     */
    public fun isEquivalentTo(other: ScaledUnit<*>): Boolean = (this.quantity == other.quantity)


    public companion object {
        /**
         * Returns the combination of units as a new (and possibly simplified) unit.
         * Attention: The internal converters of the involved units are ignored!
         */
        internal fun from(numerator: List<ScaledUnit<*>>, denominator: List<ScaledUnit<*>>): ScaledUnit<Combined> {
            val n = numerator.fold(UNITLESS as ScaledUnit<*>) { a, b -> (a * b).first }
            val d = denominator.fold(UNITLESS as ScaledUnit<*>) { a, b -> (a * b).first }
            return (n / d).first as ScaledUnit<Combined>
        }

        internal fun <D : PhysicalDimension> baseUnit(
            name: String,
            symbol: String,
            quantity: Quantity,
            dimension: D
        ): ScaledUnit<D> =
            ScaledUnit(name, symbol, quantity, dimension, IDENTITY, DecimalPrefix.NONE, true)

        /**
         * Used in Range constructor
         */
        internal fun <D : PhysicalDimension> prefixedUnit(
            prefix: Prefix,
            unit: ScaledUnit<D>
        ): ScaledUnit<D> = ScaledUnit(
            unit.name,
            unit.symbol,
            unit.quantity,
            unit.dimension,
            unit.converter,
            prefix,
            false
        )

        internal fun combinedUnit(name: String, symbol: String, quantity: Quantity): ScaledUnit<Combined> = ScaledUnit(
            name,
            symbol,
            quantity,
            Combined
        )
    }
}
