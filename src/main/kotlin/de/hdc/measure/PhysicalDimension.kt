@file:Suppress("KDocMissingDocumentation")

package de.hdc.measure

/** A unit of a given Quantity or a combination of Quantities. */
public interface PhysicalDimension

// Combined units
public data object Combined : PhysicalDimension

// SI dimensions
public data object Dimensionless : PhysicalDimension
public data object SubstanceAmount : PhysicalDimension
public data object ElectricCurrent : PhysicalDimension
public data object Length : PhysicalDimension
public data object LuminousIntensity : PhysicalDimension
public data object Mass : PhysicalDimension
public data object Temperature : PhysicalDimension
public data object Time : PhysicalDimension

// Non SI dimensions
public data object Pieces : PhysicalDimension

// Derived dimensions
public data object Charge : PhysicalDimension
public data object Area : PhysicalDimension
public data object Volume : PhysicalDimension
public data object Angle : PhysicalDimension
public data object SolidAngle : PhysicalDimension
public data object Velocity : PhysicalDimension
public data object Acceleration : PhysicalDimension
public data object Frequency : PhysicalDimension
public data object Force : PhysicalDimension
public data object Pressure : PhysicalDimension
public data object Energy : PhysicalDimension
public data object Power : PhysicalDimension
public data object Action : PhysicalDimension
public data object AngularVelocity : PhysicalDimension
public data object AngularAcceleration : PhysicalDimension
public data object Density : PhysicalDimension
public data object MolarMass : PhysicalDimension
public data object Molarity : PhysicalDimension
public data object Entropy : PhysicalDimension
public data object GasConstant : PhysicalDimension
public data object InvSubstanceAmount : PhysicalDimension
public data object GravConstant : PhysicalDimension
public data object Proportionality : PhysicalDimension
