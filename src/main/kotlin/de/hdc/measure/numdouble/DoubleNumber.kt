package de.hdc.measure.numdouble

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.toBFN
import kotlin.math.*

/**
 * Implementation of [AlgebraicNumber] with Double precision.
 */
public class DoubleNumber(internal val value: Double) : AlgebraicNumber<Double> {
    public companion object {
        /** Default number of significant digits. */
        public val defaultPrecision: UInt = 15u

        /** Maximum difference allowed in approximates() calls. */
        public val maxDifference: DoubleNumber = DoubleNumber("1e${defaultPrecision - 1u}".toDouble())

        /**
         * Higher precision arc tangent function.
         * https://en.wikipedia.org/wiki/Atan2
         */
        @Suppress("KotlinConstantConditions")
        public fun atan2(y: Double, x: Double): Double {
            return when {
                x > 0.0 -> atan(y / x)
                (x < 0.0) && (y >= 0.0) -> atan(y / x) + PI
                (x < 0.0) && (y < 0.0) -> atan(y / x) - PI
                (x == 0.0) && (y > 0.0) -> PI / 2
                (x == 0.0) && (y < 0.0) -> -(PI / 2)
                (x == 0.0) && (y == 0.0) -> 0.0
                else -> error("Should not happen!")
            }
        }
    }

    override fun compareTo(other: AlgebraicNumber<Double>): Int {
        return if (other is DoubleNumber) {
            value.compareTo(other.value)
        } else {
            -1
        }
    }

    override fun getValue(): Double = value

    override fun plus(other: AlgebraicNumber<Double>): DoubleNumber =
        DoubleNumber(value + other.toDouble())

    override fun minus(other: AlgebraicNumber<Double>): DoubleNumber =
        DoubleNumber(value - other.toDouble())

    override fun times(other: AlgebraicNumber<Double>): DoubleNumber =
        DoubleNumber(value * other.toDouble())

    override fun times(other: Double): DoubleNumber = DoubleNumber(value * other)

    override fun div(other: AlgebraicNumber<Double>): DoubleNumber =
        DoubleNumber(value / other.toDouble())

    override fun div(other: Double): DoubleNumber = DoubleNumber(value / other)
    override fun div(other: Float): DoubleNumber = DoubleNumber(value / other.toDouble())
    override fun div(other: Int): DoubleNumber = DoubleNumber(value / other)
    override fun div(other: Long): DoubleNumber = DoubleNumber(value / other)

    public fun approximates(other: DoubleNumber, maxDiff: Double): Boolean =
        value.approximates(other.value, maxDiff)

    override fun approximates(other: AlgebraicNumber<Double>, maxDiff: AlgebraicNumber<Double>): Boolean =
        value.approximates(other.toDouble(), maxDiff.toDouble())

    override fun toDouble(): Double = value

    @Suppress("DEPRECATION")
    override fun fromSI(prefix: Prefix, converter: Converter): DoubleNumber =
        DoubleNumber((converter.fromBaseUnit(value.toBFN()) / prefix.multiplier).toDouble())

    @Suppress("DEPRECATION")
    override fun toSI(prefix: Prefix, converter: Converter): DoubleNumber =
        DoubleNumber(converter.toBaseUnit(value.toBFN() * prefix.multiplier).toDouble())

    override fun equals(other: Any?): Boolean {
        if (other !is DoubleNumber) return false
        return value == other.value
    }

    override fun isZero(): Boolean = value == 0.0

    override fun isNegative(): Boolean = value < 0.0

    override fun isPositive(): Boolean = value > 0.0

    override fun isNaN(): Boolean = value.isNaN()

    override fun isInfinite(): Boolean = value.isInfinite()

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun toString(): String = value.toString()

    override fun reciprocal(): AlgebraicNumber<Double> = DoubleNumber(1.0 / value)

    override fun inverse(): AlgebraicNumber<Double> = DoubleNumber(-1.0 * value)

    /** Return the absolute (positive) value */
    public fun abs(): DoubleNumber = DoubleNumber(abs(value))

    /** Returns the natural exponent e^x */
    public fun exp(): DoubleNumber = DoubleNumber(exp(value))

    /** Returns the natural logarithm */
    public fun log(base: Double): DoubleNumber = DoubleNumber(log(value, base))

    /** Returns the logarithm to base 2 */
    public fun log2(): DoubleNumber = DoubleNumber(log2(value))

    /** Returns the logarithm to base 10 */
    public fun log10(): DoubleNumber = DoubleNumber(log10(value))

    /** Returns this value raised to the power of exp */
    public fun pow(exp: DoubleNumber): DoubleNumber = DoubleNumber(value.pow(exp.value))

    /** Returns this exp-th root of this */
    public fun root(exp: DoubleNumber): DoubleNumber = DoubleNumber(value.pow(1.0 / exp.value))

    /** Returns the square root */
    public fun sqrt(): DoubleNumber = DoubleNumber(sqrt(value))

    /** Returns the dine */
    public fun sin(): DoubleNumber = DoubleNumber(sin(value))

    /** Returns the hyperbolic sine */
    public fun sinh(): DoubleNumber = DoubleNumber(sinh(value))

    /** Returns the cosine */
    public fun cos(): DoubleNumber = DoubleNumber(cos(value))

    /** Returns the hyperbolic cosine */
    public fun cosh(): DoubleNumber = DoubleNumber(cosh(value))

    /** Returns the tangens */
    public fun tan(): DoubleNumber = DoubleNumber(tan(value))

    /** Returns the hyperbolic tangens */
    public fun tanh(): DoubleNumber = DoubleNumber(tanh(value))
//    public fun cot(): DoubleNumber = DoubleNumber(cot(value))
//    public fun coth(): DoubleNumber = DoubleNumber(coth(value))
    /** Returns the arc sine */
    public fun asin(): DoubleNumber = DoubleNumber(asin(value))

    /** Returns the hyperbolic arc sine */
    public fun asinh(): DoubleNumber = DoubleNumber(asinh(value))

    /** Returns the arc cosine */
    public fun acos(): DoubleNumber = DoubleNumber(acos(value))

    /** Returns the hyperbolic arc cosine */
    public fun acosh(): DoubleNumber = DoubleNumber(acosh(value))

    /** Returns the arc tangens */
    public fun atan(): DoubleNumber = DoubleNumber(atan(value))

    /** Returns the hyperbolic arc tangens */
    public fun atanh(): DoubleNumber = DoubleNumber(atanh(value))
//    public fun acot(): DoubleNumber = DoubleNumber(acot(value))
//    public fun acoth(): DoubleNumber = DoubleNumber(acoth(value))

    /**
     * Higher precision arc tangent function.
     * https://en.wikipedia.org/wiki/Atan2
     */
    public fun atan2(y: DoubleNumber): DoubleNumber = DoubleNumber(atan2(value, y.value))

    /**
     * Sums up all elements in the Iterable.
     */
    public fun Iterable<DoubleNumber>.sumOf(selector: (DoubleNumber) -> DoubleNumber): DoubleNumber {
        var sum = DoubleNumber(0.0)
        for (element in this) {
            sum += selector(element)
        }
        return sum
    }
}
