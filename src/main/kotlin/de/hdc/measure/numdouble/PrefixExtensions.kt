@file:Suppress(
    "NonAsciiCharacters", "FunctionName",
    "KDocMissingDocumentation"
)

package de.hdc.measure.numdouble

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.core.ScaledUnit

public infix fun <D : PhysicalDimension> Double.Y(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.Y..measureUnit)

public infix fun <D : PhysicalDimension> Double.Z(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.Z..measureUnit)

public infix fun <D : PhysicalDimension> Double.X(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.X..measureUnit)

public infix fun <D : PhysicalDimension> Double.P(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.P..measureUnit)

public infix fun <D : PhysicalDimension> Double.T(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.T..measureUnit)

public infix fun <D : PhysicalDimension> Double.G(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.G..measureUnit)

public infix fun <D : PhysicalDimension> Double.M(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.M..measureUnit)

public infix fun <D : PhysicalDimension> Double.k(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.k..measureUnit)

public infix fun <D : PhysicalDimension> Double.h(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.h..measureUnit)

public infix fun <D : PhysicalDimension> Double.da(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.da..measureUnit)

public infix fun <D : PhysicalDimension> Double.ˍ(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.NONE..measureUnit)

public infix fun <D : PhysicalDimension> Double.d(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.d..measureUnit)

public infix fun <D : PhysicalDimension> Double.c(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.c..measureUnit)

public infix fun <D : PhysicalDimension> Double.m(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.m..measureUnit)

public infix fun <D : PhysicalDimension> Double.µ(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.µ..measureUnit)

public infix fun <D : PhysicalDimension> Double.n(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.n..measureUnit)

public infix fun <D : PhysicalDimension> Double.p(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.p..measureUnit)

public infix fun <D : PhysicalDimension> Double.f(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.f..measureUnit)

public infix fun <D : PhysicalDimension> Double.a(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.a..measureUnit)

public infix fun <D : PhysicalDimension> Double.z(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.z..measureUnit)

public infix fun <D : PhysicalDimension> Double.y(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.y..measureUnit)

//###########################################################################
public infix fun <D : PhysicalDimension> Float.Y(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.Y..measureUnit)

public infix fun <D : PhysicalDimension> Float.Z(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.Z..measureUnit)

public infix fun <D : PhysicalDimension> Float.X(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.X..measureUnit)

public infix fun <D : PhysicalDimension> Float.P(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.P..measureUnit)

public infix fun <D : PhysicalDimension> Float.T(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.T..measureUnit)

public infix fun <D : PhysicalDimension> Float.G(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.G..measureUnit)

public infix fun <D : PhysicalDimension> Float.M(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.M..measureUnit)

public infix fun <D : PhysicalDimension> Float.k(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.k..measureUnit)

public infix fun <D : PhysicalDimension> Float.h(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.h..measureUnit)

public infix fun <D : PhysicalDimension> Float.da(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.da..measureUnit)

public infix fun <D : PhysicalDimension> Float.ˍ(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.NONE..measureUnit)

public infix fun <D : PhysicalDimension> Float.d(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.d..measureUnit)

public infix fun <D : PhysicalDimension> Float.c(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.c..measureUnit)

public infix fun <D : PhysicalDimension> Float.m(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.m..measureUnit)

public infix fun <D : PhysicalDimension> Float.µ(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.µ..measureUnit)

public infix fun <D : PhysicalDimension> Float.n(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.n..measureUnit)

public infix fun <D : PhysicalDimension> Float.p(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.p..measureUnit)

public infix fun <D : PhysicalDimension> Float.f(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.f..measureUnit)

public infix fun <D : PhysicalDimension> Float.a(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.a..measureUnit)

public infix fun <D : PhysicalDimension> Float.z(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.z..measureUnit)

public infix fun <D : PhysicalDimension> Float.y(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this.toDouble(), DecimalPrefix.y..measureUnit)

//###########################################################################
public infix fun <D : PhysicalDimension> DoubleNumber.Y(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.Y..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.Z(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.Z..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.X(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.X..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.P(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.P..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.T(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.T..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.G(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.G..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.M(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.M..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.k(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.k..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.h(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.h..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.da(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.da..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.ˍ(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.NONE..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.d(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.d..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.c(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.c..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.m(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.m..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.µ(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.µ..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.n(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.n..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.p(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.p..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.f(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.f..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.a(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.a..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.z(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.z..measureUnit)

public infix fun <D : PhysicalDimension> DoubleNumber.y(measureUnit: ScaledUnit<D>): UnitNumberDouble<D> =
    UnitNumberDouble(this, DecimalPrefix.y..measureUnit)
