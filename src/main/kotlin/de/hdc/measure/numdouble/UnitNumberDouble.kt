package de.hdc.measure.numdouble

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.toBFN

/**
 * An immutable value and an associated unit.
 * Implementations vary in used backing fields for values (Long / Double / BigDecimal)
 *
 * @param value The internal value in SI base unit
 * @param unit The external BaseUnit / ScaledUnit
 * @param convert Convert to SI units. Set true for first time construction. False for calculations afterwards.
 */
public class UnitNumberDouble<D : PhysicalDimension> internal constructor(
    value: DoubleNumber,
    override val unit: ScaledUnit<D>,
    convert: Boolean = true
) : UnitNumber<Double, DoubleNumber, D>,
    AlgebraicNumber<Double>,// by value,
    Comparable<AlgebraicNumber<Double>> {

    override val baseValue: DoubleNumber = if (convert) {
        value.toSI(unit.prefix, unit.converter)
    } else {
        value
    }

    internal constructor(
        value: Double,
        unit: ScaledUnit<D>,
        convert: Boolean = true
    ) : this(DoubleNumber(value), unit, convert)

    override infix fun <ND : PhysicalDimension> toUnit(newUnit: ScaledUnit<ND>): UnitNumberDouble<ND> {
        require(this.unit.quantity == newUnit.quantity)
        return UnitNumberDouble(baseValue, newUnit, false)
    }

    override fun toString(): String = format(decimals = 1u, padding = 0u)

    override fun compareTo(other: AlgebraicNumber<Double>): Int {
        return when {
            other !is UnitNumber<*, *, *> -> -1
            unit.quantity != other.unit.quantity -> -1
            else -> {
                if (other.baseValue !is DoubleNumber) return -1
                baseValue.compareTo(other.baseValue as DoubleNumber)
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnitNumber<*, *, *>

        return (unit.quantity == other.unit.quantity) &&
                ((unit.dimension == other.unit.dimension) ||
                        (unit.dimension == Combined) ||
                        (other.unit.dimension == Combined)) &&
                (baseValue == other.baseValue)
    }

    override fun hashCode(): Int {
        var result = unit.hashCode()
        result = 31 * result + baseValue.hashCode()
        return result
    }

    // Implement AlgebraicNumber ##########################################
    /**
     * Returns the internal value in SI base unit which may differ from then display unit.
     */
    @Deprecated(
        "Most Likely not the value you are looking for! " +
                "Returns the internal value in SI base unit which may differ from the display unit.",
        ReplaceWith("value.getPrintValue()")
    )
    override fun getValue(): Double = baseValue.getValue()

    override fun isZero(): Boolean = baseValue.isZero()
    override fun isNegative(): Boolean = baseValue.isNegative()
    override fun isPositive(): Boolean = baseValue.isPositive()
    override fun isNaN(): Boolean = baseValue.isNaN()
    override fun isInfinite(): Boolean = baseValue.isInfinite()

    public override fun simplify(): UnitNumber<*, *, *> {
        val u = unit.simplify()
        return if (u == null) this
        else UnitNumberDouble(u.second.fromBaseUnit(baseValue.toBFN()).toDouble(), u.first, true)
    }

    override fun approximates(other: AlgebraicNumber<Double>, maxDiff: AlgebraicNumber<Double>): Boolean =
        baseValue.approximates(other, maxDiff)

    override fun reciprocal(): UnitNumberDouble<*> {
        val u = UNITLESS / unit
        return UnitNumberDouble(u.second.fromBaseUnit(baseValue.reciprocal().getValue().toBFN()).toDouble(), u.first)
    }

    override fun inverse(): UnitNumberDouble<D> = UnitNumberDouble(baseValue.inverse() as DoubleNumber, unit)

    override fun fromSI(prefix: Prefix, converter: Converter): DoubleNumber =
        baseValue.fromSI(prefix, converter)

    override fun toSI(prefix: Prefix, converter: Converter): DoubleNumber =
        baseValue.toSI(prefix, converter)

    override operator fun times(other: Double): UnitNumberDouble<D> =
        UnitNumberDouble(baseValue * other, unit, false)

    override operator fun times(other: Float): UnitNumberDouble<D> = this * other.toDouble()
    override operator fun times(other: Int): UnitNumberDouble<D> = this * other.toDouble()
    override operator fun times(other: Long): UnitNumberDouble<D> = this * other.toDouble()

    public fun times(other: UnitNumber<Double, DoubleNumber, D>): UnitNumberDouble<*> {
        return UnitNumberDouble(baseValue * other.baseValue, unit, false)
    }

    override operator fun div(other: Double): UnitNumberDouble<D> =
        UnitNumberDouble(baseValue / other, unit, false)

    /**
     * Scales this value by a factor.
     */
    override operator fun div(other: Float): UnitNumberDouble<D> =
        UnitNumberDouble(baseValue / other, unit, false)

    /**
     * Scales this value by a factor.
     */
    override operator fun div(other: Int): UnitNumberDouble<D> =
        UnitNumberDouble(baseValue / other, unit, false)

    /**
     * Scales this value by a factor.
     */
    override operator fun div(other: Long): UnitNumberDouble<D> =
        UnitNumberDouble(baseValue / other, unit, false)

    public fun div(other: UnitNumber<Double, DoubleNumber, D>): UnitNumberDouble<D> {
        return UnitNumberDouble(baseValue / other.baseValue, unit, false)
    }

    /**
     * Adds two values in the same PhysicalDimension.
     */
    override operator fun plus(other: AlgebraicNumber<Double>): UnitNumberDouble<D> {
        require(other is UnitNumberDouble<*>) { "Can not add '${this.javaClass.name}' to '${other.javaClass.name}'!" }

        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberDouble<D> ?: error("Can not add '${this.unit}' to '${other.unit}'!")
        if (unit.quantity != other.unit.quantity)
            error("Can not add '${unit.quantity}' to '${other.unit.quantity}'!")
        return UnitNumberDouble(baseValue + other.baseValue, unit, false)
    }

    /**
     * Subtracts two values in the same PhysicalDimension.
     */
    override operator fun minus(other: AlgebraicNumber<Double>): UnitNumberDouble<D> {
        require(other is UnitNumberDouble<*>) {
            "Can not subtract '${other.javaClass.name}' from '${this.javaClass.name}'!"
        }

        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberDouble<D> ?: error("Can not subtract '${other.unit}' from '${this.unit}'!")
        if (unit.quantity != other.unit.quantity)
            error("Can not subtract '${other.unit.quantity}' from '${this.unit.quantity}'!")
        return UnitNumberDouble(baseValue - other.baseValue, unit, false)
    }

    public operator fun times(other: UnitNumberDouble<*>): UnitNumberDouble<*> {
        @Suppress("UNCHECKED_CAST")
        other as? UnitNumber<Double, DoubleNumber, D> ?: error("Can not multiply '${this.unit}' with '${other.unit}'!")
        val u = unit * other.unit
        return UnitNumberDouble(u.second.fromBaseUnit((baseValue * other.baseValue).toBFN()).toDouble(), u.first, false)
    }

    override operator fun times(other: AlgebraicNumber<Double>): UnitNumber<Double, DoubleNumber, D> {
        return UnitNumberDouble(baseValue * other, unit, false)
    }

    public operator fun div(other: UnitNumberDouble<*>): UnitNumberDouble<*> {
        @Suppress("UNCHECKED_CAST")
        other as? UnitNumber<Double, DoubleNumber, D> ?: error("Can not divide '${this.unit}' by '${other.unit}'!")
        val u = unit / other.unit
        return UnitNumberDouble(u.second.fromBaseUnit((baseValue / other.baseValue).toBFN()).toDouble(), u.first, false)
    }

    override operator fun div(other: AlgebraicNumber<Double>): UnitNumberDouble<D> {
        return UnitNumberDouble(baseValue / other, unit, false)
    }
}

/** Scales a UnitNumberDouble by a factor */
public operator fun <D : PhysicalDimension> Double.times(other: UnitNumberDouble<D>): UnitNumberDouble<D> =
    other * this

/** Scales a UnitNumberDouble by a factor */
public operator fun <D : PhysicalDimension> Float.times(other: UnitNumberDouble<D>): UnitNumberDouble<D> =
    other * this

/** Scales a UnitNumberDouble by a factor */
public operator fun <D : PhysicalDimension> Int.times(other: UnitNumberDouble<D>): UnitNumberDouble<D> =
    other * this

/** Scales a UnitNumberDouble by a factor */
public operator fun <D : PhysicalDimension> Long.times(other: UnitNumberDouble<D>): UnitNumberDouble<D> =
    other * this
