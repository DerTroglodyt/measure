@file:Suppress("ObjectPropertyName")

package de.hdc.measure

import de.hdc.measure.core.*
import de.hdc.measure.core.ScaledUnit.Companion.from
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.numbigfloat.BigFloatNumber.Companion.defaultContext

@Suppress("NonAsciiCharacters")
public
/**
 * Most common mathematical and physical constants.
 */
class Const {
    public companion object {
        /**
         *  π constant
         */
        public val PI: UnitNumberBF<Dimensionless> =
            UnitNumberBF(defaultContext.pi().toBFN(), UNITLESS)

        /**
         *  π constant
         */
        public val π: UnitNumberBF<Dimensionless> = PI

        /**
         * Euler constant
         */
        public val euler: UnitNumberBF<Dimensionless> =
            UnitNumberBF(defaultContext.e().toBFN(), UNITLESS)

        /**
         * Gravitational constant
         */
        public val G: UnitNumberBF<Combined> =
            UnitNumberBF(
                "6.67430e-14".toBFN(),
                from(listOf(m, m, m), listOf(g, s, s))
            )

        /**
         *
         */
        public val gₙ: UnitNumberBF<Acceleration> = UnitNumberBF("9.80665".toBFN(), m_s2)

        /**
         * Light speed
         */
        public val c: UnitNumberBF<Velocity> =
            UnitNumberBF("299792458".toBFN(), m_s)

        /**
         * Planck
         */
        public val h: UnitNumberBF<Action> =
            UnitNumberBF("6.62607015e-34".toBFN(), Js)

        /**
         * Thermodynamics: Boltzmann
         */
        public val k: UnitNumberBF<Combined> =
            UnitNumberBF("1.380649e-23".toBFN(), from(listOf(J), listOf(K)))

        /**
         * Thermodynamics: Stefan-Boltzmann
         */
        public val σ: UnitNumberBF<Combined> =
            UnitNumberBF("5.670367e-8".toBFN(), from(listOf(W), listOf(m2, K, K, K, K)))

        /**
         * Universal gas constant
         */
        public val R: UnitNumberBF<Combined> =
            UnitNumberBF("8.31446261815324".toBFN(), from(listOf(J), listOf(K, mol)))

        /**
         * Elementary charge
         */
        public val e: UnitNumberBF<Charge> =
            UnitNumberBF("1.602176634e-19".toBFN(), C)

        /**
         * Avogadro
         */
        public val N: UnitNumberBF<Combined> =
            UnitNumberBF("6.02214076e23".toBFN(), from(listOf(UNITLESS), listOf(mol)))

        /**
         * Molar mass
         */
        public val Mu: UnitNumberBF<MolarMass> =
            UnitNumberBF("0.99999999965".toBFN(), g_mol)

    }
}
