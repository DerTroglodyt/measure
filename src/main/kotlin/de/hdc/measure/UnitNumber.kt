package de.hdc.measure

import ch.obermuhlner.math.big.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.numdouble.*
import de.hdc.measure.numinteger.*
import kotlin.math.*

/**
 * An immutable value and an associated unit.
 * Implementations vary in used backing fields for values (Long / Double / BigDecimal)
 *
 * Usage examples:
 *
 *    val distance = 12.53 k m
 *    val x = distance + (45.67 c m)
 *    // pretty printing by default
 *    println(x)
 *    // 12.5 km
 *
 *    // formatted printing
 *    println(x.format(7, 0))
 *    // 12.530 456 7 km
 *
 *    // formatted printing
 *    println(x.format(7, 0))
 *    // 12.530 456 7 km
 *
 *    // formatted printing without unit optimization
 *    println(x.format(4, 0, false))
 *    // 12 530.456 7 m
 *
 *    // conversion to other unit
 *    println((x to AU).format())
 *    // 83.761 nAU
 *
 *    val celsius = 32.0 ˍ `°C`
 *    val kelvin = celsius to K
 *    println("$celsius == $kelvin")
 *    // 32.0 °C == 305.2 K
 *
 *    // Ensure type safety of parameters and return types:
 *    fun foo(
 *        distance: UnitNumDouble<Length>,
 *        elapsed: UnitNumDouble<Time>
 *    ): UnitNumDouble<Velocity> {
 *        return (distance / elapsed) as UnitNumDouble<Velocity>
 *    }
 *
 *    // Integer, Double or arbitrary precision:
 *    val a: UnitNumLong<Length> = 34 ˍ m
 *    val b: UnitNumDouble<Length> = 3.41 c m
 *    val c: UnitNumDouble<Length> = 0.001 k m
 *    val hp: UnitNumberBF<Length> = "1.23456789012345678901234567890" k m
 *
 *    // Special Money type to avoid rounding errors:
 *    val dollar: Money<USD> = "15.99" ˍ USD
 *    val euro: Money<EUR> = "15.99" ˍ EUR
 */
public interface UnitNumber<T : Any, AN : AlgebraicNumber<T>, D : PhysicalDimension>
    : AlgebraicNumber<T>,// by value,
    Comparable<AlgebraicNumber<T>> {
    /**
     * The raw value in SI base unit including prefix.
     */
    public val baseValue: AlgebraicNumber<T>

    /**
     * The value in the assigned unit (excluding prefix).
     * Differs from the baseValue in SI units.
     */
    public val printValue: AlgebraicNumber<T>
        get() = fromSI(unit.prefix, unit.converter)

    /**
     * The external BaseUnit / ScaledUnit
     */
    public val unit: ScaledUnit<D>

    /**
     * Forced conversion to another unit with the same Quantity.
     * May fail at runtime!
     */
    public infix fun <ND : PhysicalDimension> toUnit(newUnit: ScaledUnit<ND>): UnitNumber<T, AN, ND>

    /**
     * Returns a formatted String.
     */
    public fun format(decimals: UInt = 3u, padding: UInt = 4u, optimize: Boolean = false): String {
        fun addUnit(n: String, u: String): String = n + if (u.isEmpty()) "" else "${Typography.nbsp}$u"

        // special cases
        when {
            isNaN() -> return addUnit("NaN", unit.toString())
            isInfinite() -> return addUnit("∞", unit.toString())
        }

        val (v, prefix, u) = findOptimalPrefix(optimize)
        val pu = prefix.toString() + u.toString()

        var s = v.format(decimals)
        var x = s.indexOf('.')
        if (x < 0) x = s.length
        x = padding.toInt() - x
        if (x > 0) {
            (1..x).forEach { i -> s = " $s" }
        }
        return addUnit(s, pu)
    }

    override fun toString(): String
    override fun compareTo(other: AlgebraicNumber<T>): Int
    override fun equals(other: Any?): Boolean
    override fun hashCode(): Int

    /**
     * Tries to find a more compressed Unit from the ScaledUnit.UnitTable.
     */
    public fun simplify(): UnitNumber<*, *, *>
    override fun approximates(other: AlgebraicNumber<T>, maxDiff: AlgebraicNumber<T>): Boolean

    /**
     * Tests if this number does not represent a valid numerical value.
     */
    public fun isInvalid(): Boolean {
        return ((this.baseValue.toDouble() == Double.POSITIVE_INFINITY)
                || (this.baseValue.toDouble() == Double.NEGATIVE_INFINITY)
                || (this.baseValue.toDouble() == Double.MAX_VALUE)
                || (this.baseValue.toDouble() == Double.MIN_VALUE)
                || (this.baseValue.toDouble().isNaN()))
    }

    /**
     * Tests if this number represents a valid numerical value.
     */
    public fun isValid(): Boolean = !this.isInvalid()

    public override fun reciprocal(): UnitNumber<T, AN, *>
    override fun inverse(): UnitNumber<T, AN, D>

    /**
     * Scales this value by a factor.
     */
    public override operator fun times(other: Double): UnitNumber<T, AN, D>

    /**
     * Scales this value by a factor.
     */
    public override operator fun times(other: Float): UnitNumber<T, AN, D>

    /**
     * Scales this value by a factor.
     */
    public override operator fun times(other: Int): UnitNumber<T, AN, D>

    /**
     * Scales this value by a factor.
     */
    public override operator fun times(other: Long): UnitNumber<T, AN, D>

    /**
     * Scales this value by a factor.
     */
    public override operator fun div(other: Double): UnitNumber<T, AN, D>

    /**
     * Scales this value by a factor.
     */
    public override operator fun div(other: Float): UnitNumber<T, AN, D>

    /**
     * Scales this value by a factor.
     */
    public override operator fun div(other: Int): UnitNumber<T, AN, D>

    /**
     * Scales this value by a factor.
     */
    public override operator fun div(other: Long): UnitNumber<T, AN, D>

    /**
     * Adds two values in the same PhysicalDimension.
     */
    public override operator fun plus(other: AlgebraicNumber<T>): UnitNumber<T, AN, D>

    /**
     * Subtracts two values in the same PhysicalDimension.
     */
    public override operator fun minus(other: AlgebraicNumber<T>): UnitNumber<T, AN, D>

    //    public operator fun times(other: UnitNumber<T, AN, D>): UnitNumber<T, AN, *>
    public override operator fun times(other: AlgebraicNumber<T>): UnitNumber<T, AN, D>

    //    public operator fun div(other: UnitNumber<T, AN, D>): UnitNumber<T, AN, *>
    public override operator fun div(other: AlgebraicNumber<T>): UnitNumber<T, AN, D>

    /**
     * Converts prefix * printValue to a Double.
     */
    override fun toDouble(): Double = fromSI(DecimalPrefix.NONE, unit.converter).toDouble()

    /**
     * Converts prefix * printValue to an Int.
     */
    override fun toInt(): Int = fromSI(DecimalPrefix.NONE, unit.converter).toInt()

    /**
     * Converts prefix * printValue to a Long.
     */
    override fun toLong(): Long = fromSI(DecimalPrefix.NONE, unit.converter).toLong()

    /**
     * Converts prefix * printValue to a Short.
     */
    override fun toShort(): Short = fromSI(DecimalPrefix.NONE, unit.converter).toShort()

    /**
     * Converts prefix * printValue to a Byte.
     */
    override fun toByte(): Byte = fromSI(DecimalPrefix.NONE, unit.converter).toByte()

    /**
     * Converts prefix * printValue to a Float.
     */
    override fun toFloat(): Float = fromSI(DecimalPrefix.NONE, unit.converter).toFloat()

    /**
     * Converts printValue to a Char.
     */
    @Deprecated(
        "Direct conversion to Char is deprecated. Use toInt().toChar() or Char constructor" +
                "instead.\n" +
                "If you override toChar() function in your Number inheritor, it's recommended to" +
                "gradually deprecate the overriding function and then remove it.\n" +
                "See https://youtrack.jetbrains.com/issue/KT-46465 for details about the migration",
        replaceWith = ReplaceWith("this.toInt().toChar()")
    )
    override fun toChar(): Char = toInt().toChar()

    /**
     * Truncates a value into a given range.
     */
    public fun clamp(min: UnitNumber<T, AN, D>, max: UnitNumber<T, AN, D>): UnitNumber<T, AN, D> {
        return when {
            (this < min) -> min
            (this > max) -> max
            else -> this
        }
    }

    /**
     * Maps a value from one range onto another range.
     */
    public fun mapTo(
        from: ClosedRange<UnitNumber<T, AN, D>>,
        to: ClosedRange<UnitNumber<T, AN, D>>
    ): UnitNumber<T, AN, D> {
        require(from.contains(this)) { "$this is not in from range $from!" }
        require(from.start != from.endInclusive) { "From interval width must be greater than zero!" }
        val slope =
            (to.endInclusive - to.start).baseValue.toDouble() / (from.endInclusive - from.start).baseValue.toDouble()
        return to.start + (this - from.start) * slope
    }

    // Contains all factory methods to create a UnitNumberImpl
    public companion object {
        public operator fun <D : PhysicalDimension> invoke(
            value: BigFloatNumber,
            unit: ScaledUnit<D>,
            convert: Boolean = true
        ): UnitNumber<BigFloat, BigFloatNumber, D> {
            return UnitNumberBF(value, unit, convert)
        }

        public operator fun <D : PhysicalDimension> invoke(
            value: String,
            unit: ScaledUnit<D>,
            convert: Boolean = true
        ): UnitNumber<BigFloat, BigFloatNumber, D> {
            return UnitNumberBF(value.toBFN(), unit, convert)
        }

        public operator fun <D : PhysicalDimension> invoke(
            value: Float,
            unit: ScaledUnit<D>,
            convert: Boolean = true
        ): UnitNumber<Double, DoubleNumber, D> {
            return UnitNumberDouble(DoubleNumber(value.toDouble()), unit, convert)
        }

        public operator fun <D : PhysicalDimension> invoke(
            value: Double,
            unit: ScaledUnit<D>,
            convert: Boolean = true
        ): UnitNumber<Double, DoubleNumber, D> {
            return UnitNumberDouble(DoubleNumber(value), unit, convert)
        }

        public operator fun <D : PhysicalDimension> invoke(
            value: Int,
            unit: ScaledUnit<D>,
            convert: Boolean = true
        ): UnitNumber<Long, LongNumber, D> {
            return UnitNumberLong(LongNumber(value.toLong()), unit, convert)
        }

        public operator fun <D : PhysicalDimension> invoke(
            value: Long,
            unit: ScaledUnit<D>,
            convert: Boolean = true
        ): UnitNumber<Long, LongNumber, D> {
            return UnitNumberLong(LongNumber(value), unit, convert)
        }
    }
}

/**
 * Sums up all elements in the Iterable.
 * @param initialValue The starting value of the accumulator.
 */
public fun <T : Any, AN : AlgebraicNumber<T>, D : PhysicalDimension> Iterable<UnitNumber<T, AN, D>>.sumOf(
    initialValue: UnitNumber<T, AN, D>
): UnitNumber<T, AN, D> {
    var sum = initialValue
    for (element in this) {
        sum += element
    }
    return sum
}

/**
 * Sums up all elements in the Iterable.
 * @param initialValue The starting value of the accumulator.
 * @param selector Calvulates the new accumulator value from the old and a list element.
 */
public fun <T : Any, AN : AlgebraicNumber<T>, D : PhysicalDimension> Iterable<UnitNumber<T, AN, D>>.fold(
    initialValue: UnitNumber<T, AN, D>,
    selector: (accumulator: UnitNumber<T, AN, D>, element: UnitNumber<T, AN, D>) -> UnitNumber<T, AN, D>
): UnitNumber<T, AN, D> {
    var acc = initialValue
    for (element in this) {
        acc = selector(acc, element)
    }
    return acc
}

/**
 * Tries to shorten the displayed value by finding a better prefix.
 */
internal fun UnitNumber<*, *, *>.findOptimalPrefix(optimize: Boolean): Triple<Double, Prefix, ScaledUnit<*>> {
    var v = toDouble()
    var u: ScaledUnit<*> = unit
    if (optimize) {
        // special cases that replace base unit
        if (!unit.quantity.isDimensionless()) {
            val scu = ScaledUnitTable[unit.quantity]
            run loop@{
                scu?.forEach { su ->
                    if (abs(v) >= su.converter.multiplier.toDouble()) {
                        v = fromSI(su.prefix, su.converter).toDouble()
                        u = su
                        return@loop
                    }
                }
            }
        }
    }
    var p = DecimalPrefix.NONE

    if (abs(v) < 1.0 && (v != 0.0)) {
        while ((abs(v) < 1.0) && (!p.isLast())) {
            v *= (p.up().multiplier.toDouble() / p.multiplier.toDouble())
            p = p.down()
        }
    } else if (abs(v) >= 1_000.0) {
        while ((abs(v) >= 1_000.0) && (!p.isLast())) {
            v /= (p.up().multiplier.toDouble() / p.multiplier.toDouble())
            p = p.up()
        }
    }
    return Triple(v, p, u)
}
