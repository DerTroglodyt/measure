package de.hdc.measure.money

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numinteger.*
import de.hdc.measure.format
import java.math.BigDecimal
import java.util.Currency

/**
 * Holds information on a specific currency of a Money value.
 * Money values are stored as an integer number of the money value in cent (if applicable).
 *
 * Convenience constructors exist for Credits, USD, EUR, CNY, JPY and GBP:
 *
 * `val dollar = "3.45" ˍ USD`  // $3.45
 *
 * `val dollar = "3.45" M USD`  // $3 450 000.00
 *
 * @param name Official name
 * @param symbol Official currency symbol
 * @param isoCode International ISO code
 * @param fractionDigits How many digits are allowed after decimal point
 */
public abstract class MoneyCurrency(
    public val name: String, public val symbol: String, public val isoCode: String, public val fractionDigits: UInt
) {
    /**
     * Factor to convert internal Long representation to Double currency value.
     */
    internal val fractionalFactor: Long = BigDecimal("10.0").pow(fractionDigits.toInt()).toLong()

    init {
        require(fractionDigits >= 0u)
    }

    internal constructor(cur: Currency) : this(
        cur.displayName,
        cur.symbol,
        cur.currencyCode,
        cur.defaultFractionDigits.toUInt()
    )
}

/** Fictional currency unit */
public object CRD : MoneyCurrency("Credits", "Cr", "CRD", 0u)

/** US Dollar */
public object USD : MoneyCurrency(Currency.getInstance("USD"))

/** Euro */
public object EUR : MoneyCurrency(Currency.getInstance("EUR"))

/** Chinese Yen */
public object CNY : MoneyCurrency(Currency.getInstance("CNY"))

/** Japanese Yen */
public object JPY : MoneyCurrency(Currency.getInstance("JPY"))

/** British Pound */
public object GBP : MoneyCurrency(Currency.getInstance("GBP"))

/**
 * Holds a value in a specific currency.
 * Money values are stored as an integer number of the money value in cent (if applicable).
 * All arithmetic is done in the smallest whole currency units.
 * toString() and toDouble() auto converts the Long value back into Double value with fractions.
 */
public class Money<C : MoneyCurrency> private constructor(
    /**
     * The already converted Long value to store.
     */
    value: Long,
    /**
     * The currency this value is representing.
     */
    internal val cur: C
) : LongNumber(value) {
    @Deprecated(
        """
        Possible loss of precision when applying fractional factor of currency. 
        Use String constant instead of Double for unlimited precision.
    """,
        level = DeprecationLevel.WARNING
    )
    public constructor(value: Double, currency: C) : this((value * currency.fractionalFactor).toLong(), currency)
    public constructor(value: String, currency: C) : this(
        (BigDecimal(value) * BigDecimal(currency.fractionalFactor)).toLong(), currency
    )

    internal constructor(prefix: DecimalPrefix, value: String, currency: C) : this(
        (prefix.multiplier.value.toBigDecimal() * BigDecimal(value) * BigDecimal(currency.fractionalFactor)).toLong(),
        currency
    )

    override fun toDouble(): Double = value.toDouble() / cur.fractionalFactor
    override fun toString(): String = toDouble().format(cur.fractionDigits) + "${Typography.nbsp}${cur.symbol}"

    /**
     * Returns the resulting Money value of an addition.
     */
    public operator fun plus(other: Money<C>): Money<C> = Money(this.value + other.value, cur)

    /**
     * Returns the resulting Money value of an subtraction.
     */
    public operator fun minus(other: Money<C>): Money<C> = Money(this.value - other.value, cur)

    /**
     * Returns the whole Money value and discards the remaining fraction that can't be stored in Money units.
     * Possible precision loss by conversion of other.toDouble()!
     */
    public operator fun times(other: Number): Money<C> {
        if (other is UnitNumber<*, *, *>) {
            require(other.unit.quantity.isDimensionless()) {
                "Only scalar multiplication allowed!"
            }
        }
        val real = this.value * other.toDouble()
        return Money(real.toLong(), cur)
    }

    /**
     * Returns the whole Money value and the remaining fraction that can't be stored in Money units.
     * Possible precision loss by conversion of other.toDouble()!
     */
    public fun timesRem(other: Number): Pair<Money<C>, Double> {
        if (other is UnitNumber<*, *, *>) {
            require(other.unit.quantity.isDimensionless()) {
                "Only scalar multiplication allowed!"
            }
        }
        val real = this.value * other.toDouble()
        val residual = (real - real.toLong()) / cur.fractionalFactor
        return Money(real.toLong(), cur) to residual
    }

    /**
     * Returns the whole Money value and discards the remaining fraction that can't be stored in Money units.
     * Possible precision loss by conversion of other.toDouble()!
     */
    public operator fun div(other: Number): Money<C> {
//        require(other !is Money<*>) { "Can not divide two Money values!" }
        if (other is UnitNumber<*, *, *>) {
            require(other.unit.quantity.isDimensionless()) {
                "Only scalar division allowed!"
            }
        }
        val real = this.value / other.toDouble()
        return Money(real.toLong(), cur)
    }

    /**
     * Returns the whole Money value and the remaining fraction that can't be stored in Money units.
     * Possible precision loss by conversion of other.toDouble()!
     */
    public fun divRem(other: Number): Pair<Money<C>, Double> {
//        require(other !is Money<*>) { "Can not divide two Money values!" }
        if (other is UnitNumber<*, *, *>) {
            require(other.unit.quantity.isDimensionless()) {
                "Only scalar division allowed!"
            }
        }
        val real = this.value / other.toDouble()
        val residual = (real - real.toLong()) / cur.fractionalFactor
        return Money(real.toLong(), cur) to residual
    }

    @Deprecated("Not implemented.", level = DeprecationLevel.ERROR)
    override fun reciprocal(): AlgebraicNumber<Long> {
        throw IllegalAccessException("No reciprocal of Money implemented!")
    }

    public override fun compareTo(other: AlgebraicNumber<Long>): Int {
        if ((other is Money<*>) && (other.cur == cur)) {
            return value.compareTo(other.value)
        }
        return -1
    }
}
