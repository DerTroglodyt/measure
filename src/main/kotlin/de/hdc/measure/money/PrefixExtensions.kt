@file:Suppress("FunctionName", "NonAsciiCharacters", "KDocMissingDocumentation")

package de.hdc.measure.money

import de.hdc.measure.core.*

public infix fun <D : MoneyCurrency> String.y(currency: D): Money<D> =
    Money(DecimalPrefix.y, this, currency)

public infix fun <D : MoneyCurrency> String.z(currency: D): Money<D> =
    Money(DecimalPrefix.z, this, currency)

public infix fun <D : MoneyCurrency> String.a(currency: D): Money<D> =
    Money(DecimalPrefix.a, this, currency)

public infix fun <D : MoneyCurrency> String.f(currency: D): Money<D> =
    Money(DecimalPrefix.f, this, currency)

public infix fun <D : MoneyCurrency> String.p(currency: D): Money<D> =
    Money(DecimalPrefix.p, this, currency)

public infix fun <D : MoneyCurrency> String.n(currency: D): Money<D> =
    Money(DecimalPrefix.n, this, currency)

public infix fun <D : MoneyCurrency> String.µ(currency: D): Money<D> =
    Money(DecimalPrefix.µ, this, currency)

public infix fun <D : MoneyCurrency> String.m(currency: D): Money<D> =
    Money(DecimalPrefix.m, this, currency)

public infix fun <D : MoneyCurrency> String.ˍ(currency: D): Money<D> =
    Money(DecimalPrefix.NONE, this, currency)

public infix fun <D : MoneyCurrency> String.k(currency: D): Money<D> =
    Money(DecimalPrefix.k, this, currency)

public infix fun <D : MoneyCurrency> String.M(currency: D): Money<D> =
    Money(DecimalPrefix.M, this, currency)

public infix fun <D : MoneyCurrency> String.G(currency: D): Money<D> =
    Money(DecimalPrefix.G, this, currency)

public infix fun <D : MoneyCurrency> String.T(currency: D): Money<D> =
    Money(DecimalPrefix.T, this, currency)

public infix fun <D : MoneyCurrency> String.P(currency: D): Money<D> =
    Money(DecimalPrefix.P, this, currency)

public infix fun <D : MoneyCurrency> String.X(currency: D): Money<D> =
    Money(DecimalPrefix.X, this, currency)

public infix fun <D : MoneyCurrency> String.Z(currency: D): Money<D> =
    Money(DecimalPrefix.Z, this, currency)

public infix fun <D : MoneyCurrency> String.Y(currency: D): Money<D> =
    Money(DecimalPrefix.Y, this, currency)
