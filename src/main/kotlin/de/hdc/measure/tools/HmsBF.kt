package de.hdc.measure.tools

import ch.obermuhlner.math.big.*
import ch.obermuhlner.math.big.kotlin.bigfloat.*
import de.hdc.measure.*
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.Const.Companion.π
import kotlin.math.*

//todo
/**
 * Container for times / angles
 * hours = [0..23]
 * minutes = [0..59]
 * seconds = [0.0..59.9999999]
 */
public data class HmsBF(val hours: Int, val minutes: Int, val seconds: BigFloat) {

    public constructor(hours: Int, minutes: Int, seconds: String) :
            this(hours, minutes, seconds.toBF())

    /**
     * Converts from hour, minute, seconds to Measure<Degree>
     */
    public fun toDegrees(): UnitNumberBF<Angle> =
        UnitNumberBF(((seconds / 240) + (minutes / 4.0) + (hours * 15)).toBFN(), `°`)

    /**
     * Converts from hour, minute, seconds to Measure<SI_SECOND>
     */
    public fun toSeconds(): UnitNumberBF<Time> = UnitNumberBF((seconds + (hours * 3600 + minutes * 60)).toBFN(), s)

    override fun toString(): String = "${hours}h$minutes'${seconds.toDouble().format(1u)}\""

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HmsBF

        if (hours != other.hours) return false
        if (minutes != other.minutes) return false
        return seconds.approximates(other.seconds, "0.00000000001".toBFN().getValue())
    }

    override fun hashCode(): Int {
        var result = hours
        result = 31 * result + minutes
        result = 31 * result + seconds.hashCode()
        return result
    }

}

/**
 * Converts from Radian to hour, minute, seconds
 * hours = [0..23]
 * minutes = [0..59]
 * seconds = [0.0..59.9999999]
 */
internal fun UnitNumberBF<Angle>.toHMS(): HmsBF {
    var deg = (this * 180 / π).baseValue.toDouble() % 360.0
    val h = floor((deg / 15.0)).toInt()
    deg = (deg - (h * 15.0))
    deg *= 4.0
    val m = truncate(deg).toInt()
    deg -= m
    deg *= 60.0
    return HmsBF(h, m, deg.toString())
}
