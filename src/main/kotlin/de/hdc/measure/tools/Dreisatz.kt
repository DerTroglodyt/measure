package de.hdc.measure.tools

import de.hdc.measure.Combined
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.UnitNumberBF
import de.hdc.measure.numdouble.UnitNumberDouble
import de.hdc.measure.numinteger.UnitNumberLong

/**
 * Cross-multiplication (German: Dreisatz)
 * Solves the equation a0/b0 = a1/b1 for any one of the parameters being null.
 *
 * @throws IllegalArgumentException if there is more or less than 1 parameter set to null.
 */
public fun <T : Any, AN : AlgebraicNumber<T>> dreisatz(
    a0: AN?,
    b0: AN?,
    a1: AN?,
    b1: AN?
): AlgebraicNumber<T> {
    var nullParms = 0
    if (a0 == null) {
        nullParms += 1
    }
    if (b0 == null) {
        nullParms += 1
    }
    if (a1 == null) {
        nullParms += 1
    }
    if (b1 == null) {
        nullParms += 1
    }
    require(nullParms == 1) { "Found $nullParms missing parameters. Exactly one missing parameter allowed!" }

    return when {
        (a0 == null) -> a1!! / b1!! * b0!!
        (b0 == null) -> b1!! / a1!! * a0
        (a1 == null) -> a0 / b0 * b1!!
        else -> {
            // (b1 == null)
            b0 / a0 * a1
        }
    }
}

/**
 * Cross-multiplication (German: Dreisatz)
 * Solves the equation a0/b0 = a1/b1 for any one of the parameters being null.
 *
 * @throws IllegalArgumentException if there is more or less than 1 parameter set to null.
 */
public fun <AN : UnitNumberDouble<*>> dreisatz(
    a0: AN?,
    b0: AN?,
    a1: AN?,
    b1: AN?
): UnitNumberDouble<*> {
    var nullParms = 0
    if (a0 == null) {
        nullParms += 1
    }
    if (b0 == null) {
        nullParms += 1
    }
    if (a1 == null) {
        nullParms += 1
    }
    if (b1 == null) {
        nullParms += 1
    }
    require(nullParms == 1) { "Found $nullParms missing parameters. Exactly one missing parameter allowed!" }

    return when {
        (a0 == null) -> a1!! / b1!! * b0!!
        (b0 == null) -> b1!! / a1!! * a0
        (a1 == null) -> a0 / b0 * b1!!
        else -> {
            // (b1 == null)
            b0 / a0 * a1
        }
    }
}

/**
 * Cross-multiplication (German: Dreisatz)
 * Solves the equation a0/b0 = a1/b1 for any one of the parameters being null.
 *
 * @throws IllegalArgumentException if there is more or less than 1 parameter set to null.
 */
public fun <AN : UnitNumberBF<*>> dreisatz(
    a0: AN?,
    b0: AN?,
    a1: AN?,
    b1: AN?
): UnitNumberBF<*> {
    var nullParms = 0
    if (a0 == null) {
        nullParms += 1
    }
    if (b0 == null) {
        nullParms += 1
    }
    if (a1 == null) {
        nullParms += 1
    }
    if (b1 == null) {
        nullParms += 1
    }
    require(nullParms == 1) { "Found $nullParms missing parameters. Exactly one missing parameter allowed!" }

    return when {
        (a0 == null) -> a1!! / b1!! * b0!!
        (b0 == null) -> b1!! / a1!! * a0
        (a1 == null) -> a0 / b0 * b1!!
        else -> {
            // (b1 == null)
            b0 / a0 * a1
        }
    }
}

/**
 * Cross-multiplication (German: Dreisatz)
 * Solves the equation a0/b0 = a1/b1 for any one of the parameters being null.
 *
 * @throws IllegalArgumentException if there is more or less than 1 parameter set to null.
 */
public fun <AN : UnitNumberLong<*>> dreisatz(
    a0: AN?,
    b0: AN?,
    a1: AN?,
    b1: AN?
): UnitNumberLong<*> {
    var nullParms = 0
    if (a0 == null) {
        nullParms += 1
    }
    if (b0 == null) {
        nullParms += 1
    }
    if (a1 == null) {
        nullParms += 1
    }
    if (b1 == null) {
        nullParms += 1
    }
    require(nullParms == 1) { "Found $nullParms missing parameters. Exactly one missing parameter allowed!" }

    return when {
        (a0 == null) -> a1!! / b1!! * b0!!
        (b0 == null) -> b1!! / a1!! * a0
        (a1 == null) -> a0 / b0 * b1!!
        else -> {
            // (b1 == null)
            b0 / a0 * a1
        }
    }
}
