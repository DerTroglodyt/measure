package de.hdc.measure.tools

/**
 * Converts digits and + or - signs into a superscript.
 * Leave the rest of the String untouched.
 */
public fun String.toSuperScript(): String = SubSuperScript.toSuperScript(this)

/**
 * Converts digits and + or - signs into a subscript.
 * Leave the rest of the String untouched.
 */
public fun String.toSubScript(): String = SubSuperScript.toSubScript(this)

@Suppress("EnumEntryName")
/**
 * Internal enum for converting characters to their super-/subscript version.
 */
internal enum class SubSuperScript(internal val char: Char) {
    sup0('\u2070'),
    sup1('\u00B9'),
    sup2('\u00B2'),
    sup3('\u00B3'),
    sup4('\u2074'),
    sup5('\u2075'),
    sup6('\u2076'),
    sup7('\u2077'),
    sup8('\u2078'),
    sup9('\u2079'),
    supPlus('\u207A'),
    supMinus('\u207B'),
    sub0('\u2080'),
    sub1('\u2081'),
    sub2('\u2082'),
    sub3('\u2083'),
    sub4('\u2084'),
    sub5('\u2085'),
    sub6('\u2086'),
    sub7('\u2087'),
    sub8('\u2088'),
    sub9('\u2089'),
    subPlus('\u208A'),
    subMinus('\u208B');

    companion object {
        /**
         * Converts digits and + or - signs into a superscript.
         * Leave the rest of the String untouched.
         */
        fun toSuperScript(numberStr: String): String {
            val sb = StringBuilder()
            numberStr.forEach {
                sb.append(
                    when (it) {
                        '0' -> sup0.char
                        '1' -> sup1.char
                        '2' -> sup2.char
                        '3' -> sup3.char
                        '4' -> sup4.char
                        '5' -> sup5.char
                        '6' -> sup6.char
                        '7' -> sup7.char
                        '8' -> sup8.char
                        '9' -> sup9.char
                        '-' -> supMinus.char
                        '+' -> supPlus.char
                        else -> it
                    }
                )
            }
            return sb.toString()
        }

        /**
         * Converts digits and + or - signs into a subscript.
         * Leave the rest of the String untouched.
         */
        fun toSubScript(numberStr: String): String {
            val sb = StringBuilder()
            numberStr.forEach {
                sb.append(
                    when (it) {
                        '0' -> sub0.char
                        '1' -> sub1.char
                        '2' -> sub2.char
                        '3' -> sub3.char
                        '4' -> sub4.char
                        '5' -> sub5.char
                        '6' -> sub6.char
                        '7' -> sub7.char
                        '8' -> sub8.char
                        '9' -> sub9.char
                        '-' -> subMinus.char
                        '+' -> subPlus.char
                        else -> it
                    }
                )
            }
            return sb.toString()
        }
    }
}
