package de.hdc.measure.tools

//todo

///**
// * Container for times / angles
// * hours = [0..23]
// * minutes = [0..59]
// * seconds = [0.0..59.9999999]
// */
//public data class HmsDouble(val hours: Int, val minutes: Int, val seconds: Double) {
//    /**
//     * Converts from hour, minute, seconds to Measure<Degree>
//     */
//    public fun toDegrees(): UnitNumberImpl<Double> = UnitNumberImpl((seconds / 240) + (minutes / 4.0) + (hours * 15), `°`)
//
//    /**
//     * Converts from hour, minute, seconds to Measure<SI_SECOND>
//     */
//    public fun toSeconds(): UnitNumberDouble<Time> = UnitNumberImpl((seconds + (hours * 3600 + minutes * 60), s)
//
//    override fun toString(): String = "${hours}h$minutes'${seconds.format(1)}\""
//}
//
///**
// * Converts from Measure<Degree> or Measure<Radian> to hour, minute, seconds
// * hours = [0..23]
// * minutes = [0..59]
// * seconds = [0.0..59.9999999]
// */
//public fun UnitNumberImpl<Angle, DoubleNumber>.toHMS(): HmsBF {
//    var deg = (this to `°`).value.value % 360.0
//    val h = floor((deg / 15.0)).toInt()
//    deg = (deg - (h * 15.0))
//    deg *= 4.0
//    val m = deg.toInt()
//    deg -= m
//    deg *= 60.0
//    return HmsDouble(h, m, deg)
//}
