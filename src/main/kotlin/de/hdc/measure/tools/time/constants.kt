package de.hdc.measure.tools.time

internal val DAY_SECONDS: Long = 86_400L
internal val javaToJD: Double = 2_440_587.5
internal val javaToMJD: Double = javaToJD - 2_400_000.5
