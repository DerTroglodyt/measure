package de.hdc.measure.tools.time

import ch.obermuhlner.math.big.*
import de.hdc.measure.*
import de.hdc.measure.numbigfloat.*

/**
 * A time interval with a starting point.
 * @param start The point in time when it started
 * @param length How long it lasted
 */
public data class TimeEvent(
    val start: OccurrenceTime,
    val length: UnitNumber<BigFloat, BigFloatNumber, PhysicalDimension>
) : Occurrence

/**
 * A time interval with a starting point.
 * @param start The point in time when it started
 * @param length How long it lasted
 */
public data class DateTimeEvent(
    val start: OccurrenceDateTime,
    val length: UnitNumber<BigFloat, BigFloatNumber, PhysicalDimension>
) : Occurrence

/**
 * A time interval with a starting point.
 * @param start The day when it started
 * @param length Number of days [0..]
 */
public data class CalendarEvent(
    val start: OccurrenceDateTime,
    val length: ULong
) : Occurrence
