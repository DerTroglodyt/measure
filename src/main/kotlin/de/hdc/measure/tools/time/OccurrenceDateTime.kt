package de.hdc.measure.tools.time

public data class OccurrenceDateTime(
    val date: OccurrenceDate,
    val time: OccurrenceTime
) : Occurrence
