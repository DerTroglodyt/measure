package de.hdc.measure.tools.time

import de.hdc.measure.*
import de.hdc.measure.numbigfloat.*
import java.time.*

/**
 * The time of day of something happening.
 * @param dayTime UTC Time of day [0h..<24h]
 */
@JvmInline
public value class OccurrenceTime(
    private val dayTime: UnitNumberBF<Time>
) : Occurrence {

    init {
        require((dayTime >= "0".toBFN() ˍ h) && (dayTime < "24".toBFN() ˍ h))
    }

    public companion object {
        /**
         * Returns the current UTC time.
         */
        public fun now(): OccurrenceTime {
            val instant = Instant.now()
            val sec = (instant.epochSecond % DAY_SECONDS).toInt().toString() ˍ s
            val nano = instant.nano.toString() n s
            return OccurrenceTime(sec + nano)
        }
    }
}
