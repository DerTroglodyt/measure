package de.hdc.measure.tools.time

import java.time.*
import kotlin.math.*

/**
 * The calendar day at which something happened.
 * NOT equal to 0h 0min 0s on that day!
 * @param mjd Modified Julian Date without fraction. (Days since 1858-11-17 00:00:00)
 */
@JvmInline
public value class OccurrenceDate(
    private val mjd: Long
) : Occurrence {
    public companion object {
        /**
         * Converts from java time to Modified Julian Date.
         */
        internal fun secondsToMJD(javaEpochSeconds: Long): Long =
            floor((javaEpochSeconds / DAY_SECONDS) + javaToMJD).toLong()

        /**
         * Returns the current UTC date.
         */
        public fun now(): OccurrenceDate {
            return OccurrenceDate(secondsToMJD(Instant.now().epochSecond))
        }
    }
}
