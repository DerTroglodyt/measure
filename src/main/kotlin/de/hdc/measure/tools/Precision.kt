package de.hdc.measure.tools

import de.hdc.measure.*
import de.hdc.measure.core.*

/**
 * Defines the precision of a number.
 * @param plus How much bigger the number may be.
 * @param minus How much smaller the number may be.
 */
public data class Precision<D : PhysicalDimension, AN : Any, V : AlgebraicNumber<AN>>(
    val plus: UnitNumber<AN, V, D>,
    val minus: UnitNumber<AN, V, D> = plus
)

/** Find the smallest of the given intervals */
public fun <D : PhysicalDimension, AN : Any, V : AlgebraicNumber<AN>>
        min(vararg number: Precision<D, AN, V>): Precision<D, AN, V> = number.minBy { it.plus + it.minus }

/** Find the biggest of the given intervals */
public fun <D : PhysicalDimension, AN : Any, V : AlgebraicNumber<AN>>
        max(vararg number: Precision<D, AN, V>): Precision<D, AN, V> = number.maxBy { it.plus + it.minus }
