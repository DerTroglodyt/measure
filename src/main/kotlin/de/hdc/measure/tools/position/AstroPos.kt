package de.hdc.measure.tools.position

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*

/**
 * 3D-Position for astronomical calculations.
 * @param distance The distance from center
 * @param rightAscension Right ascension angle
 * @param declination Declination angle
 */
public data class AstroPos(
    val distance: UnitNumberBF<Length>,
    val rightAscension: UnitNumberBF<Time>,
    val declination: UnitNumberBF<Angle>
) : Comparable<UnitPosition>, UnitPosition {

    public constructor(
        distance: UnitNumberBF<Length>,
        hour: Int,
        minute: Int,
        seconds: Double,
        declination: UnitNumberBF<Angle>
    ) : this(distance, (hour * 3600 + minute * 60 + seconds).toString() ˍ s, declination)

    override val length: UnitNumberBF<Length>
        get() = distance

    override fun compareTo(other: UnitPosition): Int {
        if (other !is AstroPos) {
            return -1
        }
        if (rightAscension.approximates(other.rightAscension) &&
            declination.approximates(other.declination)
        ) {
            return 0
        }
        return if (rightAscension < (other.rightAscension)) {
            -1
        } else {
            +1
        }
    }

    override fun toString(): String =
        "(Dec: ${(declination toUnit `°`).format(1u, 0u)}, RA: ${
            (rightAscension toUnit h).format(1u, 0u)
        }, d: $distance)"

    override fun equals(other: Any?): Boolean {
        if (other !is AstroPos) {
            return false
        }

        return compareTo(other) == 0
    }

    override fun hashCode(): Int {
        var result = rightAscension.hashCode()
        result = 31 * result + declination.hashCode()
        return result
    }

    override operator fun plus(v: UnitPosition): AstroPos = (toCartesian() + v.toCartesian()).toAstro()

    override operator fun minus(v: UnitPosition): AstroPos = (toCartesian() - v.toCartesian()).toAstro()

    override fun convertTo(measureUnit: ScaledUnit<Length>): UnitPosition = this

    override fun times(scalar: Number): UnitPosition {
        return AstroPos(distance * scalar.toDouble(), rightAscension, declination)
    }

    override fun div(scalar: Number): UnitPosition {
        return AstroPos(distance / scalar.toDouble(), rightAscension, declination)
    }

    override fun normalize(): UnitPosition {
        return AstroPos("1".toBFN() ˍ m, rightAscension, declination)
    }

    @Suppress("DEPRECATION")
    override fun toCartesian(): Cartesian {
        val decRad = declination.baseValue
        val raRad = (Const.PI.baseValue / 12) * rightAscension.toUnit(h).printValue
        return Cartesian(
            distance * decRad.cos() * raRad.cos(),
            distance * decRad.cos() * raRad.sin(),
            distance * decRad.sin(),
            Cartesian.Companion.CoordinateSystem.CARTESIAN_RIGHT_HANDED
        )
    }

}
