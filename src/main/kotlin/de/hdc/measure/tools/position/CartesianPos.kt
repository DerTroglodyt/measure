package de.hdc.measure.tools.position

import ch.obermuhlner.math.big.*
import ch.obermuhlner.math.big.kotlin.bigfloat.*
import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*

/**
 * Cartesian coordinates as a 3-dimensional vector.
 */
public data class Cartesian(
    val x: UnitNumberBF<Length>,
    val y: UnitNumberBF<Length>,
    val z: UnitNumberBF<Length>,
    val coordinateSystem: CoordinateSystem = CoordinateSystem.CARTESIAN_RIGHT_HANDED
) : Comparable<UnitPosition>, UnitPosition {
    override val length: UnitNumberBF<Length> by lazy {
        UnitNumberBF(((x.baseValue * x.baseValue) + (y.baseValue * y.baseValue) + (z.baseValue * z.baseValue)).sqrt(), m)
    }

    public constructor(
        x: String,
        y: String,
        z: String,
        unit: ScaledUnit<Length> = m,
        coordinateSystem: CoordinateSystem = CoordinateSystem.CARTESIAN_RIGHT_HANDED
    ) : this(UnitNumberBF(x.toBFN(), unit), UnitNumberBF(y.toBFN(), unit), UnitNumberBF(z.toBFN(), unit), coordinateSystem)

    public constructor(
        x: BigFloat,
        y: BigFloat,
        z: BigFloat,
        unit: ScaledUnit<Length> = m,
        coordinateSystem: CoordinateSystem = CoordinateSystem.CARTESIAN_RIGHT_HANDED
    ) : this(UnitNumberBF(x.toBFN(), unit), UnitNumberBF(y.toBFN(), unit), UnitNumberBF(z.toBFN(), unit), coordinateSystem)

    public companion object {
        public val ZERO: Cartesian = Cartesian("0.0", "0.0", "0.0")

        public enum class CoordinateSystem {
            CARTESIAN_RIGHT_HANDED,
            CARTESIAN_LEFT_HANDED
        }

        /**
         * Directions in right-handed Cartesian coordinates.
         * North -> y
         * East -> x
         * Up -> z
         */
        public enum class StandardDirections(
            public val shortName: String,
            x: String,
            y: String,
            z: String
        ) { NORTH("N ", "0.0", "1.0", "0.0"),
            NORTH_EAST("NE", "1.0", "1.0", "0.0"),
            EAST("E ", "1.0", "0.0", "0.0"),
            SOUTH_EAST("SE", "1.0", "-1.0", "0.0"),
            SOUTH("S ", "0.0", "-1.0", "0.0"),
            SOUTH_WEST("SW", "-1.0", "-1.0", "0.0"),
            WEST("W ", "-1.0", "0.0", "0.0"),
            NORTH_WEST("NW", "-1.0", "1.0", "0.0"),
            UP("U ", "0.0", "0.0", "1.0"),
            DOWN("D ", "0.0", "0.0", "-1.0");


            public val dir: Cartesian = Cartesian(x, y, z, m).normalize()
        }
    }

    override fun compareTo(other: UnitPosition): Int {
        val o = if (other is SphericalPos) {
            other.toCartesian()
        } else {
            other
        }
        if (length == o.length) {
            return 0
        }
        return if (length < (o.length)) {
            -1
        } else {
            +1
        }
    }

    override fun toString(): String {
        return when (coordinateSystem) {
            CoordinateSystem.CARTESIAN_RIGHT_HANDED -> {
                "(${x.format(1u, 0u)}, ${y.format(1u, 0u)}, ${z.format(1u, 0u)})"
            }

            CoordinateSystem.CARTESIAN_LEFT_HANDED -> {
                "(${x.format(1u, 0u)}, ${y.format(1u, 0u)}, ${z.format(1u, 0u)} $coordinateSystem)"
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other !is UnitPosition) {
            return false
        }
        val o: Cartesian = if (other is SphericalPos) {
            other.toCartesian()
        } else {
            other as Cartesian
        }

        return (x == o.x) && (y == o.y) && (z == o.z) && (coordinateSystem == o.coordinateSystem)
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        result = 31 * result + z.hashCode()
        result = 31 * result + coordinateSystem.hashCode()
        return result
    }

    override operator fun plus(v: UnitPosition): Cartesian {
        val vv = v.toCartesian().convertTo(coordinateSystem)
        return Cartesian(
            x + vv.x,
            y + vv.y,
            z + vv.z,
            coordinateSystem
        )
    }

    override operator fun minus(v: UnitPosition): Cartesian {
        val vv = v.toCartesian().convertTo(coordinateSystem)
        return Cartesian(
            x - vv.x,
            y - vv.y,
            z - vv.z,
            coordinateSystem
        )
    }

    override fun times(scalar: Number): UnitPosition {
        return Cartesian(
            x * scalar.toDouble(),
            y * scalar.toDouble(),
            z * scalar.toDouble(),
            coordinateSystem
        )
    }

    public operator fun times(scalar: UnitNumberBF<Dimensionless>): Cartesian {
        return Cartesian(
            x * scalar.baseValue,
            y * scalar.baseValue,
            z * scalar.baseValue,
            coordinateSystem
        )
    }

    override fun div(scalar: Number): UnitPosition {
        return Cartesian(
            x / scalar.toDouble(),
            y / scalar.toDouble(),
            z / scalar.toDouble(),
            coordinateSystem
        )
    }

    public operator fun div(scalar: UnitNumberBF<Dimensionless>): Cartesian {
        return Cartesian(
            x / scalar.baseValue,
            y / scalar.baseValue,
            z / scalar.baseValue,
            coordinateSystem
        )
    }

    override fun normalize(): Cartesian {
        return Cartesian(
            UnitNumberBF(x.baseValue / length.baseValue, m),
            UnitNumberBF(y.baseValue / length.baseValue, m),
            UnitNumberBF(z.baseValue / length.baseValue, m),
            coordinateSystem
        )
    }

    public fun cross(v: Cartesian): Cartesian {
        val a = this.convertTo(CoordinateSystem.CARTESIAN_RIGHT_HANDED)
        val b = v.convertTo(CoordinateSystem.CARTESIAN_RIGHT_HANDED)
        return Cartesian(
            UnitNumberBF((a.y.baseValue * b.z.baseValue) - (a.z.baseValue * b.y.baseValue), m),
            UnitNumberBF((a.z.baseValue * b.x.baseValue) - (a.x.baseValue * b.z.baseValue), m),
            UnitNumberBF((a.x.baseValue * b.y.baseValue) - (a.y.baseValue * b.x.baseValue), m),
            CoordinateSystem.CARTESIAN_RIGHT_HANDED
        )
    }

    public fun scalar(other: Cartesian): BigFloat {
        val vv = other.convertTo(coordinateSystem)
        return (x.baseValue * vv.x.baseValue + y.baseValue * vv.y.baseValue + z.baseValue * vv.z.baseValue).getValue()
    }

    override infix fun convertTo(measureUnit: ScaledUnit<Length>): Cartesian {
        return Cartesian(
            x toUnit measureUnit,
            y toUnit measureUnit,
            z toUnit measureUnit,
            coordinateSystem
        )
    }

    public infix fun convertTo(newCoordinateSystem: CoordinateSystem): Cartesian {
        if (coordinateSystem == newCoordinateSystem) {
            return this
        }
        val old: Cartesian = when (coordinateSystem) {
            CoordinateSystem.CARTESIAN_RIGHT_HANDED -> this
            CoordinateSystem.CARTESIAN_LEFT_HANDED -> this.copy(
                z = z.inverse(),
                coordinateSystem = CoordinateSystem.CARTESIAN_RIGHT_HANDED
            )
        }
        return when (newCoordinateSystem) {
            CoordinateSystem.CARTESIAN_RIGHT_HANDED -> old
            CoordinateSystem.CARTESIAN_LEFT_HANDED -> old.copy(
                z = z.inverse(),
                coordinateSystem = CoordinateSystem.CARTESIAN_LEFT_HANDED
            )
        }
    }

    override fun toCartesian(): Cartesian = this

    override fun toSpherical(): SphericalPos {
        val o = convertTo(CoordinateSystem.CARTESIAN_RIGHT_HANDED)
        return SphericalPos(
            o.length,
            UnitNumberBF(BigFloatNumber.atan2(o.y.baseValue.getValue(), o.x.baseValue.getValue()).toBFN(), rad),
            UnitNumberBF(BigFloat.acos(o.z.baseValue.getValue() / o.length.baseValue.getValue()).toBFN(), rad)
        )
    }

    override fun toGeo(): GeoPos {
        val o = convertTo(CoordinateSystem.CARTESIAN_RIGHT_HANDED)
        return GeoPos(
            length,
            UnitNumberBF(
                (Const.PI.baseValue.value - BigFloatNumber.atan2(o.y.baseValue.value, o.x.baseValue.value)).toBFN(),
                rad
            ),
            UnitNumberBF(BigFloat.acos(o.z.baseValue.getValue() / o.length.baseValue.getValue()).toBFN(), rad)
        )
    }

    override fun toAstro(): AstroPos {
        val o = convertTo(CoordinateSystem.CARTESIAN_RIGHT_HANDED)
        var ra = BigFloatNumber.atan2(o.y.baseValue.value, o.x.baseValue.value / Const.PI.baseValue.value) * 12
        if (ra.isNegative) {
            ra += 24
        }
        return AstroPos(
            length,
            UnitNumberBF(ra.toBFN(), h),
            UnitNumberBF(BigFloat.asin(o.z.baseValue.getValue() / o.length.baseValue.getValue()).toBFN(), rad)
        )
    }

}
