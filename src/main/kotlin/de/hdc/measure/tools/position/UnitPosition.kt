package de.hdc.measure.tools.position

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*

/**
 * A 3D-Position.
 */
public interface UnitPosition {
    /**
     * Length of the corresponding 3D-vector.
     */
    public val length: UnitNumberBF<Length>

    /**
     * Converts this into another representation of the same Position.
     */
    public infix fun convertTo(measureUnit: ScaledUnit<Length>): UnitPosition

    /** Convert to cartesian coordinates. */
    public fun toCartesian(): Cartesian

    /** Convert to spherical coordinates. */
    public fun toSpherical(): SphericalPos = toCartesian().toSpherical()

    /** Convert to geological coordinates. */
    public fun toGeo(): GeoPos = toCartesian().toGeo()

    /** Convert to astronomical coordinates. */
    public fun toAstro(): AstroPos = toCartesian().toAstro()

    /** Add two positions */
    public operator fun plus(v: UnitPosition): UnitPosition

    /** Subtract two positions */
    public operator fun minus(v: UnitPosition): UnitPosition

    /** Scale the 3D-Vector of this position. */
    public operator fun times(scalar: Number): UnitPosition

    /** Scale the 3D-Vector of this position. */
    public operator fun div(scalar: Number): UnitPosition

    /** Returns this positions 3D-Vector scaled to a length of 1. */
    public fun normalize(): UnitPosition
}
