package de.hdc.measure.tools.position

import ch.obermuhlner.math.big.*
import ch.obermuhlner.math.big.kotlin.bigfloat.*
import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*

/**
 * 3D-Positions in geological coordinates.
 * @param radius Radius of the sphere
 * @param longitude Longitude of position
 * @param latitude Latitude of position
 */
public data class GeoPos(
    val radius: UnitNumberBF<Length>,
    val longitude: UnitNumberBF<Angle>,
    val latitude: UnitNumberBF<Angle>
) : Comparable<UnitPosition>, UnitPosition {
    override val length: UnitNumberBF<Length>
        get() = radius

    override fun compareTo(other: UnitPosition): Int {
        if (other !is GeoPos) {
            return -1
        }
        if (longitude.approximates(other.longitude, BigFloatNumber.maxDifference.toBFN()) &&
            latitude.approximates(other.latitude, BigFloatNumber.maxDifference.toBFN())
        ) {
            return 0
        }
        return if (longitude < (other.longitude)) {
            -1
        } else {
            +1
        }
    }

    override fun toString(): String =
        "(Lat: ${(latitude toUnit `°`).format(1u, 0u)}, Long: ${(longitude toUnit `°`).format(1u, 0u)})"

    override fun equals(other: Any?): Boolean {
        if (other !is GeoPos) {
            return false
        }

        return compareTo(other) == 0
    }

    override fun hashCode(): Int {
        var result = longitude.hashCode()
        result = 31 * result + latitude.hashCode()
        return result
    }

    override operator fun plus(v: UnitPosition): GeoPos = (toCartesian() + v.toCartesian()).toGeo()

    override operator fun minus(v: UnitPosition): GeoPos = (toCartesian() - v.toCartesian()).toGeo()

    override fun times(scalar: Number): UnitPosition {
        return GeoPos(radius * scalar.toDouble(), longitude, latitude)
    }

    override fun div(scalar: Number): UnitPosition {
        return GeoPos(radius / scalar.toDouble(), longitude, latitude)
    }

    override fun normalize(): UnitPosition {
        return GeoPos("1.0".toBFN() ˍ m, longitude, latitude)
    }

    override fun convertTo(measureUnit: ScaledUnit<Length>): UnitPosition = this

    override fun toCartesian(): Cartesian {
        val dec = (Const.PI / 2.0) - (latitude toUnit rad)
        val lng = longitude toUnit rad
        return Cartesian(
            radius.baseValue.value * BigFloat.sin(dec.baseValue.value) * BigFloat.cos(lng.baseValue.value),
            radius.baseValue.value * BigFloat.sin(dec.baseValue.value) * BigFloat.sin(lng.baseValue.value),
            radius.baseValue.value * BigFloat.cos(dec.baseValue.value),
            radius.unit,
            Cartesian.Companion.CoordinateSystem.CARTESIAN_RIGHT_HANDED
        )
    }

}
