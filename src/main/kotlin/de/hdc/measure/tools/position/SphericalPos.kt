package de.hdc.measure.tools.position

import ch.obermuhlner.math.big.*
import ch.obermuhlner.math.big.kotlin.bigfloat.*
import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*

/**
 * 3D-Position in spherical coordinates.
 * @param r Distance from center of sphere.
 * @param azimuth Azimuth angle.
 * @param inclination Inclination angle.
 */
public data class SphericalPos(
    val r: UnitNumberBF<Length>,
    val azimuth: UnitNumberBF<Angle>,
    val inclination: UnitNumberBF<Angle>
) : Comparable<UnitPosition>, UnitPosition {
    override val length: UnitNumberBF<Length>
        get() = r

    override fun compareTo(other: UnitPosition): Int {
        if (other !is SphericalPos) {
            return -1
        }
        if (r.approximates((other.r toUnit r.unit), BigFloatNumber.maxDifference.toBFN()) &&
            azimuth.approximates((other.azimuth toUnit azimuth.unit), BigFloatNumber.maxDifference.toBFN()) &&
            inclination.approximates((other.inclination toUnit inclination.unit), BigFloatNumber.maxDifference.toBFN())
        ) {
            return 0
        }
        return if (length < (other.length)) {
            -1
        } else {
            +1
        }
    }

    override fun toString(): String =
        "(Inc: ${(inclination toUnit `°`).format(1u, 0u)}, Azm: ${
            (azimuth toUnit `°`).format(1u, 0u)
        }, ${r.format(1u, 0u)})"

    override fun equals(other: Any?): Boolean {
        if (other !is SphericalPos) {
            return false
        }
        return compareTo(other) == 0
    }

    override fun hashCode(): Int {
        var result = r.hashCode()
        result = 31 * result + azimuth.hashCode()
        result = 31 * result + inclination.hashCode()
        return result
    }

    override operator fun plus(v: UnitPosition): SphericalPos = (toCartesian() + v.toCartesian()).toSpherical()

    override operator fun minus(v: UnitPosition): SphericalPos = (toCartesian() - v.toCartesian()).toSpherical()

    override operator fun times(scalar: Number): SphericalPos = copy(r = (r * scalar.toDouble()))

    override operator fun div(scalar: Number): SphericalPos = copy(r = (r / scalar.toDouble()))

    override fun normalize(): SphericalPos = copy(r = ("1.0".toBFN() ˍ m))

    public override fun convertTo(measureUnit: ScaledUnit<Length>): UnitPosition {
        return SphericalPos(
            (r toUnit measureUnit),
            this.inclination,
            this.azimuth
        )
    }

    override fun toCartesian(): Cartesian {
        val inc = (inclination toUnit rad).baseValue.value
        val azm = (azimuth toUnit rad).baseValue.value
        return Cartesian(
            r.baseValue.value * BigFloat.sin(inc) * BigFloat.cos(azm),
            r.baseValue.value * BigFloat.sin(inc) * BigFloat.sin(azm),
            r.baseValue.value * BigFloat.cos(inc),
            r.unit,
            Cartesian.Companion.CoordinateSystem.CARTESIAN_RIGHT_HANDED
        )
    }

}
