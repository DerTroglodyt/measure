@file:Suppress("ClassName")

package de.hdc.measure

import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.numbigfloat.BigFloatNumber.Companion.ONE

/**
 * Inch
 */
public object `in` : ScaledUnit<Length>(
    "inch", "in", m, "0.0254".toBFN()
)

/**
 * Thou
 */
public object th : ScaledUnit<Length>(
    "thou", "th", m, "0.0000254".toBFN()
)

/**
 * Feet
 */
public object ft : ScaledUnit<Length>(
    "feet", "ft", m, "0.3048".toBFN()
)

/**
 * Yard
 */
public object yd : ScaledUnit<Length>(
    "yard", "yd", m, "0.9144".toBFN()
)

/**
 * Mile
 */
public object mi : ScaledUnit<Length>(
    "mile", "mi", m, "1609.344".toBFN()
)

/**
 * Nautical Mile
 */
public object nmi : ScaledUnit<Length>(
    "nautical mile", "nmi", m, "1852".toBFN()
)

/**
 * Square Mile
 */
public object sq_mi : ScaledUnit<Area>(
    "square miles", "sq mi", m2, "2589988.110336".toBFN()
)

/**
 * Fluid Ounce
 */
public object fl_oz : ScaledUnit<Volume>(
    "fluid ounce", "fl oz", m3, "0.0000284130625".toBFN()
)

/**
 * Pint
 */
public object pt : ScaledUnit<Volume>(
    "pint", "pt", m3, "0.00056826125".toBFN()
)

/**
 * Gallon
 */
public object gal : ScaledUnit<Volume>(
    "gallons", "gal", m3, "0.00454609".toBFN()
)

/**
 * Grain
 */
public object gr : ScaledUnit<Mass>(
    "grain", "gr", g, "0.06479891".toBFN()
)

/**
 * Ounce
 */
public object oz : ScaledUnit<Mass>(
    "ounce", "oz", g, "28.349523125".toBFN()
)

/**
 * Pound
 */
public object lb : ScaledUnit<Mass>(
    "pound", "lb", g, "453.59237".toBFN()
)

/**
 * Stone
 */
public object st : ScaledUnit<Mass>(
    "stone", "st", g, "6350.29318".toBFN()
)

/**
 * Fahrenheit
 */
@Suppress("NonAsciiCharacters")
public object `°F` : ScaledUnit<Temperature>(
    name = "Fahrenheit",
    symbol = "°F",
    unit = K,
    Converter(
        toBaseUnit = { (it + "459.67".toBFN()) * ("5.0".toBFN() / "9.0".toBFN()) },
        fromBaseUnit = { (it / ("5.0".toBFN() / "9.0".toBFN())) - "459.67".toBFN() },
        multiplier = (ONE + "459.67".toBFN()) * ("5.0".toBFN() / "9.0".toBFN())
    )
)
