@file:Suppress("KDocMissingDocumentation", "ClassName", "NonAsciiCharacters")

package de.hdc.measure

import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*

// SI Units
public object UNITLESS : ScaledUnit<Dimensionless>("", "", Quantity.from(), Dimensionless)
public object mol : ScaledUnit<SubstanceAmount>("Mole", "mol", Quantity.from(N = 1), SubstanceAmount)
public object A : ScaledUnit<ElectricCurrent>("Ampere", "A", Quantity.from(I = 1), ElectricCurrent)
public object m : ScaledUnit<Length>("Meter", "m", Quantity.from(L = 1), Length)
public object Cd : ScaledUnit<LuminousIntensity>("Candela", "Cd", Quantity.from(J = 1), LuminousIntensity)

public object g : ScaledUnit<Mass>("Gram", "g", Quantity.from(M = 1), Mass)
public object K : ScaledUnit<Temperature>("Kelvin", "K", Quantity.from(Θ = 1), Temperature)
public object s : ScaledUnit<Time>("Second", "s", Quantity.from(T = 1), Time)

// Non SI Units
public object pcs : ScaledUnit<Pieces>("Pieces", "pcs", Quantity.from(), Pieces)

// Derived base units
public object m2 : ScaledUnit<Area>("Square Meter", "m²", Quantity.from(L = 2), Area)
public object m3 : ScaledUnit<Volume>("Cubic Meter", "m³", Quantity.from(L = 3), Volume)
public object rad : ScaledUnit<Angle>("Radian", "rad", Quantity.from(), Angle)
public object sr : ScaledUnit<SolidAngle>("Steradian", "sr", Quantity.from(), SolidAngle)
public object Hz : ScaledUnit<Frequency>("Hertz", "Hz", Quantity.from(T = -1), Frequency)
public object N : ScaledUnit<Force>("Newton", "N", Quantity.from(M = 1, L = 1, T = -2), Force, "1e3".toBFN())
public object Pa : ScaledUnit<Pressure>("Pascal", "Pa", Quantity.from(M = 1, L = -1, T = -2), Pressure, "1e3".toBFN())
public object J : ScaledUnit<Energy>("Joule", "J", Quantity.from(M = 1, L = 2, T = -2), Energy, "1e3".toBFN())
public object Js : ScaledUnit<Action>("JouleSecond", "Js", Quantity.from(M = 1, L = 2, T = -1), Action, "1e3".toBFN())
public object W : ScaledUnit<Power>("Watt", "W", Quantity.from(M = 1, L = 2, T = -3), Power, "1e3".toBFN())
public object C : ScaledUnit<Charge>("Coulomb", "C", Quantity.from(I = 1, T = 1), Charge)
public object m_s : ScaledUnit<Velocity>("Velocity", "[m s⁻¹]", Quantity.from(L = 1, T = -1), Velocity)
public object m_s2 : ScaledUnit<Acceleration>("Acceleration", "[m s⁻²]", Quantity.from(L = 1, T = -2), Acceleration)
public object ω : ScaledUnit<AngularVelocity>("Angular Velocity", "ω", Quantity.from(T = -1), AngularVelocity)
public object α :
    ScaledUnit<AngularAcceleration>("Angular Acceleration", "ω", Quantity.from(T = -2), AngularAcceleration)

public object kg_m3 :
    ScaledUnit<Density>("Density", "[kg m⁻³]", Quantity.from(M = 1, L = -3), Density, "1e3".toBFN())

public object kg_mol :
    ScaledUnit<MolarMass>("Molar Mass", "[kg mol⁻¹]", Quantity.from(M = 1, N = -1), MolarMass, "1e3".toBFN())

public object mol_m3 : ScaledUnit<Molarity>("Molarity", "[mol m⁻³]", Quantity.from(N = 1, L = -3), Molarity)


public object UnitTable {
    private val map: MutableMap<Quantity, MutableMap<PhysicalDimension, ScaledUnit<*>>> = mutableMapOf()

    init {
        listOf(
            UNITLESS, mol, A, m, Cd, g, K, s, pcs
        ).forEach { add(it) }
        listOf(
            m2, m3, rad
            , sr
            , Hz, N, Pa, J, Js, W, C, m_s, m_s2, ω
            , α, kg_m3, kg_mol, mol_m3
        ).forEach { add(it) }
    }

    /**
     * Returns the base unit for the given Quantity.
     */
    public operator fun get(quantity: Quantity, dimension: PhysicalDimension): ScaledUnit<*>? {
        return if (dimension == Combined) map[quantity]?.values?.first()
        else map[quantity]?.get(dimension)
    }

    public fun add(unit: ScaledUnit<*>) {
        val q = unit.quantity
        val d = unit.dimension
        require(get(q, d) == null) { "Can't insert '$unit'. Unit with Quantity '$q' already present: '${get(q, d)}'!" }
        if (map[q] == null) {
            map[q] = mutableMapOf(d to unit)
        } else {
            map[q]!![d] = unit
        }
    }
}
