package de.hdc.measure.numbigfloat

import ch.obermuhlner.math.big.BigFloat
import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numdouble.UnitNumberDouble
import kotlin.math.abs

/**
 * An immutable value and an associated unit.
 * Implementations vary in used backing fields for values (Long / Double / BigDecimal)
 *
 * @param value The internal value in SI base unit
 * @param unit The external BaseUnit / ScaledUnit
 * @param convert Convert to SI units. Set true for first time construction. False for calculations afterwards.
 */
public class UnitNumberBF<D : PhysicalDimension>(
    value: BigFloatNumber,
    override val unit: ScaledUnit<D>,
    convert: Boolean = true
) : UnitNumber<BigFloat, BigFloatNumber, D>,
    AlgebraicNumber<BigFloat>,// by value,
    Comparable<AlgebraicNumber<BigFloat>> {

    override val baseValue: BigFloatNumber = if (convert) {
        value.toSI(unit.prefix, unit.converter)
    } else {
        value
    }

    override infix fun <ND : PhysicalDimension> toUnit(newUnit: ScaledUnit<ND>): UnitNumberBF<ND> {
        require(this.unit.quantity == newUnit.quantity)
        return UnitNumberBF(baseValue, newUnit, false)
    }

    override fun toString(): String = format(1u, 0u)

    override fun compareTo(other: AlgebraicNumber<BigFloat>): Int {
        return when {
            other !is UnitNumber<*, *, *> -> -1
            unit.quantity != other.unit.quantity -> -1
            else -> {
                @Suppress("SafeCastWithReturn")
                other.baseValue as? BigFloatNumber ?: return -1
                baseValue.compareTo(other.baseValue as BigFloatNumber)
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnitNumber<*, *, *>

        return (unit.quantity == other.unit.quantity) &&
                ((unit.dimension == other.unit.dimension) ||
                        (unit.dimension == Combined) ||
                        (other.unit.dimension == Combined)) &&
                (baseValue == other.baseValue)
    }

    override fun hashCode(): Int {
        var result = unit.hashCode()
        result = 31 * result + baseValue.hashCode()
        return result
    }

    public override fun simplify(): UnitNumber<*, *, *> {
        val u = unit.simplify()
        return if (u == null) this
        else UnitNumberBF(baseValue, u.first, true)
    }

    // Implement AlgebraicNumber ##########################################
    /**
     * Returns the internal value in SI base unit which may differ from then display unit.
     */
    @Deprecated(
        "Most Likely not the value you are looking for! " +
                "Returns the internal value in SI base unit which may differ from the display unit.",
        ReplaceWith("value.getPrintValue()")
    )
    override fun getValue(): BigFloat = baseValue.getValue()

    override fun isZero(): Boolean = baseValue.isZero()
    override fun isNegative(): Boolean = baseValue.isNegative()
    override fun isPositive(): Boolean = baseValue.isPositive()
    override fun isNaN(): Boolean = baseValue.isNaN()
    override fun isInfinite(): Boolean = baseValue.isInfinite()

    public fun approximates(other: UnitNumberBF<D>, maxDiff: BigFloat = BigFloatNumber.maxDifference): Boolean =
        baseValue.value.approximates(other.baseValue.value, maxDiff)

    override fun approximates(other: AlgebraicNumber<BigFloat>, maxDiff: AlgebraicNumber<BigFloat>): Boolean =
        baseValue.approximates(other, maxDiff)

    override fun reciprocal(): UnitNumberBF<*> {
        val u = UNITLESS / unit
        return UnitNumberBF(u.second.toBaseUnit(baseValue.reciprocal()), u.first)
    }

    override fun inverse(): UnitNumberBF<D> = UnitNumberBF(baseValue.inverse(), unit)

    override fun fromSI(prefix: Prefix, converter: Converter): BigFloatNumber =
        baseValue.fromSI(prefix, converter)

    override fun toSI(prefix: Prefix, converter: Converter): BigFloatNumber =
        baseValue.toSI(prefix, converter)

    override operator fun times(other: Double): UnitNumberBF<D> =
        UnitNumberBF(baseValue * other, unit, false)

    override operator fun times(other: Float): UnitNumberBF<D> = this * other.toDouble()
    override operator fun times(other: Int): UnitNumberBF<D> = this * other.toDouble()
    override operator fun times(other: Long): UnitNumberBF<D> = this * other.toDouble()

    public fun times(other: UnitNumber<BigFloat, BigFloatNumber, D>): UnitNumberBF<*> {
        val u = unit * other.unit
        return UnitNumberBF(u.second.toBaseUnit(baseValue * other.baseValue), u.first, false)
    }

    override operator fun div(other: Double): UnitNumberBF<D> =
        UnitNumberBF((baseValue / other), unit, false)

    /**
     * Scales this value by a factor.
     */
    override operator fun div(other: Float): UnitNumberBF<D> =
        UnitNumberBF((baseValue / other), unit, false)

    /**
     * Scales this value by a factor.
     */
    override operator fun div(other: Int): UnitNumberBF<D> =
        UnitNumberBF((baseValue / other), unit, false)

    /**
     * Scales this value by a factor.
     */
    override operator fun div(other: Long): UnitNumberBF<D> =
        UnitNumberBF(baseValue / other, unit, false)

    public fun div(other: UnitNumber<BigFloat, BigFloatNumber, D>): UnitNumberBF<*> {
        return UnitNumberBF(baseValue / other.baseValue, unit, false)
    }

    /**
     * Adds two values in the same PhysicalDimension.
     */
    override operator fun plus(other: AlgebraicNumber<BigFloat>): UnitNumberBF<D> {
        require(other is UnitNumberBF<*>) {
            "Can not add '${this.javaClass.name}' to '${other.javaClass.name}'!"
        }

        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberBF<D> ?: error("Can not add '${this.unit}' to '${other.unit}'!")
        if (unit.quantity != other.unit.quantity)
            error("Can not add '${unit.quantity}' to '${other.unit.quantity}'!")
        return UnitNumberBF((baseValue + other.baseValue), unit, false)
    }

    /**
     * Subtracts two values in the same PhysicalDimension.
     */
    override operator fun minus(other: AlgebraicNumber<BigFloat>): UnitNumberBF<D> {
        require(other is UnitNumberBF<*>) {
            "Can not subtract '${other.javaClass.name}' from '${this.javaClass.name}'!"
        }

        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberBF<D> ?: error("Can not subtract '${other.unit}' from '${this.unit}'!")
        if (unit.quantity != other.unit.quantity)
            error("Can not subtract '${other.unit.quantity}' from '${this.unit.quantity}'!")
        return UnitNumberBF((baseValue - other.baseValue), unit, false)
    }

    public operator fun times(other: UnitNumberBF<*>): UnitNumberBF<*> {
        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberBF<D>
            ?: error("Can not multiply '${this.unit}' with '${other.unit}'!")
        val u = unit * other.unit
        return UnitNumberBF(u.second.toBaseUnit(baseValue * other.baseValue), u.first, false)
    }

    override operator fun times(other: AlgebraicNumber<BigFloat>): UnitNumberBF<D> {
        return UnitNumberBF((baseValue * other), unit, false)
    }

    public operator fun div(other: UnitNumberBF<*>): UnitNumberBF<*> {
        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberBF<D>
            ?: error("Can not divide '${this.unit}' by '${other.unit}'!")
        val u = unit / other.unit
        return UnitNumberBF(u.second.toBaseUnit(baseValue / other.baseValue), u.first, false)
    }

    override operator fun div(other: AlgebraicNumber<BigFloat>): UnitNumberBF<D> {
        return UnitNumberBF(baseValue / other, unit, false)
    }
}

/** Scales a UnitNumberBF by a factor */
internal operator fun <D : PhysicalDimension> Double.times(other: UnitNumberBF<D>): UnitNumberBF<D> =
    other * this

/** Scales a UnitNumberBF by a factor */
internal operator fun <D : PhysicalDimension> Float.times(other: UnitNumberBF<D>): UnitNumberBF<D> =
    other * this

/** Scales a UnitNumberBF by a factor */
internal operator fun <D : PhysicalDimension> Int.times(other: UnitNumberBF<D>): UnitNumberBF<D> =
    other * this

/** Scales a UnitNumberBF by a factor */
internal operator fun <D : PhysicalDimension> Long.times(other: UnitNumberBF<D>): UnitNumberBF<D> =
    other * this
