@file:Suppress(
    "NonAsciiCharacters", "FunctionName",
    "KDocMissingDocumentation"
)

package de.hdc.measure.numbigfloat

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.core.ScaledUnit

public infix fun <D : PhysicalDimension> BigFloatNumber.Y(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.Y..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.Z(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.Z..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.X(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.X..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.P(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.P..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.T(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.T..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.G(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.G..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.M(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.M..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.k(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.k..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.h(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.h..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.da(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.da..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.ˍ(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.NONE..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.d(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.d..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.c(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.c..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.m(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.m..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.µ(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.µ..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.n(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.n..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.p(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.p..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.f(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.f..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.a(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.a..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.z(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.z..measureUnit)

public infix fun <D : PhysicalDimension> BigFloatNumber.y(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this, DecimalPrefix.y..measureUnit)

//###########################################################################
public infix fun <D : PhysicalDimension> String.Y(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.Y..measureUnit)

public infix fun <D : PhysicalDimension> String.Z(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.Z..measureUnit)

public infix fun <D : PhysicalDimension> String.X(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.X..measureUnit)

public infix fun <D : PhysicalDimension> String.P(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.P..measureUnit)

public infix fun <D : PhysicalDimension> String.T(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.T..measureUnit)

public infix fun <D : PhysicalDimension> String.G(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.G..measureUnit)

public infix fun <D : PhysicalDimension> String.M(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.M..measureUnit)

public infix fun <D : PhysicalDimension> String.k(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.k..measureUnit)

public infix fun <D : PhysicalDimension> String.h(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.h..measureUnit)

public infix fun <D : PhysicalDimension> String.da(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.da..measureUnit)

public infix fun <D : PhysicalDimension> String.ˍ(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.NONE..measureUnit)

public infix fun <D : PhysicalDimension> String.d(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.d..measureUnit)

public infix fun <D : PhysicalDimension> String.c(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.c..measureUnit)

public infix fun <D : PhysicalDimension> String.m(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.m..measureUnit)

public infix fun <D : PhysicalDimension> String.µ(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.µ..measureUnit)

public infix fun <D : PhysicalDimension> String.n(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.n..measureUnit)

public infix fun <D : PhysicalDimension> String.p(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.p..measureUnit)

public infix fun <D : PhysicalDimension> String.f(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.f..measureUnit)

public infix fun <D : PhysicalDimension> String.a(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.a..measureUnit)

public infix fun <D : PhysicalDimension> String.z(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.z..measureUnit)

public infix fun <D : PhysicalDimension> String.y(measureUnit: ScaledUnit<D>): UnitNumberBF<D> =
    UnitNumberBF(this.toBFN(), DecimalPrefix.y..measureUnit)
