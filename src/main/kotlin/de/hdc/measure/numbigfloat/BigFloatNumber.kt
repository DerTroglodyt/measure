package de.hdc.measure.numbigfloat

import ch.obermuhlner.math.big.*
import ch.obermuhlner.math.big.BigFloat.Context
import ch.obermuhlner.math.big.kotlin.bigfloat.*
import de.hdc.measure.numbigfloat.BigFloatNumber.Companion.defaultPrecision
import de.hdc.measure.Const
import de.hdc.measure.approximates
import de.hdc.measure.core.*
import de.hdc.measure.numdouble.DoubleNumber
import de.hdc.measure.numinteger.LongNumber

/**
 * Implementation of [AlgebraicNumber] with BigFloat precision.
 */
public class BigFloatNumber(
    internal val value: BigFloat
) : AlgebraicNumber<BigFloat> {

    public companion object {
        /** Default number of significant digits. */
        public val defaultPrecision: UInt = 42u

        /** Default context when creating BigFloat numbers. */
        internal val defaultContext: Context =
            BigFloat.context(defaultPrecision.toInt()) ?: error("Could not initialize math context!")

        /** Maximum difference allowed in approximates() calls. */
        public val maxDifference: BigFloat = "1e${defaultPrecision - 1u}".toBF()

        /** Constant value */
        public val ZERO: BigFloatNumber = "0".toBFN()

        /** Constant value */
        public val ONE: BigFloatNumber = "1".toBFN()

        /** Constant value */
        public val MINUS_ONE: BigFloatNumber = "-1".toBFN()

        /** Constant value */
        public val ONE_THOUSAND: BigFloatNumber = "1000".toBFN()

        /**
         * Higher precision arc tangent function.
         * https://en.wikipedia.org/wiki/Atan2
         */
        public fun atan2(y: BigFloat, x: BigFloat): BigFloat {
            return when {
                x > ZERO.value -> BigFloat.atan(y / x)
                (x < ZERO.value) && (y >= ZERO.value) -> BigFloat.atan(y / x) + Const.PI.baseValue.value
                (x < ZERO.value) && (y < ZERO.value) -> BigFloat.atan(y / x) - Const.PI.baseValue.value
                x.isZero && (y > ZERO.value) -> (Const.PI.baseValue.value / 2)
                x.isZero && (y < ZERO.value) -> -(Const.PI.baseValue.value / 2)
                x.isZero && y.isZero -> ZERO.value
                else -> error("Should not happen!")
            }
        }
    }

    override fun compareTo(other: AlgebraicNumber<BigFloat>): Int {
        return if (other is BigFloatNumber) {
            value.compareTo(other.value)
        } else {
            -1
        }
    }

    override fun getValue(): BigFloat = value

    override fun plus(other: AlgebraicNumber<BigFloat>): BigFloatNumber =
        BigFloatNumber(value + other.getValue())

    override fun minus(other: AlgebraicNumber<BigFloat>): BigFloatNumber =
        BigFloatNumber(value - other.getValue())

    override fun times(other: AlgebraicNumber<BigFloat>): BigFloatNumber =
        BigFloatNumber(value * other.getValue())

    override fun div(other: AlgebraicNumber<BigFloat>): BigFloatNumber =
        BigFloatNumber(value / other.getValue())

    override fun times(other: Double): BigFloatNumber = BigFloatNumber(value * other)
    override fun times(other: Float): BigFloatNumber = BigFloatNumber(value * other.toDouble())
    override fun times(other: Int): BigFloatNumber = BigFloatNumber(value * other)
    override fun times(other: Long): BigFloatNumber = BigFloatNumber(value * other)

    override fun div(other: Double): BigFloatNumber = BigFloatNumber(value / other)
    override fun div(other: Float): BigFloatNumber = BigFloatNumber(value / other.toDouble())
    override fun div(other: Int): BigFloatNumber = BigFloatNumber(value / other)
    override fun div(other: Long): BigFloatNumber = BigFloatNumber(value / other)

    public fun approximates(
        other: BigFloatNumber,
        maxDiff: BigFloat = maxDifference
    ): Boolean {
        return value.approximates(other.getValue(), maxDiff)
    }

    override fun approximates(
        other: AlgebraicNumber<BigFloat>,
        maxDiff: AlgebraicNumber<BigFloat>
    ): Boolean {
        return value.approximates(other.getValue(), maxDiff.getValue())
    }

    override fun toDouble(): Double = value.toDouble()
    override fun fromSI(prefix: Prefix, converter: Converter): BigFloatNumber =
        converter.fromBaseUnit(this) / prefix.multiplier

    override fun toSI(prefix: Prefix, converter: Converter): BigFloatNumber =
        converter.toBaseUnit(this * prefix.multiplier)

    override fun equals(other: Any?): Boolean {
        return when (other) {
            is BigFloatNumber -> value.isEqual(other.value)
            is AlgebraicNumber<*> -> toDouble() == other.toDouble()
            else -> false
        }
    }

    override fun isZero(): Boolean = value == ZERO.value

    override fun isNegative(): Boolean = value < ZERO.value

    override fun isPositive(): Boolean = value > ZERO.value

    override fun isNaN(): Boolean = value.isNaN

    override fun isInfinite(): Boolean = value.isInfinity

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun toString(): String = value.toString()

    override fun reciprocal(): BigFloatNumber = ONE / this

    override fun inverse(): BigFloatNumber = MINUS_ONE * this

    /** Return the absolute (positive) value */
    public fun abs(): BigFloatNumber = BigFloat.abs(value).toBFN()

    /** Returns the natural exponent e^x */
    public fun exp(): BigFloatNumber = BigFloat.exp(value).toBFN()

    /** Returns the natural logarithm */
    public fun log(): BigFloatNumber = BigFloat.log(value).toBFN()

    /** Returns the logarithm to base 2 */
    public fun log2(): BigFloatNumber = BigFloat.log2(value).toBFN()

    /** Returns the logarithm to base 10*/
    public fun log10(): BigFloatNumber = BigFloat.log10(value).toBFN()

    /** Returns this value raised to the power of exp */
    public fun pow(exp: BigFloatNumber): BigFloatNumber = BigFloat.pow(value, exp.value).toBFN()

    /** Returns this exp-th root of this */
    public fun root(exp: BigFloatNumber): BigFloatNumber = BigFloat.root(value, exp.value).toBFN()

    /** Returns the square root */
    public fun sqrt(): BigFloatNumber = BigFloat.sqrt(value).toBFN()

    /** Returns the sine */
    public fun sin(): BigFloatNumber = BigFloat.sin(value).toBFN()

    /** Returns the hyperbolic sine */
    public fun sinh(): BigFloatNumber = BigFloat.sinh(value).toBFN()

    /** Returns the cosine */
    public fun cos(): BigFloatNumber = BigFloat.cos(value).toBFN()

    /** Returns the hyperbolic cosine */
    public fun cosh(): BigFloatNumber = BigFloat.cosh(value).toBFN()

    /** Returns the tangens */
    public fun tan(): BigFloatNumber = BigFloat.tan(value).toBFN()

    /** Returns the hyperbolic tangens */
    public fun tanh(): BigFloatNumber = BigFloat.tanh(value).toBFN()

    /** Returns the cotangens */
    public fun cot(): BigFloatNumber = BigFloat.cot(value).toBFN()

    /** Returns the hyperbolic cotangens */
    public fun coth(): BigFloatNumber = BigFloat.coth(value).toBFN()

    /** Returns the arc sine */
    public fun asin(): BigFloatNumber = BigFloat.asin(value).toBFN()

    /** Returns the hyperbolic arc sine */
    public fun asinh(): BigFloatNumber = BigFloat.asinh(value).toBFN()

    /** Returns the arc cosine */
    public fun acos(): BigFloatNumber = BigFloat.acos(value).toBFN()

    /** Returns the hyperbolic arc cosine */
    public fun acosh(): BigFloatNumber = BigFloat.acosh(value).toBFN()

    /** Returns the arc tangens */
    public fun atan(): BigFloatNumber = BigFloat.atan(value).toBFN()

    /** Returns the hyperbolic arc tangens */
    public fun atanh(): BigFloatNumber = BigFloat.atanh(value).toBFN()

    /** Returns the arc cotangens*/
    public fun acot(): BigFloatNumber = BigFloat.acot(value).toBFN()

    /** Returns the hyperbolic cotangens*/
    public fun acoth(): BigFloatNumber = BigFloat.acoth(value).toBFN()

    /**
     * Higher precision arc tangent function.
     * https://en.wikipedia.org/wiki/Atan2
     */
    public fun atan2(y: BigFloatNumber): BigFloatNumber = atan2(value, y.value).toBFN()

    /**
     * Sums up all elements in the Iterable.
     */
    public fun Iterable<BigFloatNumber>.sumOf(selector: (BigFloatNumber) -> BigFloatNumber): BigFloatNumber {
        var sum = "0".toBFN()
        for (element in this) {
            sum += selector(element)
        }
        return sum
    }
}

private fun context(precision: UInt): Context {
    return if (precision == defaultPrecision) BigFloatNumber.defaultContext
    else BigFloat.context(precision.toInt())
}

/**
 * The preferred way of constructing a BigFloatNumber.
 * @param precision Number of significant digits.
 */
public fun String.toBFN(precision: UInt = defaultPrecision): BigFloatNumber = BigFloatNumber(
    context(precision).valueOf(this.trim().replace("_", ""))
)

/**
 * Convenience constructor
 * @param precision Number of significant digits.
 */
public fun String.toBF(precision: UInt = defaultPrecision): BigFloat =
    context(precision).valueOf(this.trim().replace("_", ""))

/** Convenience constructor */
public fun BigFloat.toBFN(): BigFloatNumber = BigFloatNumber(this)

/**
 * Convenience constructor
 * @param precision Number of significant digits.
 */
@Deprecated("Possible loss of precision!", ReplaceWith("String.toBFN()"))
internal fun Double.toBFN(precision: UInt = defaultPrecision): BigFloatNumber =
    BigFloatNumber(context(precision).valueOf(this))

@Deprecated("Possible loss of precision!", ReplaceWith("String.toBFN()"))
internal fun DoubleNumber.toBFN(precision: UInt = defaultPrecision): BigFloatNumber =
    BigFloatNumber(context(precision).valueOf(this.value))

/**
 * Convenience constructor
 * @param precision Number of significant digits.
 */
@Deprecated("Possible loss of precision!", ReplaceWith("String.toBFN()"))
internal fun Float.toBFN(precision: UInt = defaultPrecision): BigFloatNumber =
    BigFloatNumber(context(precision).valueOf(this.toDouble()))

/**
 * Convenience constructor
 * @param precision Number of significant digits.
 */
internal fun Int.toBFN(precision: UInt = defaultPrecision): BigFloatNumber =
    BigFloatNumber(context(precision).valueOf(this))

/**
 * Convenience constructor
 * @param precision Number of significant digits.
 */
internal fun Long.toBFN(precision: UInt = defaultPrecision): BigFloatNumber =
    BigFloatNumber(context(precision).valueOf(this))

internal fun LongNumber.toBFN(precision: UInt = defaultPrecision): BigFloatNumber =
    BigFloatNumber(context(precision).valueOf(this.value))
