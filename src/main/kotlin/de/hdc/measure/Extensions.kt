package de.hdc.measure

import ch.obermuhlner.math.big.BigFloat
import ch.obermuhlner.math.big.kotlin.bigfloat.*
import de.hdc.measure.numbigfloat.BigFloatNumber.Companion.maxDifference
import java.util.*
import kotlin.math.*

/**
 * Tests if the two numbers are equal within the given range of maximal difference.
 */
public fun BigFloat.approximates(other: BigFloat, maxDiff: BigFloat = maxDifference): Boolean {
    return (this.isEqual(other)
            || (other.isLessThan(this + maxDiff) && other.isGreaterThan(this - maxDiff))
            )
}

/**
 * Ignores last two decimal digits when comparing for equal.
 */
public infix fun Number.approximates(x: Double): Boolean {
    return (this == x) || ((x < this.toDouble() + 1e-14) && (x > this.toDouble() - 1e-14))
}

/**
 * Tests if the two numbers are equal within the given range of maximal difference.
 */
public fun Number.approximates(x: Double, maxDiff: Double): Boolean {
    return (this == x) || ((x < this.toDouble() + maxDiff) && (x > this.toDouble() - maxDiff))
}

/**
 * Produces a string representation with the given number of digits after the decimal separator.
 */
public fun Number.format(digits: UInt = 3u): String {
    val invalid = this.isInvalid()
    return when {
        (invalid) -> this.toString()
        else -> {
            // Need to use .toDouble() or else %.f throws error for LongNumber.
            var s = String.format(Locale.UK, "%.${digits}f", this.toDouble())

            // insert thousand spacer
            val x = s.indexOf('.')
            var t = ""

            // insert spacer behind decimal point
            if (x >= 0) {
                var u = s.substring(x + 1)
                t = "."
                s = s.substring(0, x)

                while (u.length > 3) {
                    t = t + u.substring(0..2) + "\u202F"
                    u = u.substring(3)
                }
                t += u
            }

            val log = log10(abs(this.toDouble())).toInt() / 3
            if (log > 0) {
                // insert spacer in front of decimal point
                for (i in 1..log) {
                    t = "\u202F" + s.substring(s.lastIndex - 2) + t
                    s = s.substring(0, s.lastIndex - 2)
                }
            }
            s + t
        }
    }
}

/**
 * Tests if this number does not represent a numerical value within the value range (exclusive).
 */
public fun Number.isInvalid(): Boolean {
    return ((this.toDouble() == Double.POSITIVE_INFINITY)
            || (this.toDouble() == Double.NEGATIVE_INFINITY)
            || (this.toDouble() == Double.MAX_VALUE)
            || (this.toDouble() == Double.MIN_VALUE)
            || (this.toDouble().isNaN()))
}

/**
 * Tests if this number represents a numerical value within the value range (exclusive).
 */
public fun Number.isValid(): Boolean = !this.isInvalid()
