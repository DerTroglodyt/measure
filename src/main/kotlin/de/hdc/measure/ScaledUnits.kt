@file:Suppress("ClassName", "NonAsciiCharacters")

package de.hdc.measure

import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.*
import de.hdc.measure.numbigfloat.BigFloatNumber.Companion.ONE

//Scaled Units for convenience
/**
 * (metric) ton
 */
public object t : ScaledUnit<Mass>("Ton", "t", g, "1e6".toBFN())

/**
 * Minute
 */
public object min : ScaledUnit<Time>("Minute", "min", s, "60".toBFN())

/**
 * Hour
 */
public object h : ScaledUnit<Time>("Hour", "h", s, "3600".toBFN())

/**
 * Day
 */
public object d : ScaledUnit<Time>("Day", "d", s, "86400".toBFN())

/**
 * Astronomical Unit (mean distance earth <-> sun)
 */
public object AU : ScaledUnit<Length>(
    "Astronomical Unit", "AU", m, "149597870700".toBFN()
)

/**
 * Light year
 */
public object ly : ScaledUnit<Length>(
    "Light Year", "ly", m, "9460730472580800".toBFN()
)

/**
 * Parsec
 */
public object pc : ScaledUnit<Length>(
    "Parsec", "pc", m, "3.08567758149137e16".toBFN()
)

/**
 * Liter
 */
public object L : ScaledUnit<Volume>(
    "Liter", "L", m3, "1e-3".toBFN()
)

/**
 * Degrees Celsius
 */
public object `°C` : ScaledUnit<Temperature>(
    "Celsius", "°C", K, Converter({ it + "273.15".toBFN() }, { it - "273.15".toBFN() })
)

/**
 * Kilometers per hour
 */
public object km_h : ScaledUnit<Velocity>(
    "Velocity", "[km h⁻¹]", m_s, ONE / "3.6".toBFN()
)

/**
 * Degrees (angle)
 */
public object `°` : ScaledUnit<Angle>(
    "Degree", "°", rad, (Const.PI / "180".toBFN()).baseValue
)

/**
 * Molarity
 */
public object mol_L : ScaledUnit<Molarity>(
    "Molarity", "[mol L⁻¹]", mol_m3, "1e-3".toBFN()
)

/**
 * Molar Mass
 */
public object g_mol : ScaledUnit<MolarMass>(
    "Molar Mass", "[g mol⁻¹]", kg_mol, "1".toBFN()
)

/**
 * List of all known (non base) ScaledUnits.
 */
public object ScaledUnitTable {
    private val map: MutableMap<String, MutableList<ScaledUnit<*>>> = mutableMapOf()

    init {
//        require(UnitTable[Quantity.from(L = 5)] == null)
        val units = listOf(t, min, h, d, AU, ly, pc, L, `°C`, km_h, `°`, mol_L, g_mol)
        val s = units.groupBy { it.quantity.toString() }.keys.toSet()
        s.forEach { dim ->
            map[dim] = units.filter { it.quantity.toString() == dim }
                .sortedByDescending { it.converter.multiplier }
                .toMutableList()
        }
    }

    /**
     * Lists all known ScaledUnits with the given dimension in descending order of multiplier (bigger units first).
     * Used in UnitNumberImpl.findOptimalPrefix.
     */
    public operator fun get(key: Quantity): List<ScaledUnit<*>>? = map[key.toString()]

    /**
     * Puts a new ScaledUnit into the global ScaledUnitTable.
     */
    public fun add(value: ScaledUnit<*>) {
        if (value.toString().isEmpty()) {
            error("Invalid ScaledUnit put into table!")
        }
        val entry = map[value.quantity.toString()] ?: let {
            map[value.quantity.toString()] = mutableListOf()
            map[value.quantity.toString()]!!
        }
        entry.add(value)
        entry.sortByDescending { it.converter.multiplier }
    }
}
