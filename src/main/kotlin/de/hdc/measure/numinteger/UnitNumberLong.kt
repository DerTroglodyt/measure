package de.hdc.measure.numinteger

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.toBFN
import de.hdc.measure.numdouble.UnitNumberDouble
import kotlin.math.abs

/**
 * An immutable value and an associated unit.
 * Implementations vary in used backing fields for values (Long / Double / BigDecimal)
 *
 * @param value The internal value in SI base unit
 * @param unit The external BaseUnit / ScaledUnit
 * @param convert Convert to SI units. Set true for first time construction. False for calculations afterwards.
 */
public class UnitNumberLong<D : PhysicalDimension> internal constructor(
    value: LongNumber,
    override val unit: ScaledUnit<D>,
    convert: Boolean = true
) : UnitNumber<Long, LongNumber, D>,
    AlgebraicNumber<Long>,// by value,
    Comparable<AlgebraicNumber<Long>> {

    override val baseValue: LongNumber = if (convert) {
        value.toSI(unit.prefix, unit.converter)
    } else {
        value
    }

    internal constructor(
        value: Long,
        unit: ScaledUnit<D>,
        convert: Boolean = true
    ) : this(LongNumber(value), unit, convert)

    override infix fun <ND : PhysicalDimension> toUnit(newUnit: ScaledUnit<ND>): UnitNumberLong<ND> {
        require(this.unit.quantity == newUnit.quantity)
        return UnitNumberLong(baseValue, newUnit, false)
    }

    override fun toString(): String = format(1u, 0u)

    override fun compareTo(other: AlgebraicNumber<Long>): Int {
        return when {
            other !is UnitNumber<*, *, *> -> -1
            unit.quantity != other.unit.quantity -> -1
            else -> {
                @Suppress("SafeCastWithReturn")
                other.baseValue as? LongNumber ?: return -1
                baseValue.compareTo(other.baseValue as LongNumber)
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnitNumber<*, *, *>

        return (unit.quantity == other.unit.quantity) &&
                ((unit.dimension == other.unit.dimension) || (unit.dimension == Combined) || (other.unit.dimension == Combined)) &&
                (baseValue == other.baseValue)
    }

    override fun hashCode(): Int {
        var result = unit.hashCode()
        result = 31 * result + baseValue.hashCode()
        return result
    }

    public override fun simplify(): UnitNumber<*, *, *> {
        val u = unit.simplify()
        return if (u == null) this
        else UnitNumberLong(baseValue, u.first, true)
    }

    // Implement AlgebraicNumber ##########################################
    /**
     * Returns the internal value in SI base unit which may differ from then display unit.
     */
    @Deprecated(
        "Most Likely not the value you are looking for! " +
                "Returns the internal value in SI base unit which may differ from the display unit.",
        ReplaceWith("value.getPrintValue()")
    )
    override fun getValue(): Long = baseValue.getValue()

    override fun isZero(): Boolean = baseValue.isZero()
    override fun isNegative(): Boolean = baseValue.isNegative()
    override fun isPositive(): Boolean = baseValue.isPositive()
    override fun isNaN(): Boolean = baseValue.isNaN()
    override fun isInfinite(): Boolean = baseValue.isInfinite()

    @Deprecated(
        message = "UnitNumber should only be compared to other UnitNumber.",
        level = DeprecationLevel.ERROR,
        replaceWith = ReplaceWith("approximates(other: UnitNumber<Long, LongNumber, D>, maxDiff: AlgebraicNumber<Long>)")
    )
    override fun approximates(other: AlgebraicNumber<Long>, maxDiff: AlgebraicNumber<Long>): Boolean =
        baseValue.approximates(other, maxDiff)

    @Deprecated("Reciprocal of a LongNumber is undefined.", level = DeprecationLevel.ERROR)
    override fun reciprocal(): UnitNumberLong<*> {
        error("Reciprocal of a LongNumber is undefined.")
    }

    override fun inverse(): UnitNumberLong<D> = UnitNumberLong(baseValue.inverse() as LongNumber, unit)

    override fun fromSI(prefix: Prefix, converter: Converter): LongNumber =
        baseValue.fromSI(prefix, converter)

    override fun toSI(prefix: Prefix, converter: Converter): LongNumber =
        baseValue.toSI(prefix, converter)

    override operator fun times(other: Double): UnitNumberLong<D> =
        UnitNumberLong(baseValue * other, unit, false)

    override operator fun times(other: Float): UnitNumberLong<D> = this * other.toDouble()
    override operator fun times(other: Int): UnitNumberLong<D> = this * other.toDouble()
    override operator fun times(other: Long): UnitNumberLong<D> = this * other.toDouble()

    public fun times(other: UnitNumber<Long, LongNumber, D>): UnitNumberLong<*> {
        return UnitNumberLong(baseValue * other.baseValue, unit, false)
    }

    override operator fun div(other: Double): UnitNumberLong<D> =
        UnitNumberLong(baseValue / other, unit, false)

    /**
     * Scales this value by a factor.
     */
    override operator fun div(other: Float): UnitNumberLong<D> =
        UnitNumberLong(baseValue / other, unit, false)

    /**
     * Scales this value by a factor.
     */
    override operator fun div(other: Int): UnitNumberLong<D> =
        UnitNumberLong(baseValue / other, unit, false)

    /**
     * Scales this value by a factor.
     */
    override operator fun div(other: Long): UnitNumberLong<D> =
        UnitNumberLong(baseValue / other, unit, false)

    public fun div(other: UnitNumber<Long, LongNumber, D>): UnitNumberLong<D> {
        return UnitNumberLong(baseValue / other.baseValue, unit, false)
    }

    /**
     * Adds two values in the same PhysicalDimension.
     */
    override operator fun plus(other: AlgebraicNumber<Long>): UnitNumberLong<D> {
        require(other is UnitNumberLong<*>) { "Can not add '${this.javaClass.name}' to '${other.javaClass.name}'!" }

        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberLong<D> ?: error("Can not add '${this.unit}' to '${other.unit}'!")
        if (unit.quantity != other.unit.quantity)
            error("Can not add '${unit.quantity}' to '${other.unit.quantity}'!")
        return UnitNumberLong(baseValue + other.baseValue, unit, false)
    }

    /**
     * Subtracts two values in the same PhysicalDimension.
     */
    override operator fun minus(other: AlgebraicNumber<Long>): UnitNumberLong<D> {
        require(other is UnitNumberLong<*>) {
            "Can not subtract '${other.javaClass.name}' from '${this.javaClass.name}'!"
        }

        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberLong<D> ?: error("Can not subtract '${other.unit}' from '${this.unit}'!")
        if (unit.quantity != other.unit.quantity)
            error("Can not subtract '${other.unit.quantity}' from '${this.unit.quantity}'!")
        return UnitNumberLong(baseValue - other.baseValue, unit, false)
    }

    public operator fun times(other: UnitNumberLong<*>): UnitNumberLong<*> {
        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberLong<D> ?: error("Can not multiply '${this.unit}' with '${other.unit}'!")
        val u = unit * other.unit
        return UnitNumberLong(u.second.toBaseUnit((baseValue * other.baseValue).toBFN()).toLong(), u.first, false)
    }

    override operator fun times(other: AlgebraicNumber<Long>): UnitNumberLong<D> {
        return UnitNumberLong(baseValue * other, unit, false)
    }

    public operator fun div(other: UnitNumberLong<*>): UnitNumberLong<*> {
        @Suppress("UNCHECKED_CAST")
        other as? UnitNumberLong<D> ?: error("Can not divide '${this.unit}' by '${other.unit}'!")
        val u = unit / other.unit
        return UnitNumberLong(u.second.toBaseUnit((baseValue / other.baseValue).toBFN()).toLong(), u.first, false)
    }

    override operator fun div(other: AlgebraicNumber<Long>): UnitNumberLong<D> {
        return UnitNumberLong(baseValue / other, unit, false)
    }
}

/** Scales a UnitNumberLong by a factor */
internal operator fun <D : PhysicalDimension> Double.times(other: UnitNumberLong<D>): UnitNumberLong<D> =
    other * this

/** Scales a UnitNumberLong by a factor */
internal operator fun <D : PhysicalDimension> Float.times(other: UnitNumberLong<D>): UnitNumberLong<D> =
    other * this

/** Scales a UnitNumberLong by a factor */
internal operator fun <D : PhysicalDimension> Int.times(other: UnitNumberLong<D>): UnitNumberLong<D> =
    other * this

/** Scales a UnitNumberLong by a factor */
internal operator fun <D : PhysicalDimension> Long.times(other: UnitNumberLong<D>): UnitNumberLong<D> =
    other * this
