@file:Suppress(
    "NonAsciiCharacters", "FunctionName",
    "KDocMissingDocumentation"
)

package de.hdc.measure.numinteger

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.core.ScaledUnit

public infix fun <D : PhysicalDimension> Long.Y(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.Y..measureUnit)

public infix fun <D : PhysicalDimension> Long.Z(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.Z..measureUnit)

public infix fun <D : PhysicalDimension> Long.X(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.X..measureUnit)

public infix fun <D : PhysicalDimension> Long.P(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.P..measureUnit)

public infix fun <D : PhysicalDimension> Long.T(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.T..measureUnit)

public infix fun <D : PhysicalDimension> Long.G(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.G..measureUnit)

public infix fun <D : PhysicalDimension> Long.M(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.M..measureUnit)

public infix fun <D : PhysicalDimension> Long.k(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.k..measureUnit)

public infix fun <D : PhysicalDimension> Long.h(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.h..measureUnit)

public infix fun <D : PhysicalDimension> Long.da(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.da..measureUnit)

public infix fun <D : PhysicalDimension> Long.ˍ(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.NONE..measureUnit)

public infix fun <D : PhysicalDimension> Long.d(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.d..measureUnit)

public infix fun <D : PhysicalDimension> Long.c(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.c..measureUnit)

public infix fun <D : PhysicalDimension> Long.m(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.m..measureUnit)

public infix fun <D : PhysicalDimension> Long.µ(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.µ..measureUnit)

public infix fun <D : PhysicalDimension> Long.n(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.n..measureUnit)

public infix fun <D : PhysicalDimension> Long.p(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.p..measureUnit)

public infix fun <D : PhysicalDimension> Long.f(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.f..measureUnit)

public infix fun <D : PhysicalDimension> Long.a(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.a..measureUnit)

public infix fun <D : PhysicalDimension> Long.z(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.z..measureUnit)

public infix fun <D : PhysicalDimension> Long.y(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.y..measureUnit)

//###########################################################################
public infix fun <D : PhysicalDimension> Int.Y(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.Y..measureUnit)

public infix fun <D : PhysicalDimension> Int.Z(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.Z..measureUnit)

public infix fun <D : PhysicalDimension> Int.X(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.X..measureUnit)

public infix fun <D : PhysicalDimension> Int.P(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.P..measureUnit)

public infix fun <D : PhysicalDimension> Int.T(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.T..measureUnit)

public infix fun <D : PhysicalDimension> Int.G(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.G..measureUnit)

public infix fun <D : PhysicalDimension> Int.M(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.M..measureUnit)

public infix fun <D : PhysicalDimension> Int.k(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.k..measureUnit)

public infix fun <D : PhysicalDimension> Int.h(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.h..measureUnit)

public infix fun <D : PhysicalDimension> Int.da(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.da..measureUnit)

public infix fun <D : PhysicalDimension> Int.ˍ(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.NONE..measureUnit)

public infix fun <D : PhysicalDimension> Int.d(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.d..measureUnit)

public infix fun <D : PhysicalDimension> Int.c(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.c..measureUnit)

public infix fun <D : PhysicalDimension> Int.m(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.m..measureUnit)

public infix fun <D : PhysicalDimension> Int.µ(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.µ..measureUnit)

public infix fun <D : PhysicalDimension> Int.n(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.n..measureUnit)

public infix fun <D : PhysicalDimension> Int.p(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.p..measureUnit)

public infix fun <D : PhysicalDimension> Int.f(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.f..measureUnit)

public infix fun <D : PhysicalDimension> Int.a(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.a..measureUnit)

public infix fun <D : PhysicalDimension> Int.z(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.z..measureUnit)

public infix fun <D : PhysicalDimension> Int.y(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this.toLong(), DecimalPrefix.y..measureUnit)

//###########################################################################
public infix fun <D : PhysicalDimension> LongNumber.Y(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.Y..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.Z(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.Z..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.X(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.X..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.P(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.P..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.T(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.T..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.G(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.G..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.M(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.M..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.k(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.k..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.h(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.h..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.da(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.da..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.ˍ(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.NONE..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.d(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.d..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.c(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.c..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.m(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.m..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.µ(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.µ..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.n(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.n..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.p(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.p..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.f(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.f..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.a(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.a..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.z(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.z..measureUnit)

public infix fun <D : PhysicalDimension> LongNumber.y(measureUnit: ScaledUnit<D>): UnitNumberLong<D> =
    UnitNumberLong(this, DecimalPrefix.y..measureUnit)
