package de.hdc.measure.numinteger

import de.hdc.measure.*
import de.hdc.measure.core.*
import de.hdc.measure.numbigfloat.toBFN
import de.hdc.measure.approximates
import de.hdc.measure.isInvalid

/**
 * An integer number type.
 * @param value The [Long] value
 */
public open class LongNumber(internal val value: Long) : AlgebraicNumber<Long> {

    override fun compareTo(other: AlgebraicNumber<Long>): Int {
        return if (other is LongNumber) {
            value.compareTo(other.value)
        } else {
            -1
        }
    }

    override fun getValue(): Long = value

    override fun plus(other: AlgebraicNumber<Long>): LongNumber =
        LongNumber(value + other.toLong())

    override fun minus(other: AlgebraicNumber<Long>): LongNumber =
        LongNumber(value - other.toLong())

    override fun times(other: AlgebraicNumber<Long>): LongNumber =
        LongNumber(value * other.toLong())

    override fun times(other: Double): LongNumber = LongNumber(value * other.toLong())
    override fun times(other: Long): LongNumber = LongNumber(value * other)
    override fun times(other: Int): LongNumber = LongNumber(value * other.toLong())

    override fun div(other: AlgebraicNumber<Long>): LongNumber =
        LongNumber(value / other.toLong())

    override fun div(other: Double): LongNumber = LongNumber(value / other.toLong())
    override fun div(other: Float): LongNumber = LongNumber(value / other.toLong())
    override fun div(other: Int): LongNumber = LongNumber(value / other)
    override fun div(other: Long): LongNumber = LongNumber(value / other)

    override fun approximates(other: AlgebraicNumber<Long>, maxDiff: AlgebraicNumber<Long>): Boolean =
        this.toDouble().approximates(other.toDouble(), maxDiff.toDouble())

    override fun toDouble(): Double = value.toDouble()
    override fun fromSI(prefix: Prefix, converter: Converter): LongNumber =
        LongNumber((converter.fromBaseUnit(value.toBFN()) / prefix.multiplier).toLong())

    override fun toSI(prefix: Prefix, converter: Converter): LongNumber =
        LongNumber(converter.toBaseUnit(value.toBFN() * prefix.multiplier).toLong())

    /** Tests if two values differ less than the given range. */
    public fun approximates(other: AlgebraicNumber<Long>, maxDiff: Double): Boolean =
        value.approximates(other.toDouble(), maxDiff)

    override fun equals(other: Any?): Boolean {
        if (other !is LongNumber) return false
        return value == other.value
    }

    override fun isZero(): Boolean = this.value == 0L

    override fun isNegative(): Boolean = this.value < 0L

    override fun isPositive(): Boolean = this.value > 0L

    override fun isNaN(): Boolean = this.value.isInvalid()

    override fun isInfinite(): Boolean = false

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun toString(): String = value.toString()

    @Deprecated("Reciprocal of a LongNumber is undefined.", level = DeprecationLevel.ERROR)
    override fun reciprocal(): AlgebraicNumber<Long> = error("Reciprocal of a LongNumber is undefined.")

    override fun inverse(): AlgebraicNumber<Long> = LongNumber(-1 * value)

    /**
     * The absolut (positive) value of this number.
     */
    public fun abs(): LongNumber = LongNumber(kotlin.math.abs(value))

    /**
     * Sums up all elements in the Iterable.
     */
    public fun Iterable<LongNumber>.sumOf(selector: (LongNumber) -> LongNumber): LongNumber {
        var sum = LongNumber(0L)
        for (element in this) {
            sum += selector(element)
        }
        return sum
    }
}

/** Scales a UnitNumberLong by a factor */
internal operator fun <D : PhysicalDimension> Double.times(other: LongNumber): LongNumber =
    other * this.toLong()

/** Scales a UnitNumberLong by a factor */
internal operator fun <D : PhysicalDimension> Float.times(other: LongNumber): LongNumber =
    other * this.toLong()

/** Scales a UnitNumberLong by a factor */
internal operator fun <D : PhysicalDimension> Int.times(other: LongNumber): LongNumber =
    other * this

/** Scales a UnitNumberLong by a factor */
internal operator fun <D : PhysicalDimension> Long.times(other: LongNumber): LongNumber =
    other * this
